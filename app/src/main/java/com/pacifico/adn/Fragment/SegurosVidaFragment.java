package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RadioGroup;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.R;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;
import com.pacifico.adn.Views.Interface.OnStepperDoneSelectedListener;

public class SegurosVidaFragment extends Fragment implements OnStepperChangeValueListener, RadioGroup.OnCheckedChangeListener, StepperView.OnFocusChangeListener {
    private double montoSeguroVidaLey;
    int factorVidaLey, montoIngresoBrutoMensual;
    private ADNActivity actADN;
    private StepperView stepSeguroIndividual, stepActual;
    private RadioGroup rgMonedaSeguroIndividual;
    private TextView txtVidaLey16, txtTopeVidaLey;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_seguros_vida, container, false);
        stepSeguroIndividual = (StepperView)view.findViewById(R.id.stepSeguroIndividual);
        final StepperView stepSeguroVidaLey = (StepperView)view.findViewById(R.id.stepSeguroVidaLey);
        rgMonedaSeguroIndividual = (RadioGroup)view.findViewById(R.id.rgMonedaSeguroIndividual);
        RadioGroup rgMonedaVidaLey = (RadioGroup)view.findViewById(R.id.rgMonedaVidaLey);
        txtVidaLey16 = (TextView)view.findViewById(R.id.txtVidaLey16);
        TextView txtFactorVidaLey = (TextView)view.findViewById(R.id.txtFactorVidaLey);
        txtTopeVidaLey = (TextView)view.findViewById(R.id.txtTopeVidaLey);
        CheckBox checkCuentaVidaLey = (CheckBox)view.findViewById(R.id.checkCuentaVidaLey);
        rgMonedaSeguroIndividual.check((actADN.getAdnBean().getMonedaSeguroIndividual() == Constantes.MonedaDolares ? R.id.rb11 : R.id.rb12));
        stepSeguroIndividual.setValorActual(actADN.getAdnBean().getSeguroIndividual());
        stepSeguroIndividual.setOnStepperDoneSelectedListener(new OnStepperDoneSelectedListener() {
            @Override
            public void onStepperDoneSelected(StepperView stepperView) {
                stepSeguroVidaLey.requestStepperFocus();
            }
        });
        rgMonedaVidaLey.check((actADN.getAdnBean().getMonedaIngresoBrutoMensualVidaLey() == Constantes.MonedaDolares ? R.id.rb21 : R.id.rb22));
        montoIngresoBrutoMensual = actADN.getAdnBean().getIngresoBrutoMensualVidaLey();
        stepSeguroVidaLey.setValorActual(montoIngresoBrutoMensual);
        factorVidaLey = actADN.getAdnBean().getFactorVidaLey();
        txtFactorVidaLey.setText(String.format("X %d =", factorVidaLey));
        checkCuentaVidaLey.setChecked(actADN.getAdnBean().getFlagCuentaConVidaLey() == 0);
        txtTopeVidaLey.setText(String.format("*Tope de Ley %s %s", actADN.getAdnBean().getMonedaTopeVidaLey() == Constantes.MonedaDolares ? getString(R.string.simbolo_dolares) : getString(R.string.simbolo_soles), Util.obtenerNumeroConFormatoDefinido(actADN.getAdnBean().getTopeVidaLey(), "###,###")));
        validarVidaLey();
        stepSeguroIndividual.setOnStepperChangeValueListener(this);
        stepSeguroVidaLey.setOnStepperChangeValueListener(this);
        rgMonedaSeguroIndividual.setOnCheckedChangeListener(this);
        rgMonedaVidaLey.setOnCheckedChangeListener(this);
        checkCuentaVidaLey.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                actADN.getAdnBean().setFlagCuentaConVidaLey(isChecked ? 0 : 1);
                txtVidaLey16.setTextColor(isChecked ? getResources().getColor(R.color.colorGris) : getResources().getColor(R.color.colorMarron));
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
                calcularTotal();
            }
        });
        stepSeguroIndividual.setOnFocusChangeListener(this);
        stepSeguroVidaLey.setOnFocusChangeListener(this);
        RelativeLayout relLaySegurosVida = (RelativeLayout)view.findViewById(R.id.relLaySegurosVida);
        relLaySegurosVida.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(stepActual != null){
                    stepActual.clearFocus();
                }
            }
        });
        return view;
    }
    @Override
    public void onChangeValue(StepperView stepView){
        int valorActual = stepView.getValorActual();
        if(stepView == stepSeguroIndividual){
            if(valorActual != actADN.getAdnBean().getSeguroIndividual()){
                actADN.getAdnBean().setSeguroIndividual(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else{
            if(valorActual != actADN.getAdnBean().getIngresoBrutoMensualVidaLey()){
                montoIngresoBrutoMensual = valorActual;
                actADN.getAdnBean().setIngresoBrutoMensualVidaLey(montoIngresoBrutoMensual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
                validarVidaLey();
            }
        }
        calcularTotal();
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group == rgMonedaSeguroIndividual){
            actADN.getAdnBean().setMonedaSeguroIndividual((checkedId == R.id.rb11) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else{
            actADN.getAdnBean().setMonedaIngresoBrutoMensualVidaLey((checkedId == R.id.rb21) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setMonedaSeguroVidaLey((checkedId == R.id.rb21) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
            validarVidaLey();
        }
        calcularTotal();
    }
    public void calcularTotal(){
        double factorSeguroIndividual = actADN.getAdnBean().getMonedaSeguroIndividual() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double montoVidaLey = 0;
        if(actADN.getAdnBean().getFlagCuentaConVidaLey() == 1){
            double factorVidaLey = actADN.getAdnBean().getMonedaIngresoBrutoMensualVidaLey() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
            montoVidaLey = montoSeguroVidaLey / factorVidaLey;
        }
        double montoTotal = stepSeguroIndividual.getValorActual() / factorSeguroIndividual + montoVidaLey;
        actADN.getAdnBean().setTotalSegurosVida(Util.redondearNumero(montoTotal, 2));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    public void validarVidaLey(){
        double factorSeguroVidaLey = actADN.getAdnBean().getMonedaSeguroVidaLey() == Constantes.MonedaSoles ? 1 : actADN.getAdnBean().getTipoCambio();
        double topeVidaLey = actADN.getAdnBean().getTopeVidaLey() / factorSeguroVidaLey;
        double montoIngresoBrutoMensualTemp = montoIngresoBrutoMensual;
        if(montoIngresoBrutoMensualTemp > topeVidaLey){
            montoIngresoBrutoMensualTemp = topeVidaLey;
            txtTopeVidaLey.setTextColor(getResources().getColor(R.color.colorNaranja));
        }else{
            txtTopeVidaLey.setTextColor(getResources().getColor(R.color.colorMarron));
        }
        montoSeguroVidaLey = Util.redondearNumero(montoIngresoBrutoMensualTemp * factorVidaLey, 0);
        actADN.getAdnBean().setSeguroVidaLey(montoSeguroVidaLey);
        txtVidaLey16.setText(Util.obtenerNumeroConFormatoDefinido(montoSeguroVidaLey, "##,###,###"));
    }
    @Override
    public void onFocusChange(boolean hasFocus, StepperView stepperView){
        if(hasFocus){
            stepActual = stepperView;
        }else{
            stepActual = null;
        }
    }
    public void clearStepActual(){
        if(stepActual != null){
            stepActual.clearFocus();
        }
    }
}