package com.pacifico.adn.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

import com.pacifico.adn.Model.Bean.DispositivoBean;
import com.pacifico.adn.Model.Bean.ParametroBean;
import com.pacifico.adn.Model.Controller.DispositivoController;
import com.pacifico.adn.Model.Controller.ParametroController;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by vctrls3477 on 5/06/16.
 */
public class Util{
    public static double expiroTiempoSinSincronizacion(){
        DispositivoBean dispositivoBean = DispositivoController.obtenerDispositivoPorIdDispositivo(1);
        String fechaUltimaSincronizacion = dispositivoBean.getFechaUltimaSincronizacion();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSS");
        try{
            Date dateUltimaSincronizacion = simpleDateFormat.parse(fechaUltimaSincronizacion);
            Date dateAhora = new Date();
            double diferencia = dateAhora.getTime() - dateUltimaSincronizacion.getTime();
            ParametroBean tiempoSinSincronizacionParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(9);
            double tiempoSinSincronizacion = tiempoSinSincronizacionParametroBean.getValorNumerico() * 60 * 60 * 1000;
            diferencia = diferencia - tiempoSinSincronizacion;
            return diferencia;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.i("TAG", "error");
        }
        return 0;
    }
    public static String obtenerNumeroConFormato(double numero){
        return obtenerNumeroConFormatoDefinido(numero, "##,###,###");
    }
    public static String obtenerNumeroConDecimales(double numero){
        return obtenerDecimalFormat("###########.##").format(numero);
    }
    public static String obtenerNumeroConFormatoDefinido(double numero, String formato){
        return obtenerDecimalFormat(formato).format(Math.round(numero));
    }
    public static String obtenerMontoDolaresConFomato(double monto){
        return String.format("$ %s", obtenerNumeroConFormato(monto));
    }
    public static void mostrarAlertaConTitulo(String titulo, String mensaje, Context context){
        new AlertDialog.Builder(context)
                .setTitle(titulo)
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public static String obtenerFechaActual(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar ahora = Calendar.getInstance();
        return simpleDateFormat.format(ahora.getTime());
    }

    public static String capitalizedString(String string){

        if(string != null && string.length() > 0){
            String[] arrString = string.split(" ");
            String strFinal = "";
            for(int i = 0; i < arrString.length; i++){
                String tempString = arrString[i].trim();

                if (tempString.length() > 1)
                    strFinal = strFinal + String.format("%s%s ", tempString.substring(0, 1), tempString.substring(1).toLowerCase());
                else if (tempString.length() == 1)
                    strFinal = strFinal + tempString;
            }
            return strFinal.trim();
        }else{
            return "";
        }
    }

    public static String strNULL(Object object) {
        return object == null ? "" : object.toString();
    }

    public static InputFilter obtenerFiltroSoloLetras(){
        return new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend){
                for (int i = start; i < end; ++i){
                    if (!Pattern.compile("[ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz'áéíóúÁÉÍÓÚ ]*").matcher(String.valueOf(source.charAt(i))).matches()){
                        return "";
                    }
                }
                return null;
            }
        };
    }
    public static double redondearNumero(double numero, int decimales){
        int factor = (int)Math.pow(10, decimales);
        numero = numero * factor;
        double numeroRedondeado = Math.round(numero);
        return numeroRedondeado / factor;
    }
    public static DecimalFormat obtenerDecimalFormat(String formato){
        DecimalFormat formatter = new DecimalFormat(formato);
        DecimalFormatSymbols custom = new DecimalFormatSymbols(new Locale("es", "US"));
        custom.setDecimalSeparator('.');
        custom.setGroupingSeparator(',');
        formatter.setDecimalFormatSymbols(custom);
        return formatter;
    }
}