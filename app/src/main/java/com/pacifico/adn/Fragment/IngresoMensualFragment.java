package com.pacifico.adn.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioGroup;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.R;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.CustomEditText;

public class IngresoMensualFragment extends Fragment{
    private RadioGroup rgPorcentaje;
    private TextView txtIngresoMensual, txtMonto5, txtMonto7;
    private CustomEditText edtMonto;
    private int monto;
    private double monto5, monto7;
    private ADNActivity actADN;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_ingreso_mensual, container, false);
        rgPorcentaje = (RadioGroup)view.findViewById(R.id.rgPorcentaje);
        edtMonto = (CustomEditText)view.findViewById(R.id.edtMonto);
        txtIngresoMensual = (TextView)view.findViewById(R.id.txtIngresoMensual);
        txtMonto5 = (TextView)view.findViewById(R.id.txtMonto5);
        txtMonto7 = (TextView)view.findViewById(R.id.txtMonto7);
        monto = 0;
        double porcentajeInversion = actADN.getAdnBean().getPorcentajeInversion();
        if(porcentajeInversion > -1){
            if(porcentajeInversion == 5){
                rgPorcentaje.check(R.id.rb1);
            }else if (porcentajeInversion == 7){
                rgPorcentaje.check(R.id.rb2);
            }else{
                rgPorcentaje.check(R.id.rb3);
                monto = (int)actADN.getAdnBean().getMontoMensualInvertir();
                edtMonto.setText(String.format("$ %d", monto));
            }
        }
        edtMonto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edtMonto.setText(Integer.toString(monto));
                    edtMonto.selectAll();
                } else {
                    if (edtMonto.length() == 0) {
                        monto = 0;
                    } else {
                        monto = Integer.parseInt(edtMonto.getText().toString());
                    }
                    edtMonto.setText(String.format("$ %d", monto));
                    actualizarMontoTotal();
                }
            }
        });
        rgPorcentaje.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb3) {
                    edtMonto.requestFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(edtMonto, 0);
                }
                actualizarMontoTotal();
            }
        });
        actualizarIngresoMensual();
        RelativeLayout relLayIngresoMensual = (RelativeLayout)view.findViewById(R.id.relLayIngresoMensual);
        relLayIngresoMensual.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(edtMonto.hasFocus()){
                    edtMonto.clearFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(edtMonto.getWindowToken(), 0);
                }
            }
        });
        return view;
    }
    public void actualizarIngresoMensual(){
        double factorIngresoMensual = (actADN.getAdnBean().getMonedaIngresoMensualTitular() == Constantes.MonedaDolares) ? 1 : actADN.getAdnBean().getTipoCambio();
        double ingresoMensual = actADN.getAdnBean().getIngresoMensualTitular() / factorIngresoMensual;
        monto5 = Util.redondearNumero(ingresoMensual * 0.05, 0);
        monto7 = Util.redondearNumero(ingresoMensual * 0.07, 0);
        txtIngresoMensual.setText(Util.obtenerMontoDolaresConFomato(ingresoMensual));
        txtMonto5.setText(Util.obtenerMontoDolaresConFomato(monto5));
        txtMonto7.setText(Util.obtenerMontoDolaresConFomato(monto7));
        actualizarMontoTotal();
    }
    public void actualizarMontoTotal(){
        int checkedId = rgPorcentaje.getCheckedRadioButtonId();
        if(checkedId != -1){
            double montoFinal = monto;
            int porcentaje = 0;
            if (checkedId == R.id.rb1) {
                montoFinal = monto5;
                porcentaje = 5;
            } else if (checkedId == R.id.rb2) {
                montoFinal = monto7;
                porcentaje = 7;
            }
            if(porcentaje != actADN.getAdnBean().getPorcentajeInversion()){
                actADN.getAdnBean().setPorcentajeInversion(porcentaje);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
            if(montoFinal != actADN.getAdnBean().getMontoMensualInvertir()){
                actADN.getAdnBean().setMontoMensualInvertir(montoFinal);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
            actADN.getFlujoADNFragment().actualizarMontoTotal();
        }
    }
    public void clearEdtMontoFocus(){
        if(edtMonto.hasFocus()){
            edtMonto.clearFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(edtMonto.getWindowToken(), 0);
        }
    }
}