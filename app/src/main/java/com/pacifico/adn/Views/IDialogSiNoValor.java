package com.pacifico.adn.Views;


import com.pacifico.adn.Model.Bean.TablaTablasBean;

/**
 * Created by joel on 7/24/16.
 */
public interface IDialogSiNoValor {
    void respuesta_si(TablaTablasBean valor);
    void respuesta_no();
}
