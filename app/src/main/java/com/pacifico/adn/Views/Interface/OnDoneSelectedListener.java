package com.pacifico.adn.Views.Interface;

import android.widget.EditText;

/**
 * Created by vctrls3477 on 18/08/16.
 */
public interface OnDoneSelectedListener{
    void onDoneSelected(EditText editText);
}