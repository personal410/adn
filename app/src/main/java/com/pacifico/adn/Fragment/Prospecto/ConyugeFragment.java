package com.pacifico.adn.Fragment.Prospecto;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.CustomEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ConyugeFragment extends Fragment implements View.OnFocusChangeListener {
    private Calendar calMax = Calendar.getInstance();
    private Calendar calMin = Calendar.getInstance();
    private Calendar calFechaNacimiento;
    private CustomEditText edtPrimerNombre, edtApellidoPaterno, edtApellidoMaterno, edtFechaNacimiento, edtEdad, edtOcupacion, edtActual;
    private ADNActivity adnActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        adnActivity = (ADNActivity)getActivity();
        calMax.add(Calendar.YEAR, -18);
        calMin.add(Calendar.YEAR, -100);
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_conyuge, container, false);
        edtPrimerNombre = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtPrimerNombre);
        edtApellidoPaterno = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtApellidoPaterno);
        edtApellidoMaterno = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtApellidoMaterno);
        edtFechaNacimiento = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtFechaNacimiento);
        edtEdad = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtEdad);
        edtOcupacion = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtOcupacion);
        edtPrimerNombre.setOnFocusChangeListener(this);
        edtPrimerNombre.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(100)});
        edtApellidoPaterno.setOnFocusChangeListener(this);
        edtApellidoPaterno.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(80)});
        edtApellidoMaterno.setOnFocusChangeListener(this);
        edtApellidoMaterno.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(80)});
        edtFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calFechaNacimiento.set(Calendar.YEAR, year);
                        calFechaNacimiento.set(Calendar.MONTH, monthOfYear);
                        calFechaNacimiento.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateEdtFechaNacimiento();
                    }
                }, calFechaNacimiento.get(Calendar.YEAR), calFechaNacimiento.get(Calendar.MONTH), calFechaNacimiento.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.getDatePicker().setMinDate(calMin.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(calMax.getTimeInMillis());
                datePickerDialog.setTitle("Seleccione una fecha");
                datePickerDialog.show();
            }
        });
        edtOcupacion.setOnFocusChangeListener(this);
        edtOcupacion.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(100)});
        if(adnActivity.getConyugeBean() != null){
            FamiliarBean conyugeBean = adnActivity.getConyugeBean();
            edtPrimerNombre.setText(conyugeBean.getNombres());
            edtApellidoPaterno.setText(conyugeBean.getApellidoPaterno());
            edtApellidoMaterno.setText(conyugeBean.getApellidoMaterno());
            if(conyugeBean.getFechaNacimiento().length() > 0){
                String[] arrFechaNacimiento = conyugeBean.getFechaNacimiento().split("-");
                calFechaNacimiento = Calendar.getInstance();
                calFechaNacimiento.set(Integer.parseInt(arrFechaNacimiento[0]), Integer.parseInt(arrFechaNacimiento[1]) - 1, Integer.parseInt(arrFechaNacimiento[2]));
                updateEdtFechaNacimiento();
            }else{
                calFechaNacimiento = (Calendar)calMax.clone();
                calFechaNacimiento.add(Calendar.YEAR, -12);
            }
            edtOcupacion.setText(conyugeBean.getOcupacion());
        }else{
            calFechaNacimiento = (Calendar)calMax.clone();
            calFechaNacimiento.add(Calendar.YEAR, -12);
        }
        RelativeLayout relLayConyuge = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayConyuge);
        relLayConyuge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtActual != null) {
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(edtActual.getWindowToken(), 0);
                    edtActual.clearFocus();
                }
            }
        });
        return view;
    }
    private void updateEdtFechaNacimiento(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM, yyyy");
        edtFechaNacimiento.setText(simpleDateFormat.format(calFechaNacimiento.getTime()));
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calHoy = Calendar.getInstance();
        int diaHoy = calHoy.get(Calendar.DAY_OF_MONTH);
        int mesHoy = calHoy.get(Calendar.MONTH);
        int anioHoy = calHoy.get(Calendar.YEAR);
        int diaFecNac = calFechaNacimiento.get(Calendar.DAY_OF_MONTH);
        int mesFecNac = calFechaNacimiento.get(Calendar.MONTH);
        int anioFecNac = calFechaNacimiento.get(Calendar.YEAR);
        int edad = anioHoy - anioFecNac;
        if(mesHoy < mesFecNac){
            edad -= 1;
        }else if(mesHoy == mesFecNac){
            if(diaHoy < diaFecNac){
                edad -= 1;
            }
        }
        String nuevoFechaNacimiento = simpleDateFormat.format(calFechaNacimiento.getTime());
        if(!nuevoFechaNacimiento.equals(adnActivity.getConyugeBean().getFechaNacimiento())){
            adnActivity.getConyugeBean().setFechaNacimiento(nuevoFechaNacimiento);
            adnActivity.getConyugeBean().setEdad(edad);
            adnActivity.getConyugeBean().setFlagModificado(1);
        }
        edtEdad.setText(String.format("%d años", edad));
        if(edtFechaNacimiento.length() == 0){
            edtFechaNacimiento.setError(Constantes.MensajeErrorCampoVacio);
        }else{
            edtFechaNacimiento.setError(null);
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus){
        if(hasFocus){
            edtActual = (CustomEditText)v;
        }else{
            FamiliarBean conyugeBean = adnActivity.getConyugeBean();
            String textoActual = edtActual.getText().toString().trim();
            if(edtActual.equals(edtPrimerNombre)){
                edtPrimerNombre.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getNombres())){
                    conyugeBean.setNombres(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
                if(adnActivity.getProspectoBean().getFlagConyuge() == 1 && textoActual.length() == 0){
                    edtPrimerNombre.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtPrimerNombre.setError(null);
                }
            }else if(edtActual.equals(edtApellidoPaterno)){
                edtApellidoPaterno.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getApellidoPaterno())){
                    conyugeBean.setApellidoPaterno(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
                if(adnActivity.getProspectoBean().getFlagConyuge() == 1 && textoActual.length() == 0){
                    edtApellidoPaterno.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtApellidoPaterno.setError(null);
                }
            }else if(edtActual.equals(edtApellidoMaterno)){
                edtApellidoMaterno.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getApellidoMaterno())){
                    conyugeBean.setApellidoMaterno(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
            }else if(edtActual.equals(edtOcupacion)){
                edtOcupacion.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getOcupacion())){
                    conyugeBean.setOcupacion(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
                if(adnActivity.getProspectoBean().getFlagConyuge() == 1 && textoActual.length() == 0){
                    edtOcupacion.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtOcupacion.setError(null);
                }
            }
            edtActual = null;
        }
    }
    public void validarCampos(){
        ArrayList<CustomEditText> arrEdtTxts = new ArrayList<>();
        arrEdtTxts.add(edtPrimerNombre);
        arrEdtTxts.add(edtApellidoPaterno);
       /* arrEdtTxts.add(edtOcupacion);*/
        for(CustomEditText editText : arrEdtTxts){
            if(adnActivity.getProspectoBean().getFlagConyuge() == 1){
                if(editText.getText().toString().trim().length() == 0){
                    editText.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    editText.setError(null);
                }
            }else{
                editText.setError(null);
            }
        }
        if(adnActivity.getProspectoBean().getFlagConyuge() == 1){
            if(edtFechaNacimiento.getText().toString().length() == 0){
                edtFechaNacimiento.setError(Constantes.MensajeErrorCampoVacio);
            }else{
                edtFechaNacimiento.setError(null);
            }
        }else{
            edtFechaNacimiento.setError(null);
        }
    }
    public CustomEditText getEdtActual() {
        return edtActual;
    }
}