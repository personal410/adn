package com.pacifico.adn.Activity;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pacifico.adn.Adapters.PosiblesProspectosArrayAdapter;
import com.pacifico.adn.Model.Bean.AdnBean;
import com.pacifico.adn.Model.Bean.CitaBean;
import com.pacifico.adn.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.Model.Bean.ParametroBean;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.adn.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Model.Bean.ReunionInternaBean;
import com.pacifico.adn.Model.Controller.ADNController;
import com.pacifico.adn.Model.Controller.CitaReunionController;
import com.pacifico.adn.Model.Controller.FamiliarController;
import com.pacifico.adn.Model.Controller.ParametroController;
import com.pacifico.adn.Model.Controller.ProspectoController;
import com.pacifico.adn.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.adn.Model.Controller.ReferidoController;
import com.pacifico.adn.Network.RespuestaSincronizacionListener;
import com.pacifico.adn.Network.SincronizacionController;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.CustomEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BuscarProspectoActivity extends AppCompatActivity{
    private RelativeLayout relBuscarProspecto1, relMain ;
    private CustomEditText edtProspecto;
    private ListView lstPosiblesProspectos;
    private PosiblesProspectosArrayAdapter adapPosiblesProspectos;
    private ProspectoBean proSeleccionado;
    private ArrayList<ProspectoBean> arrProspectosDisponibles;
    private ProgressDialog progressDialog;
    private boolean cargarProspectoPorHora = true;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(com.pacifico.adn.R.layout.activity_buscar_prospecto);
        relBuscarProspecto1 = (RelativeLayout)findViewById(com.pacifico.adn.R.id.relBuscarProspecto1);
        edtProspecto = (CustomEditText)findViewById(com.pacifico.adn.R.id.edtProspecto);
        relMain = (RelativeLayout)findViewById(com.pacifico.adn.R.id.relMain);
        relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                edtProspecto.clearFocus();
            }
        });
        Intent intent = getIntent();
        if(intent != null){
            Bundle extras = intent.getExtras();
            if(extras != null){
                int tipoOperacion = extras.getInt(Constantes.TipoOperacion, -1);

                if(tipoOperacion == Constantes.OperacionRegularizarDatosADN || tipoOperacion == Constantes.OperacionMostrarADN){
                    int idProspectoDispositivo = extras.getInt(Constantes.ParametroExtraIdProspectoDispositivo, -1);
                    Log.i("TAG", "idProspectoDispositivo: " + idProspectoDispositivo);
                    if(idProspectoDispositivo != -1){
                        cargarProspectoPorHora = false;
                        proSeleccionado = ProspectoController.obtenerProspectoPorIdProspectoDispositivo(idProspectoDispositivo);
                        edtProspecto.setText(proSeleccionado.getNombreCompleto());
                    }
                }
            }
        }
        edtProspecto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus){
                float traslado = hasFocus ? -(relBuscarProspecto1.getY() - 20) : 0;
                Animation animation = new TranslateAnimation(0, 0, 0, traslado);
                animation.setDuration(500);
                animation.setFillAfter(true);
                relBuscarProspecto1.startAnimation(animation);
              /*  int traslado = hasFocus ? -100 : 484;
                final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)relBuscarProspecto1.getLayoutParams();
                ValueAnimator animator = ValueAnimator.ofInt(params.topMargin, traslado);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        params.topMargin = (int)animation.getAnimatedValue();
                        relBuscarProspecto1.requestLayout();
                    }
                });
                animator.setDuration(500);
                animator.start();*/
                if (!hasFocus) {
                    InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(edtProspecto.getWindowToken(), 0);
                    showOrHideLstPossibleProspects(false);
                }
            }
        });
        edtProspecto.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count){}
            @Override
            public void afterTextChanged(Editable s) {
                if(edtProspecto.hasFocus()){
                    filtrarProspectos();
                }
            }
        });
    }
    @Override
    protected void onResume(){
        super.onResume();
        arrProspectosDisponibles = ProspectoController.obtenerProspectosConCita();
        if(cargarProspectoPorHora){
            ProspectoBean prospectoBeanTemp = ProspectoController.obtenerProspectoConCitaActualProxima();
            if(prospectoBeanTemp != null){
                proSeleccionado = prospectoBeanTemp;
                edtProspecto.setText(proSeleccionado.getNombreCompleto());
            }
        }else{
            cargarProspectoPorHora = true;
        }
        verificarTiempoSinSincronizar();
    }
    @Override
    public void onBackPressed(){}
    private void showOrHideLstPossibleProspects(boolean show){
        if(lstPosiblesProspectos == null){
            lstPosiblesProspectos = new ListView(this);
            RelativeLayout.LayoutParams layParamsLstPossibleProspects = new RelativeLayout.LayoutParams(edtProspecto.getWidth()-16, 400);
            layParamsLstPossibleProspects.addRule(RelativeLayout.CENTER_HORIZONTAL);
            //layParamsLstPossibleProspects.addRule(RelativeLayout.ALIGN_RIGHT, com.pacifico.adn.R.id.relBuscarProspecto1);
            layParamsLstPossibleProspects.topMargin = 368;
            lstPosiblesProspectos.setLayoutParams(layParamsLstPossibleProspects);
            lstPosiblesProspectos.setBackgroundColor(getResources().getColor(com.pacifico.adn.R.color.colorBlanco));
            adapPosiblesProspectos = new PosiblesProspectosArrayAdapter(this);
            lstPosiblesProspectos.setAdapter(adapPosiblesProspectos);
            filtrarProspectos();
            lstPosiblesProspectos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                    proSeleccionado = adapPosiblesProspectos.getProspectByPosition(position);
                    edtProspecto.setText(proSeleccionado.getNombreCompleto());
                    edtProspecto.clearFocus();
                    showOrHideLstPossibleProspects(false);
                }
            });
        }
        if(show){
            if(lstPosiblesProspectos.getParent() == null){
                relMain.addView(lstPosiblesProspectos);
            }
        }else{
            relMain.removeView(lstPosiblesProspectos);
        }
    }
    public void start(View v){
        if(proSeleccionado == null){
            Toast.makeText(this, "Seleccione un prospecto", Toast.LENGTH_SHORT).show();
        }else{
            if(proSeleccionado.getNombreCompleto().equals(edtProspecto.getText().toString())) {
                Intent intentADN = new Intent(this, ADNActivity.class);
                intentADN.putExtra("prospecto", proSeleccionado);
                proSeleccionado = null;
                edtProspecto.setText("");
                startActivity(intentADN);
            }else{
                Toast.makeText(this, "Seleccione un prospecto", Toast.LENGTH_SHORT).show();

            }
        }
    }
    public void filtrarProspectos(){
        String prospecto = edtProspecto.getText().toString().toLowerCase();
        showOrHideLstPossibleProspects(prospecto.length() > 0);
        if(prospecto.length() > 0){
            ArrayList<ProspectoBean> arrProspectoFinal = new ArrayList<>();
            for(ProspectoBean prospectoBean : arrProspectosDisponibles){
                if(prospectoBean.getNombreCompleto().toLowerCase().contains(prospecto)){
                    arrProspectoFinal.add(prospectoBean);
                }
            }
            adapPosiblesProspectos.setArrProspectos(arrProspectoFinal);
        }
    }
    private void sincronizar(){
        SincronizacionController sincronizacionController = new SincronizacionController();
        if(progressDialog == null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
            @Override
            public void terminoSincronizacion(int codigo, String mensaje) {
                progressDialog.dismiss();
                if (codigo < 0) {
                    mostrarMensajeParaContinuar(mensaje);
                } else if (codigo == 1) {
                    mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                } else {
                    if (codigo == 4 || codigo == 6) {
                        Intent inicioSesionIntent = new Intent(BuscarProspectoActivity.this, InicioSesionActivity.class);
                        inicioSesionIntent.putExtra("etapa", (codigo - 4) / 2 + 1);
                        startActivity(inicioSesionIntent);
                    } else {
                        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                    }
                }
            }
        });
        int resultado = sincronizacionController.sincronizar(this);
        if(resultado == 0){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
        }else if(resultado == 2){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }
    private void mostrarMensajeParaContinuar(String mensaje){
        new AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //dialog.cancel();
                verificarTiempoSinSincronizar();
            }
        }).show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                sincronizar();
            }
        }
    }
    @Override
    protected void onPause(){
        super.onPause();
        edtProspecto.clearFocus();
    }

    public void verificarTiempoSinSincronizar(){
        double tiempo = Util.expiroTiempoSinSincronizacion();
        if (tiempo > 0){
            int cantObjectosSinEnviar = 0;
            ArrayList<ReunionInternaBean> arrReunionInternas = CitaReunionController.obtenerReunionInternasSinEnviar();
            cantObjectosSinEnviar += arrReunionInternas.size();
            ArrayList<ProspectoBean> arrProspectos = ProspectoController.obtenerProspectosSinEviar();
            cantObjectosSinEnviar += arrProspectos.size();
            ArrayList<ProspectoMovimientoEtapaBean> arrProspectoMovimientoEtapas = ProspectoMovimientoEtapaController.obtenerProspectoMovimientoEtapaSinEnviar();
            cantObjectosSinEnviar += arrProspectoMovimientoEtapas.size();
            final ArrayList<AdnBean> arrAdns = ADNController.obtenerADNSinEviar();
            cantObjectosSinEnviar += arrAdns.size();
            ArrayList<FamiliarBean> arrFamiliares = FamiliarController.obtenerFamiliaresSinEviar();
            cantObjectosSinEnviar += arrFamiliares.size();
            ArrayList<CitaBean> arrCitas = CitaReunionController.obtenerCitasSinEnviar();
            cantObjectosSinEnviar += arrCitas.size();
            ArrayList<CitaMovimientoEstadoBean> arrCitaMovimientoEstados = CitaReunionController.obtenerCitasMovimientoEstadoSinEnviar();
            cantObjectosSinEnviar += arrCitaMovimientoEstados.size();
            ArrayList<ReferidoBean> arrReferidos = ReferidoController.obtenerReferidosSinEnviar();
            cantObjectosSinEnviar += arrReferidos.size();
            ArrayList<RecordatorioLlamadaBean> arrRecordatorioLlamadas = CitaReunionController.obtenerRecordatorioLlamadasSinEnviar();
            cantObjectosSinEnviar += arrRecordatorioLlamadas.size();
            if (cantObjectosSinEnviar > 0){
                ParametroBean tiempoSinSincronizacionParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(16);
                double tiempoSinSincronizacionExtra = tiempoSinSincronizacionParametroBean.getValorNumerico() * 60 * 60 * 1000;
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MILLISECOND, (int)(tiempoSinSincronizacionExtra - tiempo));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM 'a las' hh:mm a");
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog
                        .setTitle("Alerta")
                        .setMessage("Por favor, realiza la sincronización hasta el " + simpleDateFormat.format(calendar.getTime())+". Después de esta hora se bloquearán las aplicaciones.")
                        .setPositiveButton("Sincronizar Ahora", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sincronizar();
                            }
                        }).setCancelable(false);
                if (tiempo < tiempoSinSincronizacionExtra){
                    alertDialog.setNegativeButton("Sincronizar mas tarde", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                        }
                    });
                }
                alertDialog.show();
            }
        }
    }
}