package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;
import com.pacifico.adn.Views.Interface.OnStepperDoneSelectedListener;

public class ActivosFragment extends Fragment implements OnStepperChangeValueListener, RadioGroup.OnCheckedChangeListener, OnStepperDoneSelectedListener, StepperView.OnFocusChangeListener {
    private StepperView stepEfectivoAhorros;
    private StepperView stepPropiedades;
    private StepperView stepVehiculos;
    private StepperView stepActual;
    private RadioGroup rgMonedaEfectivoAhorros, rgMonedaPropiedades;
    private ADNActivity actADN;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_activos, container, false);
        stepEfectivoAhorros = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepEfectivoAhorros);
        stepPropiedades = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepPropiedades);
        stepVehiculos = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepVehiculos);
        rgMonedaEfectivoAhorros = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaEfectivoAhorros);
        rgMonedaPropiedades = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaPropiedades);
        RadioGroup rgMonedaVehiculos = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaVehiculos);
        stepEfectivoAhorros.setValorActual(actADN.getAdnBean().getEfectivoAhorros());
        stepPropiedades.setValorActual(actADN.getAdnBean().getPropiedades());
        stepVehiculos.setValorActual(actADN.getAdnBean().getVehiculos());
        rgMonedaEfectivoAhorros.check(actADN.getAdnBean().getMonedaEfectivoAhorros() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb11 : com.pacifico.adn.R.id.rb12);
        rgMonedaPropiedades.check(actADN.getAdnBean().getMonedaPropiedades() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb21 : com.pacifico.adn.R.id.rb22);
        rgMonedaVehiculos.check(actADN.getAdnBean().getMonedaVehiculos() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb31 : com.pacifico.adn.R.id.rb32);
        stepEfectivoAhorros.setOnStepperChangeValueListener(this);
        stepPropiedades.setOnStepperChangeValueListener(this);
        stepVehiculos.setOnStepperChangeValueListener(this);
        rgMonedaEfectivoAhorros.setOnCheckedChangeListener(this);
        rgMonedaPropiedades.setOnCheckedChangeListener(this);
        rgMonedaVehiculos.setOnCheckedChangeListener(this);
        stepEfectivoAhorros.setOnStepperDoneSelectedListener(this);
        stepPropiedades.setOnStepperDoneSelectedListener(this);
        stepEfectivoAhorros.setOnFocusChangeListener(this);
        stepPropiedades.setOnFocusChangeListener(this);
        stepVehiculos.setOnFocusChangeListener(this);
        RelativeLayout relLayActivos = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayActivos);
        relLayActivos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(stepActual != null){
                    stepActual.clearFocus();
                }
            }
        });
        return view;
    }
    @Override
    public void onChangeValue(StepperView stepView){
        int valorActual = stepView.getValorActual();
        if(stepView == stepEfectivoAhorros){
            if (valorActual != actADN.getAdnBean().getEfectivoAhorros()){
                actADN.getAdnBean().setEfectivoAhorros(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else if(stepView == stepPropiedades){
            if(valorActual != actADN.getAdnBean().getPropiedades()){
                actADN.getAdnBean().setPropiedades(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else{
            if(valorActual != actADN.getAdnBean().getVehiculos()){
                actADN.getAdnBean().setVehiculos(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }
        calcularTotal();
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group == rgMonedaEfectivoAhorros){
            actADN.getAdnBean().setMonedaEfectivoAhorros(checkedId == com.pacifico.adn.R.id.rb11 ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else if(group == rgMonedaPropiedades){
            actADN.getAdnBean().setMonedaPropiedades(checkedId == com.pacifico.adn.R.id.rb21 ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else{
            actADN.getAdnBean().setMonedaVehiculos(checkedId == com.pacifico.adn.R.id.rb31 ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }
        calcularTotal();
    }
    public void calcularTotal(){
        double factorEfectivoAhorros = actADN.getAdnBean().getMonedaEfectivoAhorros() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorPropiedades = actADN.getAdnBean().getMonedaPropiedades() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorVehiculos = actADN.getAdnBean().getMonedaVehiculos() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double montoTotal = stepEfectivoAhorros.getValorActual() / factorEfectivoAhorros + stepPropiedades.getValorActual() / factorPropiedades + stepVehiculos.getValorActual() / factorVehiculos;
        actADN.getAdnBean().setTotalActivoRealizable(Util.redondearNumero(montoTotal, 2));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    @Override
    public void onStepperDoneSelected(StepperView stepperView){
        if(stepperView.equals(stepEfectivoAhorros)){
            stepPropiedades.requestStepperFocus();
        }else{
            stepVehiculos.requestStepperFocus();
        }
    }
    @Override
    public void onFocusChange(boolean hasFocus, StepperView stepperView) {
        if(hasFocus){
            stepActual = stepperView;
        }else{
            stepActual = null;
        }
    }
    public void clearStepActual(){
        if(stepActual != null){
            stepActual.clearFocus();
        }
    }
}