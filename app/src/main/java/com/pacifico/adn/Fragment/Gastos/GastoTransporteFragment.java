package com.pacifico.adn.Fragment.Gastos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Constantes;

import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;

public class GastoTransporteFragment extends Fragment{
    private ADNActivity actADN;
    private StepperView stepGastoVehiculosTransporte, stepActual;
    private GastosMensualFragment gastosMensualFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_gasto_transporte, container, false);
        RadioGroup rgMonedaGastoVehiculosTransporte = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaGastoVehiculosTransporte);
        stepGastoVehiculosTransporte = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepGastoVehiculosTransporte);
        rgMonedaGastoVehiculosTransporte.check(actADN.getAdnBean().getMonedaGastoVehiculoTransporte() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb11 : com.pacifico.adn.R.id.rb12);
        stepGastoVehiculosTransporte.setValorActual(actADN.getAdnBean().getGastoVehiculoTransporte());
        rgMonedaGastoVehiculosTransporte.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                actADN.getAdnBean().setMonedaGastoVehiculoTransporte((checkedId == com.pacifico.adn.R.id.rb11) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
                calcularTotal();
            }
        });
        stepGastoVehiculosTransporte.setOnStepperChangeValueListener(new OnStepperChangeValueListener() {
            @Override
            public void onChangeValue(StepperView stepView) {
                int valorActual = stepView.getValorActual();
                if (valorActual != actADN.getAdnBean().getGastoVehiculoTransporte()) {
                    actADN.getAdnBean().setGastoVehiculoTransporte(valorActual);
                    actADN.getAdnBean().setFlagModificado(1);
                    actADN.verificarTipoCambio();
                    calcularTotal();
                }
            }
        });
        stepGastoVehiculosTransporte.setOnFocusChangeListener(new StepperView.OnFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus, StepperView stepperView) {
                if (hasFocus){
                    stepActual = stepperView;
                }else{
                    stepActual = null;
                }
            }
        });
        RelativeLayout relLayGastoTransporte = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayGastoTransporte);
        relLayGastoTransporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stepActual == null){
                    gastosMensualFragment.getFlujoADNFragment().clearFocusStepTotalGastoFamiliares();
                }else{
                    stepGastoVehiculosTransporte.clearFocus();
                }
            }
        });
        return view;
    }
    public void calcularTotal(){
        double factorGastoVehiculosTransporte = actADN.getAdnBean().getMonedaGastoVehiculoTransporte() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double montoTotal = stepGastoVehiculosTransporte.getValorActual() / factorGastoVehiculosTransporte;
        //montoTotal = Util.redondearNumero(montoTotal, 2);
        actADN.getAdnBean().setTotalGastoTransporte(montoTotal);
        double totalGastos = actADN.getAdnBean().getTotalGastoVivienda() + actADN.getAdnBean().getTotalGastoSaludEducacion() + montoTotal + actADN.getAdnBean().getTotalGastoOtros();
        actADN.getAdnBean().setTotalGastoFamiliarMensual(Util.redondearNumero(totalGastos, 2));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    public void setGastosMensualFragment(GastosMensualFragment gastosMensualFragment) {
        this.gastosMensualFragment = gastosMensualFragment;
    }
    public StepperView getStepActual() {
        return stepActual;
    }
}