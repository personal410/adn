package com.pacifico.adn.Util;

import android.content.Context;
import android.graphics.Typeface;

import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public  class Fuente {
    public static void setFuenteBd(Context context,TextView txt){
        String font_path = "fonts/foco_std_bd_webfont.ttf";
        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);
        txt.setTypeface(TF);
    }
    public static void setFuenteButtonRg(Context context,Button txt,Button txt2){
        String font_path = "fonts/foco_std_rg_webfont.ttf";
        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);
        txt.setTypeface(TF);
        txt2.setTypeface(TF);
    }
    public static void setFuenteRadioRg(Context context, RadioButton radio){
        String font_path = "fonts/foco_std_rg_webfont.ttf";
        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);
        radio.setTypeface(TF);
    }

    public static void setFuenteRg(Context context, TextView rb){
        String font_path = "fonts/foco_std_rg_webfont.ttf";
        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);
        rb.setTypeface(TF);
    }
    public static void setFuenteRgEdit(Context context, EditText rb){
        String font_path = "fonts/foco_std_rg_webfont.ttf";
        Typeface tf = Typeface.createFromAsset(context.getAssets(), font_path);
        rb.setTypeface(tf);
    }
    public static Typeface obtenerFuenteRegular(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts/foco_std_rg_webfont.ttf");
    }

}