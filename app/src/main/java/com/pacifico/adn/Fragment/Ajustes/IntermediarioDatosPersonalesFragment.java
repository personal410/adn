package com.pacifico.adn.Fragment.Ajustes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pacifico.adn.Model.Bean.IntermediarioBean;
import com.pacifico.adn.Model.Controller.IntermediarioController;
import com.pacifico.adn.R;

import static com.pacifico.adn.Util.Util.capitalizedString;
import static com.pacifico.adn.Util.Util.strNULL;

public class IntermediarioDatosPersonalesFragment extends Fragment{
    private IntermediarioBean intermediarioBean = IntermediarioController.obtenerIntermediario();
    String Fecha;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_intermediario_datos_personales, container, false);
        TextView Nombres = (TextView)view.findViewById(R.id.Nombres);
        TextView codigoAsesor = (TextView)view.findViewById(R.id.CodAsesor);
        TextView DocIdentidad = (TextView)view.findViewById(R.id.DocIden);
        TextView Gu = (TextView)view.findViewById(R.id.Gu);
        TextView Ga = (TextView)view.findViewById(R.id.Ga);
        TextView Agencia = (TextView)view.findViewById(R.id.Agencia);
        TextView correoElectronico = (TextView)view.findViewById(R.id.Email);
        TextView telfCelu = (TextView)view.findViewById(R.id.tefCel);
        TextView telfFijo = (TextView)view.findViewById(R.id.tefFijo);
        TextView FechaPromo = (TextView)view.findViewById(R.id.fechProm);
        TextView catAsesor = (TextView)view.findViewById(R.id.CatAsesor);
        Fecha();

        Nombres.setText(capitalizedString(intermediarioBean.getNombreRazonSocial()));
        codigoAsesor.setText(strNULL(intermediarioBean.getCodigoIntermediario()));
        DocIdentidad.setText(strNULL(intermediarioBean.getDocumentoIdentidad()));
        Gu.setText(capitalizedString(strNULL(intermediarioBean.getNombreGU())));
        Ga.setText(capitalizedString(strNULL(intermediarioBean.getNombreGA())));
        Agencia.setText(strNULL(intermediarioBean.getDescripcionAgencia()));
        correoElectronico.setText(strNULL(intermediarioBean.getCorreoElectronico()));
        telfCelu.setText(strNULL(intermediarioBean.getTelefonoCelular()));
        telfFijo.setText(strNULL(intermediarioBean.getTelefonoFijo()));
        FechaPromo.setText(Fecha);
        catAsesor.setText(strNULL(intermediarioBean.getDescripcionCategoria()));
        return view;
    }

    public void Fecha(){
        if(intermediarioBean.getFechaPromocion()!=null) {
            String hora = intermediarioBean.getFechaPromocion();
            String Hora[] = hora.split("-");
            Fecha = Hora[2]+"/"+Hora[1]+"/"+Hora[0];
        }
    }

}