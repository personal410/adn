package com.pacifico.adn.Model.Controller;

import android.database.Cursor;

import com.pacifico.adn.Activity.ADNApplication;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.adn.Model.Bean.TablaIdentificadorBean;
import com.pacifico.adn.Persistence.DatabaseConstants;
import com.dsbmobile.dsbframework.controller.persistence.Entity;

import java.util.ArrayList;

public class ProspectoMovimientoEtapaController{
    public static void guardarProspectoMovimientoEtapa(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean){
        TablaIdentificadorBean prospectoMovimientoTablaIdentificadorBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA);
        int lastid = prospectoMovimientoTablaIdentificadorBean.getIdentity() + 1;
        prospectoMovimientoEtapaBean.setIdMovimientoDispositivo(lastid);
        long row = ProspectoMovimientoEtapaBean.tableHelper.insertEntity(prospectoMovimientoEtapaBean);
        if(row != -1){
            prospectoMovimientoTablaIdentificadorBean.setIdentity(lastid);
            TablaIdentificadorController.actualizarTablaIdentificador(prospectoMovimientoTablaIdentificadorBean);
        }
    }
    public static void guardarListaProspectoMovimientoEtapa(ArrayList<ProspectoMovimientoEtapaBean> listaProspectoMovimientoEtapaBean){
        if(listaProspectoMovimientoEtapaBean != null){
            for(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean : listaProspectoMovimientoEtapaBean){
                if(prospectoMovimientoEtapaBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(prospectoMovimientoEtapaBean.getIdProspecto());
                    prospectoMovimientoEtapaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                ProspectoMovimientoEtapaBean.tableHelper.insertEntity(prospectoMovimientoEtapaBean);
            }
        }
    }
    public static int obtenerMaximoIdProspectoMovimientoDispositivo(){
        int idProspectoMovimientoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDMOVIMIENTODISPOSITIVO) FROM " + DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA;
        Cursor cursor = ADNApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idProspectoMovimientoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idProspectoMovimientoDispositivoMax;
    }
    public static ArrayList<ProspectoMovimientoEtapaBean> obtenerProspectoMovimientoEtapaSinEnviar(){
        ArrayList<Entity> arrBeans = ProspectoMovimientoEtapaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ProspectoMovimientoEtapaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((ProspectoMovimientoEtapaBean) entity);
        }
        return arrBeanFinal;
    }
    public static ProspectoMovimientoEtapaBean obtenerProspectoMovimientoEtapaPorIdMovimientoDispositivo(int idMovimientoDispositivo){
        String[] parametros = new String[]{Integer.toString(idMovimientoDispositivo)};
        ArrayList<Entity> arrProspectoMovimientoEtapas = ProspectoMovimientoEtapaBean.tableHelper.getEntities("IDMOVIMIENTODISPOSITIVO = ?", parametros);
        if(arrProspectoMovimientoEtapas.size() > 0){
            return (ProspectoMovimientoEtapaBean)arrProspectoMovimientoEtapas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarProspectoMovimientoEtapa(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean){
        String[] parametros = new String[]{Integer.toString(prospectoMovimientoEtapaBean.getIdMovimientoDispositivo())};
        ProspectoMovimientoEtapaBean.tableHelper.updateEntity(prospectoMovimientoEtapaBean, "IDMOVIMIENTODISPOSITIVO = ?", parametros);
    }
    public static void guardarProspectoMovimientoEtapa_GenerarIDDispositivo(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean) {
        TablaIdentificadorBean identificadorProspectoMovimientoEtapaBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA);
        prospectoMovimientoEtapaBean.setIdMovimientoDispositivo(identificadorProspectoMovimientoEtapaBean.getIdentity()+1);
        long row = ProspectoMovimientoEtapaBean.tableHelper.insertEntity(prospectoMovimientoEtapaBean);
        if(row != -1){
            identificadorProspectoMovimientoEtapaBean.setIdentity(identificadorProspectoMovimientoEtapaBean.getIdentity()+1);
            TablaIdentificadorController.actualizarTablaIdentificador(identificadorProspectoMovimientoEtapaBean);
        }
    }
}