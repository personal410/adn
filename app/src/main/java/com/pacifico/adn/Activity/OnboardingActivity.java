package com.pacifico.adn.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.pacifico.adn.Adapters.OnboardingPagerAdapter;
import com.pacifico.adn.Fragment.IndicadorPaginaFragment;
import com.pacifico.adn.Model.Bean.AdnBean;
import com.pacifico.adn.Model.Bean.CalendarioBean;
import com.pacifico.adn.Model.Bean.CitaBean;
import com.pacifico.adn.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.adn.Model.Bean.DispositivoBean;
import com.pacifico.adn.Model.Bean.EntidadBean;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.Model.Bean.IntermediarioBean;
import com.pacifico.adn.Model.Bean.MensajeSistemaBean;
import com.pacifico.adn.Model.Bean.ParametroBean;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.adn.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Model.Bean.ReunionInternaBean;
import com.pacifico.adn.Model.Bean.TablaIdentificadorBean;
import com.pacifico.adn.Model.Bean.TablaIndiceBean;
import com.pacifico.adn.Model.Bean.TablaTablasBean;
import com.pacifico.adn.Model.Controller.ADNController;
import com.pacifico.adn.Model.Controller.CitaReunionController;
import com.pacifico.adn.Model.Controller.DispositivoController;
import com.pacifico.adn.Model.Controller.EntidadController;
import com.pacifico.adn.Model.Controller.FamiliarController;
import com.pacifico.adn.Model.Controller.IntermediarioController;
import com.pacifico.adn.Model.Controller.ParametroController;
import com.pacifico.adn.Model.Controller.ProspectoController;
import com.pacifico.adn.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.adn.Model.Controller.ReferidoController;
import com.pacifico.adn.Model.Controller.TablaIdentificadorController;
import com.pacifico.adn.Model.Controller.TablasGeneralesController;
import com.pacifico.adn.Network.Response.Comun.IntermediarioResponse;
import com.pacifico.adn.Network.Response.Comun.ProcesoSincronizacionResponse;

import com.pacifico.adn.Network.Response.GetData.GetDataResponse;
import com.pacifico.adn.Network.RestMethods;
import com.pacifico.adn.Network.SincronizacionController;
import com.pacifico.adn.Persistence.DatabaseConstants;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.DataMapperResponse;
import com.pacifico.adn.Util.Util;
import com.scottyab.aescrypt.AESCrypt;
import com.squareup.okhttp.OkHttpClient;

import java.net.NetworkInterface;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class OnboardingActivity extends AppCompatActivity{
    private IndicadorPaginaFragment indicadorPaginaFragment;
    private ViewPager pagADN;
    private String login, token, numeroSerie, marca, modelo, so, codigoIntermediario, mac;
    boolean puedeAvanzar = false;
    boolean irBuscarProspecto = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            irBuscarProspecto = extras.getBoolean("irBuscarProspecto", true);
        }
        setContentView(com.pacifico.adn.R.layout.activity_onboarding);
        pagADN = (ViewPager)findViewById(com.pacifico.adn.R.id.pagContenedor);
        OnboardingPagerAdapter onboardingPagerAdapter = new OnboardingPagerAdapter(getSupportFragmentManager());
        pagADN.setAdapter(onboardingPagerAdapter);
        indicadorPaginaFragment = (IndicadorPaginaFragment)getSupportFragmentManager().findFragmentById(com.pacifico.adn.R.id.fragIndicadorPagina);
        pagADN.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){}
            @Override
            public void onPageSelected(int position){
                indicadorPaginaFragment.actualizarIndicadorPagina(position);
            }
            @Override
            public void onPageScrollStateChanged(int state){}
        });
        if(IntermediarioController.obtenerIntermediario() == null){
            login = extras.getString("login");
            token = extras.getString("token");
            numeroSerie = extras.getString("numeroSerie");
            codigoIntermediario = extras.getString("codigoIntermediario");
            marca = Build.MANUFACTURER;
            modelo = Build.MODEL;
            String version = Build.VERSION.RELEASE;
            so = "Android" + version;
            mac = getWifiMacAddress().replace(":", "");
            obtenerInformacion();
        }else{
            puedeAvanzar = true;
        }
    }
    public void actualizarPaginaConDelta(int delta){
        int pagina = pagADN.getCurrentItem() + delta;
        pagADN.setCurrentItem(pagina);
    }
    @Override
    public void onBackPressed() {
        if(pagADN.getCurrentItem() == 0){
            super.onBackPressed();
        }else{
            pagADN.setCurrentItem(pagADN.getCurrentItem() - 1);
        }
    }
    public void empezar(View v){
        if(puedeAvanzar){
            if(irBuscarProspecto){
                Intent buscarProspectoIntent = new Intent(this, BuscarProspectoActivity.class);
                startActivity(buscarProspectoIntent);
            }else{
                finish();
            }
        }else{
            Util.mostrarAlertaConTitulo("Alerta", "Se encuentra en proceso de sincronización. Espere un momento", this);
        }
    }
    public void obtenerInformacion(){
        OkHttpClient okHttpClient = new OkHttpClient();
        try{
            SincronizacionController sincronizacionController = new SincronizacionController();
            okHttpClient.setSslSocketFactory(sincronizacionController.getSSLSocketFactory(this));
        }catch(Exception e){}
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constantes.URLBase).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        RestMethods restMethods = retrofit.create(RestMethods.class);
        Call<GetDataResponse> respuesta = restMethods.GetData(login, codigoIntermediario, token, marca, modelo, so, mac, numeroSerie);
        respuesta.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Response<GetDataResponse> response, Retrofit retrofit){
                Log.i("TAG", "response iniciarSesion");
                GetDataResponse dataResponse = response.body();
                if(dataResponse == null){
                    Util.mostrarAlertaConTitulo("Alerta", "Hubo un error", OnboardingActivity.this);
                }else{
                    int codigoResultado = Integer.parseInt(dataResponse.getResultCode());
                    if(codigoResultado == 1){
                        ProcesoSincronizacionResponse procesoSincronizacionResponse = dataResponse.getProcesoSicronizacion();
                        if(procesoSincronizacionResponse.getFlagExito().equals("true")){
                            ArrayList<ParametroBean> listaParametrosBean = DataMapperResponse.transformListParametro(dataResponse.getParametro());
                            ParametroController.guardarListaParametros(listaParametrosBean);
                            ArrayList<TablaIndiceBean> listaTablaIndicesBean = DataMapperResponse.transformListTablaIndice(dataResponse.getTablaIndice());
                            TablasGeneralesController.guardarListaTablaIndice(listaTablaIndicesBean);
                            ArrayList<TablaTablasBean> listaTablaTablasBean = DataMapperResponse.transformListTablaTablas(dataResponse.getTablaTablas());
                            TablasGeneralesController.guardarListaTablaTablas(listaTablaTablasBean);
                            ArrayList<CalendarioBean> listaCalendarioBean = DataMapperResponse.transformListCalendario(dataResponse.getCalendario());
                            TablasGeneralesController.guardarListaCalendario(listaCalendarioBean);
                            ArrayList<MensajeSistemaBean> listaMensajeSistemaBean = DataMapperResponse.transformListMensajeSistema(dataResponse.getMensajeSistema());
                            TablasGeneralesController.guardarListaMensajesSistemas(listaMensajeSistemaBean);
                            ArrayList<EntidadBean> listaEntidadBean = DataMapperResponse.transformListEntidad(dataResponse.getEntidad());
                            EntidadController.guardaListaEntidad(listaEntidadBean);
                            IntermediarioResponse intermediarioResponse = dataResponse.getConsolidadoIntermediario().get(0);
                            IntermediarioBean intermediarioBean = DataMapperResponse.transform(intermediarioResponse);
                            try{
                                intermediarioBean.setLogin(AESCrypt.encrypt(Constantes.SemillaEncriptacion, login));
                                intermediarioBean.setToken(AESCrypt.encrypt(Constantes.SemillaEncriptacion, token));
                            }catch(GeneralSecurityException e){
                                e.printStackTrace();
                            }
                            IntermediarioController.guardarIntermediario(intermediarioBean);
                            ArrayList<ReunionInternaBean> listaReunionInternaBean = DataMapperResponse.transformListReunion(dataResponse.getReunion());
                            CitaReunionController.guardarListaReuniones(listaReunionInternaBean);
                            ArrayList<ProspectoBean> listaProspectosBean = DataMapperResponse.transformListProspecto(dataResponse.getProspecto());
                            ProspectoController.guardarListaProspectos(listaProspectosBean);
                            ArrayList<ProspectoMovimientoEtapaBean> listaProspectoMovimientoEtapaBean = DataMapperResponse.transformListProspectoMovimientoEtapa(dataResponse.getProspectoMovimientoEtapa());
                            ProspectoMovimientoEtapaController.guardarListaProspectoMovimientoEtapa(listaProspectoMovimientoEtapaBean);
                            ArrayList<AdnBean> listaAdnsBean = DataMapperResponse.transformListAdn(dataResponse.getADN());
                            ADNController.guardarListaADNs(listaAdnsBean);
                            ArrayList<FamiliarBean> listaFamiliaresBean = DataMapperResponse.transformListFamiliar(dataResponse.getFamiliar());
                            FamiliarController.guardarListaFamiliar(listaFamiliaresBean);
                            ArrayList<CitaBean> listaCitaBean = DataMapperResponse.transformListCita(dataResponse.getCita());
                            CitaReunionController.guardarListaCitas(listaCitaBean);
                            ArrayList<CitaMovimientoEstadoBean> listaCitaMovimientoEstadoBean = DataMapperResponse.transformListCitaMovimientoEstado(dataResponse.getCitaMovimientoEstado());
                            CitaReunionController.guardarListaCitaMovimientoEstado(listaCitaMovimientoEstadoBean);
                            ArrayList<ReferidoBean> listaReferidosBean = DataMapperResponse.transformListReferido(dataResponse.getReferido());
                            ReferidoController.guardaListaReferido(listaReferidosBean);
                            ArrayList<RecordatorioLlamadaBean> listaRecordatorioLlamadoBean = DataMapperResponse.transformListRecordatorioLlamadaBean(dataResponse.getRecordatorioLlamada());
                            CitaReunionController.guardarListaRecordatorioLlamadas(listaRecordatorioLlamadoBean);
                            DispositivoBean dispositivoBean = new DispositivoBean();
                            dispositivoBean.setIdDispositivo(1);
                            dispositivoBean.setIdIntermediario(intermediarioBean.getCodigoIntermediario());
                            dispositivoBean.setMarca(marca);
                            dispositivoBean.setModelo(modelo);
                            dispositivoBean.setSistemaOperativo(so);
                            dispositivoBean.setMAC(mac);
                            dispositivoBean.setNumeroSerie(numeroSerie);
                            dispositivoBean.setFechaCreacion(Util.obtenerFechaActual());
                            dispositivoBean.setFechaUltimaSincronizacion(procesoSincronizacionResponse.getFechaSincronizacion());
                            dispositivoBean.setFlagNuevaMigracionCartera(0);
                            DispositivoController.guardarDispositivo(dispositivoBean);

                            TablaIdentificadorBean citaIdentificadorBean = new TablaIdentificadorBean();
                            citaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_CITA);
                            citaIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdCitaDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(citaIdentificadorBean);

                            TablaIdentificadorBean citaMovimientoIdentificadorBean = new TablaIdentificadorBean();
                            citaMovimientoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO);
                            citaMovimientoIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdCitaMovimientoDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(citaMovimientoIdentificadorBean);

                            TablaIdentificadorBean familiarIdentificadorBean = new TablaIdentificadorBean();
                            familiarIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_FAMILIAR);
                            familiarIdentificadorBean.setIdentity(FamiliarController.obtenerMaximoIdFamiliarDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(familiarIdentificadorBean);

                            TablaIdentificadorBean referidoIdentificadorBean = new TablaIdentificadorBean();
                            referidoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_REFERIDO);
                            referidoIdentificadorBean.setIdentity(ReferidoController.obtenerMaximoIdReferidoDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(referidoIdentificadorBean);

                            TablaIdentificadorBean reunionInternaIdentificadorBean = new TablaIdentificadorBean();
                            reunionInternaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_REUNION_INTERNA);
                            reunionInternaIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdReunionInternaDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(reunionInternaIdentificadorBean);

                            TablaIdentificadorBean prospectoMovimientoIdentificadorBean = new TablaIdentificadorBean();
                            prospectoMovimientoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA);
                            prospectoMovimientoIdentificadorBean.setIdentity(ProspectoMovimientoEtapaController.obtenerMaximoIdProspectoMovimientoDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(prospectoMovimientoIdentificadorBean);

                            TablaIdentificadorBean recordatorioLlamadaIdentificadorBean = new TablaIdentificadorBean();
                            recordatorioLlamadaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_RECORDATORIO_LLAMADA);
                            recordatorioLlamadaIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdRecordatorioLlamadaDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(recordatorioLlamadaIdentificadorBean);

                            ArrayList<ProspectoBean> arrProspectosSinProspectoDispositivo = ProspectoController.obtenerProspectoSinProspectoDispositivo();
                            int idProspectoDispositivo = ProspectoController.obtenerMaximoIdProspectoDispositivo();
                            for(ProspectoBean prospectoBean : arrProspectosSinProspectoDispositivo){
                                idProspectoDispositivo++;
                                prospectoBean.setIdProspectoDispositivo(idProspectoDispositivo);
                                prospectoBean.setFlagEnviado(0);
                                prospectoBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                                ProspectoController.actualizarProspectoSinProspectoDispositivo(prospectoBean);
                            }

                            TablaIdentificadorBean prospectoIdentificadorBean = new TablaIdentificadorBean();
                            prospectoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_PROSPECTO);
                            prospectoIdentificadorBean.setIdentity(idProspectoDispositivo);
                            TablaIdentificadorController.actualizarTablaIdentificador(prospectoIdentificadorBean);
                            puedeAvanzar = true;
                            Log.i("TAG", "sincronizacion completa");
                        }else{
                            Util.mostrarAlertaConTitulo("Alerta", "Hubo un problema en la sincronización", OnboardingActivity.this);
                        }
                    }else{
                        Util.mostrarAlertaConTitulo("Error", dataResponse.getResultMessage(), OnboardingActivity.this);
                    }
                }
            }
            @Override
            public void onFailure(Throwable t){
                AlertDialog.Builder builder = new AlertDialog.Builder(OnboardingActivity.this);
                builder.setTitle("Hubo un error en la conexion. Se reintentara la sincronización");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        obtenerInformacion();
                    }
                });
                builder.create().show();
            }
        });
    }
    public static String getWifiMacAddress(){
        try{
            String interfaceName = "wlan0";
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for(NetworkInterface intf : interfaces) {
                if(!intf.getName().equalsIgnoreCase(interfaceName)){
                    continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if(mac == null){
                    return "";
                }
                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) {
                    buf.append(String.format("%02X:", aMac));
                }
                if (buf.length()>0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        }catch(Exception ex){}
        return "";
    }
}