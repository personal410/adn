package com.pacifico.adn.Fragment.Prospecto;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.ProspectoPagerAdapter;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.Model.Bean.TablaIdentificadorBean;
import com.pacifico.adn.Model.Controller.FamiliarController;
import com.pacifico.adn.Model.Controller.ProspectoController;
import com.pacifico.adn.Model.Controller.TablaIdentificadorController;
import com.pacifico.adn.Persistence.DatabaseConstants;
import com.pacifico.adn.Util.Fuente;
import com.pacifico.adn.Util.Util;

import java.util.ArrayList;

public class ProspectoFragment extends Fragment{
    private ViewPager pagProspecto;
    private ADNActivity adnActivity;
    private DatosPersonalesFragment datosPersonalesFragment = new DatosPersonalesFragment();
    private ConyugeFragment conyugeFragment = new ConyugeFragment();
    private HijosFragment hijosFragment = new HijosFragment();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        adnActivity = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_prospecto, container, false);
        pagProspecto = (ViewPager)view.findViewById(com.pacifico.adn.R.id.pagProspectInfo);
        ImageButton imgBtnSiguiente = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnSiguiente);
        TabLayout tblTabs = (TabLayout)view.findViewById(com.pacifico.adn.R.id.tblTabs);
        pagProspecto.setOffscreenPageLimit(3);
        ProspectoPagerAdapter adapter = new ProspectoPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(datosPersonalesFragment);
        adapter.addFrag(conyugeFragment);
        adapter.addFrag(hijosFragment);
        pagProspecto.setAdapter(adapter);
        tblTabs.setupWithViewPager(pagProspecto);
        RelativeLayout rel1 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt1 = (TextView)rel1.findViewById(com.pacifico.adn.R.id.txt1);
        txt1.setText("DATOS PERSONALES");
        Fuente.setFuenteRg(getActivity(), txt1);
        txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorCasiNegro100));
        ImageView imgView1 = (ImageView)rel1.findViewById(com.pacifico.adn.R.id.img1);
        imgView1.setImageResource(com.pacifico.adn.R.drawable.ic_datos_personales_50dp);
        RelativeLayout rel2 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt2 = (TextView)rel2.findViewById(com.pacifico.adn.R.id.txt1);
        txt2.setText("CÓNYUGE");
        Fuente.setFuenteRg(getActivity(), txt2);
        ImageView imgView2 = (ImageView)rel2.findViewById(com.pacifico.adn.R.id.img1);
        imgView2.setImageResource(com.pacifico.adn.R.drawable.ic_conyuge_50dp);
        imgView2.setColorFilter(Color.rgb(170, 170, 170));
        RelativeLayout rel3 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt3 = (TextView)rel3.findViewById(com.pacifico.adn.R.id.txt1);
        txt3.setText("HIJOS");
        Fuente.setFuenteRg(getActivity(), txt3);
        ImageView imgView3 = (ImageView)rel3.findViewById(com.pacifico.adn.R.id.img1);
        imgView3.setColorFilter(Color.rgb(170, 170, 170));
        imgView3.setImageResource(com.pacifico.adn.R.drawable.ic_child_friendly_black_24dp);
        tblTabs.getTabAt(0).setCustomView(rel1);
        tblTabs.getTabAt(1).setCustomView(rel2);
        tblTabs.getTabAt(2).setCustomView(rel3);
        pagProspecto.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tblTabs));
        tblTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                RelativeLayout relSelected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relSelected.findViewById(com.pacifico.adn.R.id.txt1);
                Fuente.setFuenteRg(getActivity(), txt1);
                txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron));
                ImageView imgView1 = (ImageView) relSelected.findViewById(com.pacifico.adn.R.id.img1);
                Fuente.setFuenteRg(getActivity(), txt1);
                txt1.setTextSize(15);
                imgView1.setColorFilter(Color.rgb(0, 0, 0));
                pagProspecto.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if(tab.getPosition() == 0){
                    if(datosPersonalesFragment.getEdtActual() != null) {
                        ocultarTecladoConEditText(datosPersonalesFragment.getEdtActual());
                    }
                }else if(tab.getPosition() == 1) {
                    if (conyugeFragment.getEdtActual() != null) {
                        ocultarTecladoConEditText(conyugeFragment.getEdtActual());
                    }
                }else{
                    if(hijosFragment.getEdtActual() != null){
                        ocultarTecladoConEditText(hijosFragment.getEdtActual());
                    }
                }
                RelativeLayout relUnselected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relUnselected.findViewById(com.pacifico.adn.R.id.txt1);
                txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron70));
                Fuente.setFuenteRg(getActivity(), txt1);
                ImageView imgView1 = (ImageView) relUnselected.findViewById(com.pacifico.adn.R.id.img1);
                imgView1.setColorFilter(Color.rgb(170, 170, 170));
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
        });
        imgBtnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarCampos();
            }
        });
        RelativeLayout relLayProspecto = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayProspecto);
        relLayProspecto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(datosPersonalesFragment.getEdtActual() != null){
                    ocultarTecladoConEditText(datosPersonalesFragment.getEdtActual());
                }
                if(conyugeFragment.getEdtActual() != null){
                    ocultarTecladoConEditText(conyugeFragment.getEdtActual());
                }
                if(hijosFragment.getEdtActual() != null){
                    ocultarTecladoConEditText(hijosFragment.getEdtActual());
                }
            }
        });
        return view;
    }
    public void validarCampos(){
        datosPersonalesFragment.validarTodosCampos();
        conyugeFragment.validarCampos();
        hijosFragment.validarCampos();

        ArrayList<String> arrCamposIncorrectos = new ArrayList<>();
        if(adnActivity.getProspectoBean().getNombres().length() == 0){
            arrCamposIncorrectos.add("Primer Nombre");
        }
        if(adnActivity.getProspectoBean().getApellidoPaterno().length() == 0){
            arrCamposIncorrectos.add("Apellido Paterno");
        }
        if(datosPersonalesFragment.getEdtNumeroDocumento().getError() != null){
            arrCamposIncorrectos.add("Número de Documento");
        }
        if(adnActivity.getProspectoBean().getFechaNacimiento() == null){
            arrCamposIncorrectos.add("Fecha de Nacimiento");
        }else{
            if(adnActivity.getProspectoBean().getFechaNacimiento().length() == 0){
                arrCamposIncorrectos.add("Fecha de Nacimiento");
            }
        }
        if(datosPersonalesFragment.getEdtTelefonoCelular().getError() != null){
            arrCamposIncorrectos.add("Teléfono celular");
        }
        if(datosPersonalesFragment.getEdtTelefonoFijo().getError() != null){
            arrCamposIncorrectos.add("Teléfono fijo");
        }
        if(datosPersonalesFragment.getEdtCorreoElectronico().getError() != null){
            arrCamposIncorrectos.add("Correo electrónico");
        }
        if (adnActivity.getProspectoBean().getCodigoEstadoCivil() == -1){
            arrCamposIncorrectos.add("Estado Civil");
        }
        if(adnActivity.getProspectoBean().getCodigoSexo() == -1){
            arrCamposIncorrectos.add("Sexo");
        }
        if(adnActivity.getProspectoBean().getCondicionFumador() == -1){
            arrCamposIncorrectos.add("Condición de Fumador");
        }
        if(adnActivity.getProspectoBean().getFlagHijo() == -1){
            arrCamposIncorrectos.add("Hijos");
        }
        if(arrCamposIncorrectos.size() == 0){
            boolean preguntarConyuge = false;
            boolean preguntarHijos = false;
            FamiliarBean conyugeBean = adnActivity.getConyugeBean();
            if(adnActivity.getProspectoBean().getFlagConyuge() == 1){
                if(conyugeBean.getNombres().length() == 0){
                    arrCamposIncorrectos.add("Nombres del Cónyuge");
                }
                if(conyugeBean.getApellidoPaterno().length() == 0){
                    arrCamposIncorrectos.add("Apellido Paterno del Cónyuge");
                }
                if(adnActivity.getConyugeBean().getFechaNacimiento().length() == 0){
                    arrCamposIncorrectos.add("Fecha de nacimiento del Cónyuge");
                }
                if(adnActivity.getConyugeBean().getOcupacion().length() == 0){
                   /* arrCamposIncorrectos.add("Ocupación del Cónyuge");*/
                }
            }else{
                if(conyugeBean.getNombres().length() > 0 || conyugeBean.getApellidoPaterno().length() > 0 || conyugeBean.getApellidoMaterno().length() > 0 || conyugeBean.getFechaNacimiento().length() > 0 || conyugeBean.getOcupacion().length() > 0){
                    preguntarConyuge = true;
                }
            }
            if(arrCamposIncorrectos.size() == 0){
                ArrayList<FamiliarBean> arrHijosFinal = hijosFragment.getArrHijos();
                if(adnActivity.getProspectoBean().getFlagHijo() == 1){
                    if(arrHijosFinal.size() > 0){
                        for(FamiliarBean hijo : arrHijosFinal){
                            if(hijo.getNombres().length() == 0){
                                arrCamposIncorrectos.add("Nombre del hijo");
                            }
                            int edad = hijo.getEdad();
                            if(edad == -1 || edad > 100){
                                arrCamposIncorrectos.add("Edad del hijo");
                            }
                            if(arrCamposIncorrectos.size() > 0){
                                break;
                            }
                        }
                    }else{
                        String mensajeError = "Debe registrar al menos un hijo";
                        Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                }else{
                    if(arrHijosFinal.size() > 0){
                        for(FamiliarBean hijo : arrHijosFinal){
                            if(hijo.getNombres().length() > 0 && hijo.getEdad() > -1){
                                preguntarHijos = true;
                                break;
                            }
                        }
                    }
                }
                if(arrCamposIncorrectos.size() == 0){
                    final boolean preguntarHijosFinal = preguntarHijos;
                    if(preguntarConyuge){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                                .setTitle("Alerta")
                                .setMessage("“Hay información registrada en los datos del cónyuge y no coincide con su estado civil, ¿Desea continuar?")
                                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(preguntarHijosFinal){
                                            pregunarPorHijos();
                                        }else{
                                            guardar();
                                            adnActivity.mostrarFlujoADN(0);
                                        }
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        pagProspecto.setCurrentItem(0);
                                    }
                                });
                        builder.show();
                    }else{
                        if(preguntarHijos){
                            pregunarPorHijos();
                        }else{
                            guardar();
                            adnActivity.mostrarFlujoADN(0);
                        }
                    }
                }else{
                    pagProspecto.setCurrentItem(2);
                    String[] camposIncorrectos = new String[arrCamposIncorrectos.size()];
                    arrCamposIncorrectos.toArray(camposIncorrectos);
                    String mensajeError = "Por favor corregir los datos en: " + TextUtils.join(", ", camposIncorrectos);
                    Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
                }
            }else{
                pagProspecto.setCurrentItem(1);
                String[] camposIncorrectos = new String[arrCamposIncorrectos.size()];
                arrCamposIncorrectos.toArray(camposIncorrectos);
                String mensajeError = "Por favor corregir los datos en: " + TextUtils.join(", ", camposIncorrectos);
                Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
            }
        }else{
            pagProspecto.setCurrentItem(0);
            String[] camposIncorrectos = new String[arrCamposIncorrectos.size()];
            arrCamposIncorrectos.toArray(camposIncorrectos);
            String mensajeError = "Por favor corregir los datos en: " + TextUtils.join(", ", camposIncorrectos);
            Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
        }
    }
    private void pregunarPorHijos(){
        adnActivity.getProspectoBean().setFlagHijo(1);
        adnActivity.getProspectoBean().setFlagModificado(1);
        guardar();
        adnActivity.mostrarFlujoADN(0);
    }
    public void guardar(){
        if(datosPersonalesFragment.getEdtActual() != null){
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(datosPersonalesFragment.getEdtActual().getWindowToken(), 0);
            datosPersonalesFragment.getEdtActual().clearFocus();
        }
        if(conyugeFragment.getEdtActual() != null){
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(conyugeFragment.getEdtActual().getWindowToken(), 0);
            conyugeFragment.getEdtActual().clearFocus();
        }
        if(hijosFragment.getEdtActual() != null){
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(hijosFragment.getEdtActual().getWindowToken(), 0);
            hijosFragment.getEdtActual().clearFocus();
        }
        if(adnActivity.getProspectoBean().getFlagModificado() == 1){
            adnActivity.getProspectoBean().setFlagModificado(0);
            adnActivity.getProspectoBean().setFechaModificacionDispositivo(Util.obtenerFechaActual());
            adnActivity.getProspectoBean().setFlagEnviado(0);
            ProspectoController.actualizarProspecto(adnActivity.getProspectoBean());
        }
        TablaIdentificadorBean familiarIdentificador = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_FAMILIAR);
        int idFamiliarDispositivoDisponible = familiarIdentificador.getIdentity() + 1;
        FamiliarBean conyugeBean = adnActivity.getConyugeBean();
        if(adnActivity.getProspectoBean().getFlagConyuge() == 1){
            if(conyugeBean != null){
                if(conyugeBean.getFlagModificado() == 1){
                    conyugeBean.setFlagModificado(0);
                    int idFamiliarDispositivo = conyugeBean.getIdFamiliarDispositivo();
                    if(idFamiliarDispositivo == -1){
                        conyugeBean.setIdFamiliar(-1);
                        conyugeBean.setIdFamiliarDispositivo(idFamiliarDispositivoDisponible);
                        idFamiliarDispositivoDisponible++;
                        conyugeBean.setIdProspecto(adnActivity.getProspectoBean().getIdProspecto());
                        conyugeBean.setIdProspectoDispositivo(adnActivity.getProspectoBean().getIdProspectoDispositivo());
                        conyugeBean.setCodigoTipoFamiliar(1);
                        conyugeBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                        FamiliarController.guardarFamiliar(conyugeBean);
                    }else{
                        conyugeBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                        conyugeBean.setFlagEnviado(0);
                        FamiliarController.actualizarFamiliar(conyugeBean);
                    }
                }
            }
        }else{
            if(conyugeBean != null){
                conyugeBean.setFlagActivo(0);
                conyugeBean.setFlagEnviado(0);
                FamiliarController.actualizarFamiliar(conyugeBean);
            }
        }
        ArrayList<FamiliarBean> arrHijosInicial = (ArrayList<FamiliarBean>)adnActivity.getArrHijosBean().clone();
        if(adnActivity.getProspectoBean().getFlagHijo() == 1){
            ArrayList<FamiliarBean> arrHijosFinal = hijosFragment.getArrHijos();
            for(FamiliarBean hijoBean : arrHijosFinal){
                if(hijoBean.getIdFamiliarDispositivo() == -1){
                    if(hijoBean.getNombres().length() > 0 && hijoBean.getEdad() > -1){
                        hijoBean.setFlagModificado(0);
                        hijoBean.setIdFamiliar(-1);
                        hijoBean.setIdFamiliarDispositivo(idFamiliarDispositivoDisponible);
                        idFamiliarDispositivoDisponible++;
                        hijoBean.setIdProspecto(adnActivity.getProspectoBean().getIdProspecto());
                        hijoBean.setIdProspectoDispositivo(adnActivity.getProspectoBean().getIdProspectoDispositivo());
                        hijoBean.setCodigoTipoFamiliar(2);
                        hijoBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                        FamiliarController.guardarFamiliar(hijoBean);
                        adnActivity.setCambiarNumeroHijosADN(true);
                    }
                }else{
                    for(FamiliarBean hijoBeanTemp : arrHijosInicial){
                        if(hijoBeanTemp.getIdFamiliarDispositivo() == hijoBean.getIdFamiliarDispositivo()){
                            if(hijoBean.getNombres().length() > 0 && hijoBean.getEdad() > -1){
                                arrHijosInicial.remove(hijoBeanTemp);
                                if(hijoBean.getFlagModificado() == 1){
                                    hijoBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                                    hijoBean.setFlagEnviado(0);
                                    FamiliarController.actualizarFamiliar(hijoBean);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        familiarIdentificador.setIdentity(idFamiliarDispositivoDisponible);
        TablaIdentificadorController.actualizarTablaIdentificador(familiarIdentificador);
        for(FamiliarBean hijoBean : arrHijosInicial){
            hijoBean.setFlagActivo(0);
            hijoBean.setFlagEnviado(0);
            FamiliarController.actualizarFamiliar(hijoBean);
            adnActivity.setCambiarNumeroHijosADN(true);
        }
        adnActivity.cargarHijos();
        hijosFragment.setArrHijos(adnActivity.getArrHijosBean());
    }
    private void ocultarTecladoConEditText(EditText editText){
        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        editText.clearFocus();
    }
    public void revisarNumeroHijos(){
        if(hijosFragment.getArrHijos().size() == 0){
            FamiliarBean hijoBean = new FamiliarBean();
            hijoBean.inicializarValores();
            hijosFragment.getArrHijos().add(hijoBean);
            hijosFragment.recargarListaHijos();
        }
    }
}