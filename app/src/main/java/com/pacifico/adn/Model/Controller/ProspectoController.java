package com.pacifico.adn.Model.Controller;

import android.database.Cursor;

import com.pacifico.adn.Activity.ADNApplication;
import com.pacifico.adn.Model.Bean.CitaBean;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Persistence.DatabaseConstants;
import com.dsbmobile.dsbframework.controller.persistence.Entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by vctrls3477 on 6/06/16.
 */
public class ProspectoController{
    public static void actualizarProspecto(ProspectoBean prospectoBean){
        String[] parameters = new String[]{Integer.toString(prospectoBean.getIdProspectoDispositivo())};
        ProspectoBean.tableHelper.updateEntity(prospectoBean, "IDPROSPECTODISPOSITIVO = ?", parameters);
    }
    public static void actualizarProspectoSinProspectoDispositivo(ProspectoBean prospectoBean){
        String[] parameters = new String[]{Integer.toString(prospectoBean.getIdProspecto())};
        ProspectoBean.tableHelper.updateEntity(prospectoBean, "IDPROSPECTO = ?", parameters);
    }
    public static void guardarProspecto(ProspectoBean prospectoBean){
        ProspectoBean.tableHelper.insertEntity(prospectoBean);
    }
    public static void guardarListaProspectos(ArrayList<ProspectoBean> listaProspectoBean) {

        ArrayList<ProspectoBean> prospectosConReferente = new ArrayList<ProspectoBean>();

        if(listaProspectoBean != null){
            for(ProspectoBean prospectoBean : listaProspectoBean){
                if (prospectoBean.getIdReferenciador() != -1)
                    prospectosConReferente.add(prospectoBean);

                guardarProspecto(prospectoBean);
            }
        }

        // ACTUALIZAR LOS ID REFERIDOS (se inserta aparte por que es la misma entidad)
        for(ProspectoBean prospectoBean : prospectosConReferente){
            ProspectoBean prospectoReferente = obtenerProspectoPorIdProspecto(prospectoBean.getIdReferenciador());
            prospectoBean.setIdReferenciadorDispositivo(prospectoReferente.getIdProspectoDispositivo());

            actualizarProspecto(prospectoBean);
        }
    }

    public static ArrayList<ProspectoBean> obtenerProspectosSinEviar(){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ProspectoBean> arrProspectosFinal = new ArrayList<>();
        for(Entity entity : arrProspectos){
            arrProspectosFinal.add((ProspectoBean)entity);
        }
        return arrProspectosFinal;
    }
    public static ProspectoBean obtenerProspectoPorIdProspecto(int idProspecto){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTO = ?", new String[]{Integer.toString(idProspecto)});
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }
    public static ArrayList<ProspectoBean> obtenerProspectosConCita(){
        ArrayList<ProspectoBean> lista = new ArrayList<>();
        String sql = "SELECT * FROM " + DatabaseConstants.TBL_PROSPECTO + " WHERE " + ProspectoBean.CN_IDPROSPECTODISPOSITIVO +
                " IN (SELECT DISTINCT " + CitaBean.CN_IDPROSPECTODISPOSITIVO + " FROM " + DatabaseConstants.TBL_CITA + ")";
        Cursor cursor = ADNApplication.getDB().rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                ProspectoBean bean = new ProspectoBean();
                bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setIdProspectoExterno(cursor.getInt(2));
                bean.setNombres(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setApellidoMaterno(cursor.getString(5));
                bean.setCodigoSexo(cursor.getInt(6));
                bean.setCodigoEstadoCivil(cursor.getInt(7));
                bean.setFechaNacimiento(cursor.getString(8));
                bean.setCodigoRangoEdad(cursor.getInt(9));
                bean.setCodigoRangoIngreso(cursor.getInt(10));
                bean.setFlagHijo(cursor.getInt(11));
                bean.setFlagConyuge(cursor.getInt(12));
                bean.setCodigoTipoDocumento(cursor.getInt(13));
                bean.setNumeroDocumento(cursor.getString(14));
                bean.setTelefonoCelular(cursor.getString(15));
                bean.setTelefonoFijo(cursor.getString(16));
                bean.setTelefonoAdicional(cursor.getString(17));
                bean.setCorreoElectronico1(cursor.getString(18));
                bean.setCorreoElectronico2(cursor.getString(19));
                bean.setCodigoNacionalidad(cursor.getInt(20));
                bean.setCondicionFumador(cursor.getInt(21));
                bean.setEmpresa(cursor.getString(22));
                bean.setCargo(cursor.getString(23));
                bean.setNota(cursor.getString(24));
                bean.setFlagEnviarADNDigital(cursor.getInt(25));
                bean.setCodigoEtapa(cursor.getInt(26));
                bean.setCodigoEstado(cursor.getInt(27));
                bean.setCodigoFuente(cursor.getInt(28));
                bean.setCodigoEntorno(cursor.getInt(29));
                bean.setIdReferenciador(cursor.getInt(30));
                bean.setFechaCreacionDispositivo(cursor.getString(31));
                bean.setFechaModificacionDispositivo(cursor.getString(32));
                bean.setAdicionalTexto1(cursor.getString(33));
                bean.setAdicionalNumerico1(cursor.getInt(34));
                bean.setAdicionalTexto2(cursor.getString(35));
                bean.setAdicionalNumerico2(cursor.getInt(36));
                bean.setIdReferenciadorDispositivo(cursor.getInt(37));
                bean.setFlagEnviado(cursor.getInt(38));
                lista.add(bean);
            } while(cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }
    public static int obtenerMaximoIdProspectoDispositivo(){
        int idProspectoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDPROSPECTODISPOSITIVO) FROM " + DatabaseConstants.TBL_PROSPECTO;
        Cursor cursor = ADNApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idProspectoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idProspectoDispositivoMax;
    }
    public static ProspectoBean obtenerProspectoPorIdProspectoDispositivo(int idProspectoDispositivo){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = ?", new String[]{Integer.toString(idProspectoDispositivo)});
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }
    public static ArrayList<ProspectoBean> obtenerProspectoSinProspectoDispositivo(){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = -1", null);
        ArrayList<ProspectoBean> arrProspectosFinal = new ArrayList<>();
        for(Entity entity : arrProspectos){
            arrProspectosFinal.add((ProspectoBean)entity);
        }
        return arrProspectosFinal;
    }
    public static ProspectoBean obtenerProspectoConCitaActualProxima(){
        Calendar ahora = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fechaActual = dateFormat.format(ahora.getTime());
        dateFormat = new SimpleDateFormat("HH:mm");
        String hora = dateFormat.format(ahora.getTime());
        ArrayList<CitaBean> arrCitaActuales = CitaReunionController.obtenerCitasDentroHora(hora, fechaActual);
        int idProspectoDispositivo = -1;
        if(arrCitaActuales.size() == 0){
            ahora.add(Calendar.MINUTE, 10);
            String horaProxima = dateFormat.format(ahora.getTime());
            ArrayList<CitaBean> arrCitaProxima = CitaReunionController.obtenerCitasDespuesHora(hora, horaProxima, fechaActual);
            if(arrCitaProxima.size() > 0){
                int indiceCitaMenorDuracion = 0;
                int minimaDuracion = 2147483647;
                for(int i = 0; i < arrCitaProxima.size() - 1; i++){
                    CitaBean citaActual = arrCitaProxima.get(i);
                    CitaBean citaSiguiente = arrCitaProxima.get(i + 1);
                    if(citaActual.getHoraInicio().equals(citaSiguiente.getHoraInicio())){
                        if(citaActual.obtenerDuracion() <= citaSiguiente.obtenerDuracion()){
                            if(citaActual.obtenerDuracion() < minimaDuracion){
                                indiceCitaMenorDuracion = i;
                                minimaDuracion = citaActual.obtenerDuracion();
                            }
                        }else{
                            if(citaSiguiente.obtenerDuracion() < minimaDuracion){
                                indiceCitaMenorDuracion = i + 1;
                                minimaDuracion = citaSiguiente.obtenerDuracion();
                            }
                        }
                    }else{
                        break;
                    }
                }
                idProspectoDispositivo = arrCitaProxima.get(indiceCitaMenorDuracion).getIdProspectoDispositivo();
            }
        }else{
            int indiceCitaMenorDuracion = 0;
            int minimaDuracion = 2147483647;
            for(int i = 0; i < arrCitaActuales.size() - 1; i++){
                CitaBean citaActual = arrCitaActuales.get(i);
                CitaBean citaSiguiente = arrCitaActuales.get(i + 1);
                if(citaActual.getHoraInicio().equals(citaSiguiente.getHoraInicio())){
                    if(citaActual.obtenerDuracion() <= citaSiguiente.obtenerDuracion()){
                        if(citaActual.obtenerDuracion() < minimaDuracion){
                            indiceCitaMenorDuracion = i;
                            minimaDuracion = citaActual.obtenerDuracion();
                        }
                    }else{
                        if(citaSiguiente.obtenerDuracion() < minimaDuracion){
                            indiceCitaMenorDuracion = i + 1;
                            minimaDuracion = citaSiguiente.obtenerDuracion();
                        }
                    }
                }else{
                    break;
                }
            }
            idProspectoDispositivo = arrCitaActuales.get(indiceCitaMenorDuracion).getIdProspectoDispositivo();
        }
        if(idProspectoDispositivo == -1){
            return null;
        }else{
            return ProspectoController.obtenerProspectoPorIdProspectoDispositivo(idProspectoDispositivo);
        }
    }
}