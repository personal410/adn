package com.pacifico.adn.Fragment.Gastos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.R;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;
import com.pacifico.adn.Views.Interface.OnStepperDoneSelectedListener;

public class GastoViviendaFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, OnStepperChangeValueListener, OnStepperDoneSelectedListener, StepperView.OnFocusChangeListener {
    private ADNActivity actADN;
    private RadioGroup rgMonedaGastoVivienda, rgMonedaGastoServicios;
    private StepperView stepGastoVivienda;
    private StepperView stepGastoServicios;
    private StepperView stepGastoHogarAlimentacion;
    private StepperView stepActual;
    private GastosMensualFragment gastosMensualFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_gasto_vivienda, container, false);
        rgMonedaGastoVivienda = (RadioGroup)view.findViewById(R.id.rgMonedaGastoVivienda);
        rgMonedaGastoServicios = (RadioGroup)view.findViewById(R.id.rgMonedaGastoServicios);
        RadioGroup rgMonedaGastoHogarAlimentacion = (RadioGroup)view.findViewById(R.id.rgMonedaGastoHogarAlimentacion);
        stepGastoVivienda = (StepperView)view.findViewById(R.id.stepGastoVivienda);
        stepGastoServicios = (StepperView)view.findViewById(R.id.stepGastoServicios);
        stepGastoHogarAlimentacion = (StepperView)view.findViewById(R.id.stepGastoHogarAlimentacion);
        rgMonedaGastoVivienda.check(actADN.getAdnBean().getMonedaGastoVivienda() == Constantes.MonedaDolares ? R.id.rb11 : R.id.rb12);
        rgMonedaGastoServicios.check(actADN.getAdnBean().getMonedaGastoServicios() == Constantes.MonedaDolares ? R.id.rb21 : R.id.rb22);
        rgMonedaGastoHogarAlimentacion.check(actADN.getAdnBean().getMonedaGastoHogarAlimentacion() == Constantes.MonedaDolares ? R.id.rb31 : R.id.rb32);
        stepGastoVivienda.setValorActual(actADN.getAdnBean().getGastoVivienda());
        stepGastoServicios.setValorActual(actADN.getAdnBean().getGastoServicios());
        stepGastoHogarAlimentacion.setValorActual(actADN.getAdnBean().getGastoHogarAlimentacion());
        rgMonedaGastoVivienda.setOnCheckedChangeListener(this);
        rgMonedaGastoServicios.setOnCheckedChangeListener(this);
        rgMonedaGastoHogarAlimentacion.setOnCheckedChangeListener(this);
        stepGastoVivienda.setOnStepperChangeValueListener(this);
        stepGastoServicios.setOnStepperChangeValueListener(this);
        stepGastoHogarAlimentacion.setOnStepperChangeValueListener(this);
        stepGastoVivienda.setOnStepperDoneSelectedListener(this);
        stepGastoServicios.setOnStepperDoneSelectedListener(this);
        stepGastoVivienda.setOnFocusChangeListener(this);
        stepGastoServicios.setOnFocusChangeListener(this);
        stepGastoHogarAlimentacion.setOnFocusChangeListener(this);
        RelativeLayout relLayGastoVivienda = (RelativeLayout)view.findViewById(R.id.relLayGastoVivienda);
        relLayGastoVivienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stepActual == null){
                    gastosMensualFragment.getFlujoADNFragment().clearFocusStepTotalGastoFamiliares();
                }else{
                    stepActual.clearFocus();
                }
            }
        });
        return view;
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group == rgMonedaGastoVivienda){
            actADN.getAdnBean().setMonedaGastoVivienda((checkedId == R.id.rb11) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else if(group == rgMonedaGastoServicios){
            actADN.getAdnBean().setMonedaGastoServicios((checkedId == R.id.rb21) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else{
            actADN.getAdnBean().setMonedaGastoHogarAlimentacion((checkedId == R.id.rb31) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }
        calcularTotal();
    }
    @Override
    public void onChangeValue(StepperView stepView){
        int valorActual = stepView.getValorActual();
        if(stepView == stepGastoVivienda){
            if(valorActual != actADN.getAdnBean().getGastoVivienda()){
                actADN.getAdnBean().setGastoVivienda(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else if(stepView == stepGastoServicios){
            if(valorActual != actADN.getAdnBean().getGastoServicios()){
                actADN.getAdnBean().setGastoServicios(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else{
            if(valorActual != actADN.getAdnBean().getGastoHogarAlimentacion()){
                actADN.getAdnBean().setGastoHogarAlimentacion(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }
        calcularTotal();
    }
    public void calcularTotal(){
        double factorGastoVivienda = actADN.getAdnBean().getMonedaGastoVivienda() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorGastoServicios = actADN.getAdnBean().getMonedaGastoServicios() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorGastoHogarAlimentacion = actADN.getAdnBean().getMonedaGastoHogarAlimentacion() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double montoTotal = stepGastoVivienda.getValorActual() / factorGastoVivienda + stepGastoServicios.getValorActual() / factorGastoServicios + stepGastoHogarAlimentacion.getValorActual() / factorGastoHogarAlimentacion;
        //montoTotal = Util.redondearNumero(montoTotal, 2);
        actADN.getAdnBean().setTotalGastoVivienda(montoTotal);
        double totalGastos = montoTotal + actADN.getAdnBean().getTotalGastoSaludEducacion() + actADN.getAdnBean().getTotalGastoTransporte() + actADN.getAdnBean().getTotalGastoOtros();
        actADN.getAdnBean().setTotalGastoFamiliarMensual(Util.redondearNumero(totalGastos, 2));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    @Override
    public void onStepperDoneSelected(StepperView stepperView){
        if(stepperView.equals(stepGastoVivienda)){
            stepGastoServicios.requestStepperFocus();
        }else{
            stepGastoHogarAlimentacion.requestStepperFocus();
        }
    }
    public void setGastosMensualFragment(GastosMensualFragment gastosMensualFragment) {
        this.gastosMensualFragment = gastosMensualFragment;
    }
    @Override
    public void onFocusChange(boolean hasFocus, StepperView stepperView){
        if(hasFocus){
            stepActual = stepperView;
        }else{
            stepActual = null;
        }
    }
    public StepperView getStepActual() {
        return stepActual;
    }
}