package com.pacifico.adn.Fragment.Gastos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;
import com.pacifico.adn.Views.Interface.OnStepperDoneSelectedListener;

public class GastoOtrosFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, OnStepperChangeValueListener, StepperView.OnFocusChangeListener {
    private ADNActivity actADN;
    private RadioGroup rgMonedaGastoEsparcimiento;
    private StepperView stepGastoEsparcimiento, stepGastoOtros, stepActual;
    private GastosMensualFragment gastosMensualFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_gasto_otros, container, false);
        rgMonedaGastoEsparcimiento = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaGastoEsparcimiento);
        RadioGroup rgMonedaGastoOtros = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaGastoOtros);
        stepGastoEsparcimiento = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepGastoEsparcimiento);
        stepGastoOtros = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepGastoOtros);
        rgMonedaGastoEsparcimiento.check(actADN.getAdnBean().getMonedaGastoEsparcimiento() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb11 : com.pacifico.adn.R.id.rb12);
        rgMonedaGastoOtros.check(actADN.getAdnBean().getMonedaGastoOtros() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb21 : com.pacifico.adn.R.id.rb22);
        stepGastoEsparcimiento.setValorActual(actADN.getAdnBean().getGastoEsparcimiento());
        stepGastoOtros.setValorActual(actADN.getAdnBean().getGastoOtros());
        rgMonedaGastoEsparcimiento.setOnCheckedChangeListener(this);
        rgMonedaGastoOtros.setOnCheckedChangeListener(this);
        stepGastoEsparcimiento.setOnStepperChangeValueListener(this);
        stepGastoOtros.setOnStepperChangeValueListener(this);
        stepGastoEsparcimiento.setOnFocusChangeListener(this);
        stepGastoOtros.setOnFocusChangeListener(this);
        stepGastoEsparcimiento.setOnStepperDoneSelectedListener(new OnStepperDoneSelectedListener() {
            @Override
            public void onStepperDoneSelected(StepperView stepperView) {
                stepGastoOtros.requestStepperFocus();
            }
        });
        RelativeLayout relLayGastoOtros = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayGastoOtros);
        relLayGastoOtros.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(stepActual == null){
                    gastosMensualFragment.getFlujoADNFragment().clearFocusStepTotalGastoFamiliares();
                }else{
                    stepActual.clearFocus();
                }
            }
        });
        return view;
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group == rgMonedaGastoEsparcimiento){
            actADN.getAdnBean().setMonedaGastoEsparcimiento((checkedId == com.pacifico.adn.R.id.rb11) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else{
            actADN.getAdnBean().setMonedaGastoOtros((checkedId == com.pacifico.adn.R.id.rb21) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }
        calcularTotal();
    }
    @Override
    public void onChangeValue(StepperView stepView){
        int valorActual = stepView.getValorActual();
        if(stepView == stepGastoEsparcimiento){
            if(valorActual != actADN.getAdnBean().getGastoEsparcimiento()){
                actADN.getAdnBean().setGastoEsparcimiento(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else{
            if(valorActual != actADN.getAdnBean().getGastoOtros()){
                actADN.getAdnBean().setGastoOtros(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }
        calcularTotal();
    }
    public void calcularTotal(){
        double factorGastoEsparcimiento = actADN.getAdnBean().getMonedaGastoEsparcimiento() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorGastoOtros = actADN.getAdnBean().getMonedaGastoOtros() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double montoTotal = stepGastoEsparcimiento.getValorActual() / factorGastoEsparcimiento + stepGastoOtros.getValorActual() / factorGastoOtros;
        //montoTotal = Util.redondearNumero(montoTotal, 2);
        actADN.getAdnBean().setTotalGastoOtros(montoTotal);
        double totalGastos = actADN.getAdnBean().getTotalGastoVivienda() + actADN.getAdnBean().getTotalGastoSaludEducacion() + actADN.getAdnBean().getTotalGastoTransporte() + montoTotal;
        actADN.getAdnBean().setTotalGastoFamiliarMensual(Util.redondearNumero(totalGastos, 2));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    public void setGastosMensualFragment(GastosMensualFragment gastosMensualFragment){
        this.gastosMensualFragment = gastosMensualFragment;
    }
    @Override
    public void onFocusChange(boolean hasFocus, StepperView stepperView){
        if(hasFocus){
            stepActual = stepperView;
        }else{
            stepActual = null;
        }
    }
    public StepperView getStepActual(){
        return stepActual;
    }
}