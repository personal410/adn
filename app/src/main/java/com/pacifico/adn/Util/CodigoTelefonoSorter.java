package com.pacifico.adn.Util;

import com.pacifico.adn.Model.Bean.TablaTablasBean;

import java.util.Comparator;

public class CodigoTelefonoSorter implements Comparator<TablaTablasBean>{
    @Override
    public int compare(TablaTablasBean first, TablaTablasBean second){
        return first.getValorCadena().compareToIgnoreCase(second.getValorCadena());
    }
}