package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.ResumenArrayAdapter;
import com.pacifico.adn.Fragment.Gastos.GastosMensualFragment;
import com.pacifico.adn.Model.Bean.AdnBean;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;

public class FlujoADNFragment extends Fragment implements View.OnClickListener, OnStepperChangeValueListener {
    private boolean stepTotalGastoFamiliaresHasFocus;
    private int posicionActual;
    private ImageButton imgBtnAtras;
    private TextView txtTituloTotal, txtMontoTotalDolares, txtMontoTotalSoles, txtSimboloDolaresTotalGastosFamiliares;
    private StepperView stepTotalGastoFamiliares;
    private ADNActivity adnActivity;
    private ResumenFragment resumenFragment;
    private AdnBean adnBean;
    private Fragment actualFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_flujo_adn, container, false);
        txtTituloTotal = (TextView)view.findViewById(com.pacifico.adn.R.id.txtTituloTotal);
        txtMontoTotalDolares = (TextView)view.findViewById(com.pacifico.adn.R.id.txtMontoTotalDolares);
        stepTotalGastoFamiliares = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepTotalGastoFamiliares);
        txtSimboloDolaresTotalGastosFamiliares = (TextView)view.findViewById(com.pacifico.adn.R.id.txtSimboloDolaresTotalGastosFamiliares);
        stepTotalGastoFamiliares.setOnStepperChangeValueListener(this);
        txtMontoTotalSoles = (TextView)view.findViewById(com.pacifico.adn.R.id.txtMontoTotalSoles);
        imgBtnAtras = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnAtras);
        ImageButton imgBtnSiguiente = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnSiguiente);
        imgBtnAtras.setOnClickListener(this);
        imgBtnSiguiente.setOnClickListener(this);
        adnActivity = (ADNActivity)getActivity();
        adnBean = adnActivity.getAdnBean();
        if(posicionActual == 0){
            actualFragment = new ActivosFragment();
        }else{
            actualFragment = new IngresoMensualFragment();
        }
        getActivity().getSupportFragmentManager().beginTransaction().add(com.pacifico.adn.R.id.fraLayContenedor, actualFragment).commit();
        resumenFragment = new ResumenFragment();
        getActivity().getSupportFragmentManager().beginTransaction().add(com.pacifico.adn.R.id.fraLayResumen, resumenFragment).commit();
        actualizarTituloMontoTotal();
        actualizarMontoTotal();
        stepTotalGastoFamiliares.setOnFocusChangeListener(new StepperView.OnFocusChangeListener(){
            @Override
            public void onFocusChange(boolean hasFocus, StepperView stepperView){
                stepTotalGastoFamiliaresHasFocus = hasFocus;
            }
        });
        return view;
    }
    public void actualizarMontoTotal(){
        double montoTotalTemp = 0;
        if(posicionActual == 4){
            txtMontoTotalDolares.setVisibility(View.INVISIBLE);
            stepTotalGastoFamiliares.setVisibility(View.VISIBLE);
            txtSimboloDolaresTotalGastosFamiliares.setVisibility(View.VISIBLE);
            montoTotalTemp = adnBean.getTotalGastoFamiliarMensual();
            stepTotalGastoFamiliares.setValorActual((int)montoTotalTemp);
        }else{
            txtMontoTotalDolares.setVisibility(View.VISIBLE);
            stepTotalGastoFamiliares.setVisibility(View.INVISIBLE);
            txtSimboloDolaresTotalGastosFamiliares.setVisibility(View.INVISIBLE);
            switch(posicionActual){
                case 0:
                    montoTotalTemp = adnBean.getTotalActivoRealizable();
                    break;
                case 1:
                    montoTotalTemp = adnBean.getTotalSegurosVida();
                    break;
                case 2:
                    montoTotalTemp = adnBean.getTotalPensionMensualAFP();
                    break;
                case 3:
                    montoTotalTemp = adnBean.getTotalIngresoFamiliarMensual();
                    break;
                case 5:
                    double ingresoNetoMensualFamiliar = adnBean.getTotalIngresoFamiliarMensual();
                    double factorIngresoConyuge = (adnBean.getMonedaIngresoMensualConyuge() == Constantes.MonedaDolares) ? 1 : adnBean.getTipoCambio();
                    double ingresoConyuge = adnBean.getIngresoMensualConyuge() / factorIngresoConyuge;
                    double ingresoProvenientesAFP = adnBean.getTotalPensionMensualAFP();
                    montoTotalTemp = Util.redondearNumero(ingresoNetoMensualFamiliar, 0) - Util.redondearNumero(ingresoConyuge, 0) - Util.redondearNumero(ingresoProvenientesAFP, 0);
                    if(montoTotalTemp < 0){
                        montoTotalTemp = 0;
                    }
                    if(adnBean.getDeficitMensual() != montoTotalTemp) {
                        adnBean.setDeficitMensual(montoTotalTemp);
                        adnBean.setFlagModificado(1);
                    }
                    this.txtMontoTotalDolares.setText(String.format("%s %s", this.getString(com.pacifico.adn.R.string.simbolo_dolares), Util.obtenerNumeroConFormato(montoTotalTemp)));
                    double montoTotalSoles = (ingresoNetoMensualFamiliar - ingresoConyuge - ingresoProvenientesAFP) * adnBean.getTipoCambio();
                    if(montoTotalSoles < 0){
                        montoTotalSoles = 0;
                    }
                    this.txtMontoTotalSoles.setText(String.format("En Soles %s %s", this.getString(com.pacifico.adn.R.string.simbolo_soles), Util.obtenerNumeroConFormato(montoTotalSoles)));
                    return;
                case 6:
                    montoTotalTemp = adnBean.getCapitalNecesarioFallecimiento();
                    break;
                case 7:
                    montoTotalTemp = adnBean.getMontoMensualInvertir();
                    break;
            }
            this.txtMontoTotalDolares.setText(String.format("%s %s", this.getString(com.pacifico.adn.R.string.simbolo_dolares), Util.obtenerNumeroConFormato(montoTotalTemp)));
        }
        double montoTotalSoles = montoTotalTemp * adnBean.getTipoCambio();
        this.txtMontoTotalSoles.setText(String.format("En Soles %s %s", this.getString(com.pacifico.adn.R.string.simbolo_soles), Util.obtenerNumeroConFormato(montoTotalSoles)));
    }
    public void actualizarTituloMontoTotal(){
        String[] titles = {"Total activos realizables", "Total seguros de vida", "Total pensión mensual AFP", "Total ingreso mensual familiar", "Total gasto mensual familiar",
                "Déficit mensual", "Capital necesario en caso de fallecimiento", "Monto mensual a invertir"};
        this.txtTituloTotal.setText(titles[posicionActual]);
    }
    @Override
    public void onClick(View v){
        if(v.equals(imgBtnAtras)){
            posicionActual--;
        }else{
            posicionActual++;
        }
        resumenFragment.setFilaSeleccionada(posicionActual);
        actualizarContenido();
    }
    public void actualizarResumen(){
        ((ResumenArrayAdapter)resumenFragment.getItemsListView().getAdapter()).notifyDataSetChanged();
    }
    public void cambiarPosicionalActualPorResumen(int posicion){
        if(posicion < adnBean.getIndicadorVentana()){
            posicionActual = posicion;
            actualizarContenido();
        }
    }
    public void actualizarContenido(){
        if(actualFragment != null){
            if(actualFragment instanceof ActivosFragment){
                ((ActivosFragment) actualFragment).clearStepActual();
            }else if(actualFragment instanceof SegurosVidaFragment){
                ((SegurosVidaFragment)actualFragment).clearStepActual();
            }else if(actualFragment instanceof PensionFragment){
                ((PensionFragment)actualFragment).clearStepActual();
            }else if(actualFragment instanceof IngresoFamiliarFragment){
                ((IngresoFamiliarFragment)actualFragment).clearStepActual();
            }else if(actualFragment instanceof GastosMensualFragment){
                clearFocusStepTotalGastoFamiliares();
                ((GastosMensualFragment)actualFragment).clearStepActual();
            }else if(actualFragment instanceof CapitalNecesarioFragment){
                ((CapitalNecesarioFragment)actualFragment).clearStepActual();
            }else if(actualFragment instanceof IngresoMensualFragment){
                ((IngresoMensualFragment)actualFragment).clearEdtMontoFocus();
            }
        }
        if(posicionActual == -1){
            posicionActual = 0;
            adnActivity.mostrarProspecto();
        }else{
            if(posicionActual < 8){
                if(posicionActual == 7){
                    if(adnActivity.getAdnBean().getAniosProteger() == 0){
                        posicionActual--;
                        resumenFragment.setFilaSeleccionada(posicionActual);
                        Snackbar.make(getView(), "Los años a proteger debe ser mayor a 0.", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                switch(posicionActual){
                    case 0:
                        actualFragment = new ActivosFragment();
                        break;
                    case 1:
                        actualFragment = new SegurosVidaFragment();
                        break;
                    case 2:
                        actualFragment = new PensionFragment();
                        break;
                    case 3:
                        actualFragment = new IngresoFamiliarFragment();
                        break;
                    case 4:
                        actualFragment = new GastosMensualFragment();
                        ((GastosMensualFragment)actualFragment).setFlujoADNFragment(this);
                        break;
                    case 5:
                        actualFragment = new IngresoNecesarioFragment();
                        break;
                    case 6:
                        actualFragment = new CapitalNecesarioFragment();
                        break;
                    case 7:
                        actualFragment = new IngresoMensualFragment();
                        break;
                }
                getActivity().getSupportFragmentManager().beginTransaction().replace(com.pacifico.adn.R.id.fraLayContenedor, actualFragment).commit();
                if(adnBean.getIndicadorVentana() < (posicionActual + 1)){
                    adnBean.setIndicadorVentana(posicionActual + 1);
                    adnBean.setFlagModificado(1);
                }
                actualizarResumen();
                actualizarTituloMontoTotal();
                actualizarMontoTotal();
            }else{
                if(adnBean.getPorcentajeInversion() == -1){
                    posicionActual--;
                    resumenFragment.setFilaSeleccionada(posicionActual);
                    Snackbar.make(getView(), "Seleccione una opción", Snackbar.LENGTH_LONG).show();
                }else{
                    adnActivity.mostrarMetodoPago();
                }
            }
        }
        adnActivity.guardarAdn();
    }
    public void setPosicionActual(int posicionActual) {
        this.posicionActual = posicionActual;
    }
    public int getPosicionActual() {
        return posicionActual;
    }
    @Override
    public void onChangeValue(StepperView stepView){
        int nuevoTotalGastoFamiliarMensual = stepView.getValorActual();
        if(nuevoTotalGastoFamiliarMensual != adnBean.getTotalGastoFamiliarMensual()){
            adnBean.setTotalGastoFamiliarMensual(nuevoTotalGastoFamiliarMensual);
            adnBean.setFlagModificado(1);
        }
        actualizarMontoTotal();
    }
    public void clearFocusStepTotalGastoFamiliares(){
        if(stepTotalGastoFamiliaresHasFocus){
            stepTotalGastoFamiliares.clearFocus();
        }
    }
}