package com.pacifico.adn.Util;

import java.text.DecimalFormat;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by vctrls3477 on 6/08/16.
 */
public class NumberTextWatcher implements TextWatcher {
    private DecimalFormat dfnd;
    private EditText et;
    public NumberTextWatcher(EditText et){
        dfnd = Util.obtenerDecimalFormat("##,###,###");
        this.et = et;
    }
    @Override
    public void afterTextChanged(Editable s) {
        et.removeTextChangedListener(this);
        try{
            int inilen, endlen;
            inilen = et.getText().length();
            String v = s.toString().replace(",", "");
            int n = Integer.parseInt(v);
            int cp = et.getSelectionStart();
            et.setText(dfnd.format(n));
            endlen = et.getText().length();
            int sel = (cp + (endlen - inilen));
            if (sel > 0 && sel <= et.getText().length()) {
                et.setSelection(sel);
            } else {
                et.setSelection(et.getText().length() - 1);
            }
        }catch(NumberFormatException nfe){
            // do nothing?
        }
        et.addTextChangedListener(this);
    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after){}
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count){}
}