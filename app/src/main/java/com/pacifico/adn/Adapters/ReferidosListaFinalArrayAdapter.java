package com.pacifico.adn.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Fragment.ReferidosListaFinalFragment;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Model.Bean.TablaTablasBean;
import com.pacifico.adn.Model.Controller.TablasGeneralesController;
import com.pacifico.adn.R;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.CustomEditText;
import com.pacifico.adn.Views.CustomView.CustomTextView;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 3/07/16.
 */
public class ReferidosListaFinalArrayAdapter extends ArrayAdapter<ReferidoBean>{
    private ADNActivity adnActivity;
    private ReferidosListaFinalFragment referidosListaFinalFragment;
    private ArrayList<TablaTablasBean> arrEdades, arrTipoEmpleos;
    private String[] arrEdadesTitulos, arrTipoEmpleosTitulos;
    public ReferidosListaFinalArrayAdapter(ReferidosListaFinalFragment nReferidosListaFinalFragment){
        super(nReferidosListaFinalFragment.getContext(), -1);
        referidosListaFinalFragment = nReferidosListaFinalFragment;
        adnActivity = (ADNActivity)referidosListaFinalFragment.getActivity();
        arrEdades = TablasGeneralesController.obtenerTablaTablasPorIdTabla(3);
        arrTipoEmpleos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(19);
        arrEdadesTitulos = new String[arrEdades.size() + 1];
        arrEdadesTitulos[0] = "Edad";
        for(int i = 0; i < arrEdades.size(); i++){
            arrEdadesTitulos[i + 1] = arrEdades.get(i).getValorCadena();
        }
        arrTipoEmpleosTitulos = new String[arrTipoEmpleos.size() + 1];
        arrTipoEmpleosTitulos[0] = "Seleccione";
        for(int i = 0; i < arrTipoEmpleos.size(); i++){
            arrTipoEmpleosTitulos[i + 1] = arrTipoEmpleos.get(i).getValorCadena();
        }
    }
    @Override
    public int getCount(){
        return adnActivity.getArrReferidosAmigos().size() + adnActivity.getArrReferidosFamiliar().size() + adnActivity.getArrReferidosTrabajo().size() + adnActivity.getArrReferidosOtros().size() + 1;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(position == 0){
            View view = inflater.inflate(R.layout.cabecera_referidos_lista_final, parent, false);
            RadioGroup rgHijos = (RadioGroup)view.findViewById(R.id.rgHijos);
            rgHijos.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId){
                    int flagHijo = checkedId == R.id.rbSinHijos ? 0 : 1;
                    for(ReferidoBean referidoBean : adnActivity.getArrReferidosFamiliar()){
                        referidoBean.setFlagHijo(flagHijo);
                        referidoBean.setFlagModificado(1);
                    }
                    for(ReferidoBean referidoBean : adnActivity.getArrReferidosAmigos()){
                        referidoBean.setFlagHijo(flagHijo);
                        referidoBean.setFlagModificado(1);
                    }
                    for(ReferidoBean referidoBean : adnActivity.getArrReferidosTrabajo()){
                        referidoBean.setFlagHijo(flagHijo);
                        referidoBean.setFlagModificado(1);
                    }
                    for(ReferidoBean referidoBean : adnActivity.getArrReferidosOtros()){
                        referidoBean.setFlagHijo(flagHijo);
                        referidoBean.setFlagModificado(1);
                    }
                    notifyDataSetChanged();
                }
            });
            return view;
        }else{
            View view = inflater.inflate(R.layout.row_referido_lista_final, parent, false);
            ImageButton imgBtnTipoReferido = (ImageButton)view.findViewById(R.id.imgBtnTipoReferido);
            CustomTextView edtNombre = (CustomTextView)view.findViewById(R.id.edtNombre);
            CustomTextView edtApellido = (CustomTextView)view.findViewById(R.id.edtApellido);
            Spinner spiEdad = (Spinner)view.findViewById(R.id.spiEdad);
            RadioGroup rgHijos = (RadioGroup)view.findViewById(R.id.rgHijos);
            Spinner spiTipoEmpleo = (Spinner)view.findViewById(R.id.spiTipoEmpleo);
            CustomEditText edtTelefono = (CustomEditText)view.findViewById(R.id.edtTelefono);
            int indice = position - 1;
            final ReferidoBean referidoBean;
            if(indice <  adnActivity.getArrReferidosFamiliar().size()){
                referidoBean = adnActivity.getArrReferidosFamiliar().get(indice);
                imgBtnTipoReferido.setImageResource(R.drawable.referido_familia);
            }else{
                indice = indice - adnActivity.getArrReferidosFamiliar().size();
                if(indice < adnActivity.getArrReferidosAmigos().size()){
                    referidoBean = adnActivity.getArrReferidosAmigos().get(indice);
                    imgBtnTipoReferido.setImageResource(R.drawable.referido_amigo);
                }else{
                    indice = indice - adnActivity.getArrReferidosAmigos().size();
                    if(indice < adnActivity.getArrReferidosTrabajo().size()){
                        referidoBean = adnActivity.getArrReferidosTrabajo().get(indice);
                        imgBtnTipoReferido.setImageResource(R.drawable.referido_trabajo);
                    }else{
                        indice = indice - adnActivity.getArrReferidosTrabajo().size();
                        referidoBean = adnActivity.getArrReferidosOtros().get(indice);
                        imgBtnTipoReferido.setImageResource(R.drawable.referido_otros);
                    }
                }
            }
            edtNombre.setText(Util.capitalizedString(referidoBean.getNombres()));
            edtApellido.setText(Util.capitalizedString(referidoBean.getApellidoPaterno()));
            DatoReferidoArrayAdapter edadArrayAdapter = new DatoReferidoArrayAdapter(adnActivity, arrEdadesTitulos);
            spiEdad.setAdapter(edadArrayAdapter);
            int codigoRangoEdad = referidoBean.getCodigoRangoEdad();
            if(codigoRangoEdad != -1){
                int indiceEdad = -1;
                for(int i = 0; i < arrEdades.size(); i++){
                    int idEdad = arrEdades.get(i).getCodigoCampo();
                    if(idEdad == codigoRangoEdad){
                        indiceEdad = i;
                        break;
                    }
                }
                if(indice > -1){
                    spiEdad.setSelection(indiceEdad + 1);
                }
            }
            spiEdad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int nuevoCodigoRangoEdad = -1;
                    if (position > 0) {
                        nuevoCodigoRangoEdad = arrEdades.get(position - 1).getCodigoCampo();
                    }
                    if (position != referidoBean.getCodigoRangoEdad()) {
                        referidoBean.setCodigoRangoEdad(nuevoCodigoRangoEdad);
                        referidoBean.setFlagModificado(1);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            spiEdad.setOnTouchListener(new View.OnTouchListener(){
                @Override
                public boolean onTouch(View v, MotionEvent event){
                    if(event.getAction() == MotionEvent.ACTION_DOWN){
                        if(referidosListaFinalFragment.getEdtActual() != null){
                            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(referidosListaFinalFragment.getEdtActual().getWindowToken(), 0);
                            referidosListaFinalFragment.getEdtActual().clearFocus();
                            referidosListaFinalFragment.setEdtActual(null);
                        }
                    }
                    return false;
                }
            });
            if(referidoBean.getFlagHijo() != -1){
                rgHijos.check(referidoBean.getFlagHijo() == 1 ? R.id.rbConHijos : R.id.rbSinHijos);
            }
            rgHijos.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    referidoBean.setFlagHijo(checkedId == R.id.rbSinHijos ? 0 : 1);
                    referidoBean.setFlagModificado(1);
                }
            });
            edtTelefono.setText(referidoBean.getTelefono());
            edtTelefono.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    CustomEditText edtTelefono = (CustomEditText) v;
                    if(hasFocus){
                        referidosListaFinalFragment.setEdtActual(edtTelefono);
                    } else {
                        String nuevoTelefono = edtTelefono.getText().toString();
                        if (!nuevoTelefono.equals(referidoBean.getTelefono())) {
                            referidoBean.setTelefono(nuevoTelefono);
                            referidoBean.setFlagModificado(1);
                        }
                        referidosListaFinalFragment.setEdtActual(null);
                    }
                }
            });
            DatoReferidoArrayAdapter tipoEmpleoArrayAdapter = new DatoReferidoArrayAdapter(adnActivity, arrTipoEmpleosTitulos);
            spiTipoEmpleo.setAdapter(tipoEmpleoArrayAdapter);
            int codigoTipoEmpleo = referidoBean.getCodigoTipoEmpleo();
            if(codigoTipoEmpleo != 0){
                int indiceTipoEmpleo = -1;
                for(int i = 0; i < arrTipoEmpleos.size(); i++){
                    if(arrTipoEmpleos.get(i).getCodigoCampo() == codigoTipoEmpleo){
                        indiceTipoEmpleo = i;
                        break;
                    }
                }
                if(indiceTipoEmpleo > -1){
                    spiTipoEmpleo.setSelection(indiceTipoEmpleo + 1);
                }
            }
            spiTipoEmpleo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int nuevoCodigoTipoEmpleo = -1;
                    if (position > 0) {
                        nuevoCodigoTipoEmpleo = arrTipoEmpleos.get(position - 1).getCodigoCampo();
                    }
                    if (nuevoCodigoTipoEmpleo != referidoBean.getCodigoTipoEmpleo()) {
                        referidoBean.setCodigoTipoEmpleo(nuevoCodigoTipoEmpleo);
                        referidoBean.setFlagModificado(1);
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            spiTipoEmpleo.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        if (referidosListaFinalFragment.getEdtActual() != null) {
                            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(referidosListaFinalFragment.getEdtActual().getWindowToken(), 0);
                            referidosListaFinalFragment.getEdtActual().clearFocus();
                            referidosListaFinalFragment.setEdtActual(null);
                        }
                    }
                    return false;
                }
            });
            return view;
        }
    }
}