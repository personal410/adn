package com.pacifico.adn.Views.CustomView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.pacifico.adn.Util.NumberTextWatcher;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.Interface.OnDoneSelectedListener;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;
import com.pacifico.adn.Views.Interface.OnStepperDoneSelectedListener;

/**
 * Created by victorsalazar on 17/03/16.
 */
public class StepperView extends LinearLayout implements View.OnTouchListener {
    final int TiempoRetardoInicial = 500;
    final int TiempoRetardoFinal = 100;
    final int TiempoAvance = 100;
    int valorActual, valorAvance, maximoValor;
    int tiempoRetardo = TiempoRetardoInicial;
    boolean incrementar, disminuir;
    CustomEditText edtValor;
    private OnStepperChangeValueListener onStepperChangeValueListener;
    private OnStepperDoneSelectedListener onStepperDoneSelectedListener;
    private OnFocusChangeListener onFocusChangeListener;
    private Handler repeaterUpdaterHandler = new Handler();
    public StepperView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setBackgroundColor(context.getResources().getColor(com.pacifico.adn.R.color.colorBorderStepper));
        this.setFocusableInTouchMode(true);
        this.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        this.setFocusable(true);
        this.setPadding(1, 1, 1, 1);
        this.setOrientation(HORIZONTAL);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, com.pacifico.adn.R.styleable.StepperView);
        valorActual = typedArray.getInteger(com.pacifico.adn.R.styleable.StepperView_startValue, 0);
        valorAvance = typedArray.getInteger(com.pacifico.adn.R.styleable.StepperView_stepValue, 100);
        maximoValor = typedArray.getInteger(com.pacifico.adn.R.styleable.StepperView_maxValue, 10000000);
        edtValor = new CustomEditText(getContext());
        Button minusBtn = new Button(this.getContext());
        LinearLayout.LayoutParams paramsMinusBtn = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsMinusBtn.weight = 1;
        minusBtn.setLayoutParams(paramsMinusBtn);
        minusBtn.setText("-");
        minusBtn.setBackgroundColor(getResources().getColor(com.pacifico.adn.R.color.colorButtonsStepper));
        minusBtn.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorRadioButton));
        minusBtn.setTextSize(25);
        minusBtn.setPadding(0, 0, 0, 0);
        minusBtn.setOnTouchListener(this);
        this.addView(minusBtn);
        LinearLayout.LayoutParams paramsEdtValue = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsEdtValue.weight = 3;
        paramsEdtValue.leftMargin = 1;
        paramsEdtValue.rightMargin = 1;
        edtValor.setLayoutParams(paramsEdtValue);
        edtValor.addTextChangedListener(new NumberTextWatcher(edtValor));
        edtValor.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        edtValor.setTypeface(Typeface.DEFAULT_BOLD);
        edtValor.setOnDoneSelectedListener(new OnDoneSelectedListener() {
            @Override
            public void onDoneSelected(EditText editText) {
                if (onStepperDoneSelectedListener != null) {
                    onStepperDoneSelectedListener.onStepperDoneSelected(StepperView.this);
                }
            }
        });
        edtValor.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        edtValor.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtValor.setBackgroundColor(getResources().getColor(com.pacifico.adn.R.color.colorBlanco));
        edtValor.setGravity(Gravity.CENTER_VERTICAL);
        edtValor.setSelectAllOnFocus(true);
        edtValor.setPadding(0, 0, 0, 0);
        edtValor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    edtValor.setText(Integer.toString(valorActual));
                    edtValor.selectAll();
                    if(onFocusChangeListener != null){
                        onFocusChangeListener.onFocusChange(true, StepperView.this);
                    }
                }else{
                    if (edtValor.getText().length() > 0) {
                        valorActual = Integer.parseInt(edtValor.getText().toString().replace(",", ""));
                        if (valorActual > maximoValor) {
                            valorActual = maximoValor;
                        }
                    } else {
                        valorActual = 0;
                    }
                    updateTextView(true);
                    if(onFocusChangeListener != null){
                        onFocusChangeListener.onFocusChange(false, StepperView.this);
                    }
                }
            }
        });
        this.addView(edtValor);
        Button plusBtn = new Button(this.getContext());
        LinearLayout.LayoutParams paramsPlusBtn = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsPlusBtn.weight = 1;
        plusBtn.setLayoutParams(paramsPlusBtn);
        plusBtn.setText("+");
        plusBtn.setTextSize(25);
        plusBtn.setPadding(0, 0, 0, 0);
        plusBtn.setBackgroundColor(getResources().getColor(com.pacifico.adn.R.color.colorButtonsStepper));
        plusBtn.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorRadioButton));
        plusBtn.setOnTouchListener(this);
        this.addView(plusBtn);
        updateTextView(false);
        typedArray.recycle();
    }
    private void updateTextView(boolean notifyListener){
        if(valorActual == -1){
            edtValor.setText("");
        }else{
            edtValor.setText(Util.obtenerDecimalFormat("##,###,###").format(valorActual));
        }
        if(onStepperChangeValueListener != null && notifyListener){
            onStepperChangeValueListener.onChangeValue(this);
        }
    }
    public int getValorActual(){
        return valorActual;
    }
    public void setValorActual(int valorActual) {
        this.valorActual = valorActual;
        updateTextView(false);
    }
    public void incrementarValor(){
        valorActual += valorAvance;
        if(valorActual > maximoValor){
            valorActual = maximoValor;
        }
        updateTextView(true);
    }
    public void disminuirValor(){
        valorActual -= valorAvance;
        if (valorActual < 0) {
            valorActual = 0;
        }
        updateTextView(true);
    }
    @Override
    public boolean onTouch(View v, MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_UP){
            incrementar = false;
            disminuir = false;
            tiempoRetardo = TiempoRetardoInicial;
        }else if(event.getAction() == MotionEvent.ACTION_DOWN){
            Button button = (Button)v;
            if(button.getText().toString().equals("+")){
                incrementarValor();
                incrementar = true;
                repeaterUpdaterHandler.postDelayed(new RepeatUpdater(), tiempoRetardo);
            }else{
                disminuirValor();
                disminuir = true;
                repeaterUpdaterHandler.postDelayed(new RepeatUpdater(), tiempoRetardo);
            }
        }
        return false;
    }
    public void fijarCantidadMaximaDigitos(int cantidadMaximaCaracteres){
        edtValor.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cantidadMaximaCaracteres)});
    }
    public void setOnStepperChangeValueListener(OnStepperChangeValueListener listener){
        onStepperChangeValueListener = listener;
    }
    public void setOnStepperDoneSelectedListener(OnStepperDoneSelectedListener onStepperDoneSelectedListener) {
        this.onStepperDoneSelectedListener = onStepperDoneSelectedListener;
    }
    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.onFocusChangeListener = onFocusChangeListener;
    }
    public void requestStepperFocus(){
        edtValor.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(edtValor, 0);
    }
    public void clearFocus(){
        edtValor.clearFocus();
        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getWindowToken(), 0);
    }
    class RepeatUpdater implements Runnable{
        public void run(){
            if(tiempoRetardo > TiempoRetardoFinal){
                tiempoRetardo = tiempoRetardo - TiempoAvance;
            }
            if(incrementar){
                incrementarValor();
                repeaterUpdaterHandler.postDelayed(new RepeatUpdater(), tiempoRetardo);
            }else{
                if(disminuir){
                    disminuirValor();
                    repeaterUpdaterHandler.postDelayed(new RepeatUpdater(), tiempoRetardo);
                }
            }
        }
    }
    public interface OnFocusChangeListener {
        void onFocusChange(boolean hasFocus, StepperView stepperView);
    }
}