package com.pacifico.adn.Views.Interface;

import com.pacifico.adn.Views.CustomView.StepperView;

/**
 * Created by vctrls3477 on 18/08/16.
 */
public interface OnStepperDoneSelectedListener{
    void onStepperDoneSelected(StepperView stepperView);
}