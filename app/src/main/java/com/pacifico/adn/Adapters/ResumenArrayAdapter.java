package com.pacifico.adn.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Fragment.ResumenFragment;
import com.pacifico.adn.Util.Fuente;
import com.pacifico.adn.Util.Util;

import java.util.Arrays;
import java.util.List;

public class ResumenArrayAdapter extends ArrayAdapter<String> {
    private ADNActivity adnActivity;
    private ResumenFragment resumenFragment;
    private List<String> arrTitulos = Arrays.asList("Activos\nRealizables", "Seguros\nde vida", "Pensión\nde AFP", "Ingresos\nmensuales", "Gastos\nmensuales", "Ingreso\nnecesario", "Capital\nnecesario");
    public ResumenArrayAdapter(ResumenFragment nResumenFragment){
        super(nResumenFragment.getContext(), -1);
        resumenFragment = nResumenFragment;
        adnActivity = (ADNActivity)nResumenFragment.getContext();
    }
    @Override
    public int getCount(){
        return arrTitulos.size();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(com.pacifico.adn.R.layout.row_detalle_resumen, parent, false);
        String titulo = arrTitulos.get(position);
        ImageView itemIsFilledImgView = (ImageView)rowView.findViewById(com.pacifico.adn.R.id.itemIsFilledImgView);
        TextView txtTitulo = (TextView)rowView.findViewById(com.pacifico.adn.R.id.txtTitulo);
        TextView txtMonto = (TextView)rowView.findViewById(com.pacifico.adn.R.id.txtMonto);
        Fuente.setFuenteBd(getContext(), txtTitulo);
        Fuente.setFuenteRg(getContext(), txtMonto);
        View viewTop = rowView.findViewById(com.pacifico.adn.R.id.viewTop);
        View viewBottom = rowView.findViewById(com.pacifico.adn.R.id.viewBottom);
        int colorVerde = Color.rgb(17, 145, 17);
        int colorVerdeClaro = Color.rgb(153, 204, 153);
        int colorGrisClaro = Color.rgb(170, 170, 170);
        boolean top = position < adnActivity.getAdnBean().getIndicadorVentana();
        boolean bottom = (position + 1) < adnActivity.getAdnBean().getIndicadorVentana();
        itemIsFilledImgView.setColorFilter(top ? (position == resumenFragment.getFilaSeleccionada() ? colorVerde : colorVerdeClaro) : colorGrisClaro);
        //itemIsFilledImgView.setAlpha((float)0.7);
        viewTop.setBackgroundColor(top ? colorVerde : colorGrisClaro);
        viewBottom.setBackgroundColor(bottom ? colorVerde : colorGrisClaro);
        txtTitulo.setText(titulo);
        double amount = 0;
        switch (position){
            case 0:
                amount = adnActivity.getAdnBean().getTotalActivoRealizable();
                break;
            case 1:
                amount = adnActivity.getAdnBean().getTotalSegurosVida();
                break;
            case 2:
                amount = adnActivity.getAdnBean().getTotalPensionMensualAFP();
                break;
            case 3:
                amount = adnActivity.getAdnBean().getTotalIngresoFamiliarMensual();
                break;
            case 4:
                amount = adnActivity.getAdnBean().getTotalGastoFamiliarMensual();
                break;
            case 5:
                amount = adnActivity.getAdnBean().getDeficitMensual();
                break;
            case 6:
                amount = adnActivity.getAdnBean().getCapitalNecesarioFallecimiento();
                break;
        }
        txtMonto.setText(Util.obtenerMontoDolaresConFomato(amount));
        if(position == 6){
            viewBottom.setVisibility(View.INVISIBLE);
        }
        return rowView;
    }
}