package com.pacifico.adn.Fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.ResumenArrayAdapter;

public class ResumenFragment extends Fragment {
    private ListView itemsListView;
    private int filaSeleccionada = 0;
    private ResumenArrayAdapter resumenArrayAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_resumen_adn, container, false);
        itemsListView = (ListView)view.findViewById(com.pacifico.adn.R.id.itemsListView);
        itemsListView.setDividerHeight(0);
        resumenArrayAdapter = new ResumenArrayAdapter(this);
        itemsListView.setAdapter(resumenArrayAdapter);
        itemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                ADNActivity adnActivity = (ADNActivity)ResumenFragment.this.getActivity();
                adnActivity.getFlujoADNFragment().cambiarPosicionalActualPorResumen(position);
                filaSeleccionada = position;
                resumenArrayAdapter.notifyDataSetChanged();
            }
        });
        return view;
    }
    public ListView getItemsListView(){
        return itemsListView;
    }

    public int getFilaSeleccionada() {
        return filaSeleccionada;
    }

    public void setFilaSeleccionada(int filaSeleccionada) {
        this.filaSeleccionada = filaSeleccionada;
        resumenArrayAdapter.notifyDataSetChanged();
    }
}