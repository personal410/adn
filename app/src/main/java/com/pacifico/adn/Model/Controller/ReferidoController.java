package com.pacifico.adn.Model.Controller;

import android.database.Cursor;

import com.pacifico.adn.Activity.ADNApplication;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Persistence.DatabaseConstants;
import com.dsbmobile.dsbframework.controller.persistence.Entity;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 4/07/16.
 */
public class ReferidoController{
    public static void guardarReferido(ReferidoBean referidoBean){
        ReferidoBean.tableHelper.insertEntity(referidoBean);
    }
    public static void actualizarReferido(ReferidoBean referidoBean){
        String sentencia = "IDREFERIDODISPOSITIVO = ?";
        String[] parametros = new String[]{Integer.toString(referidoBean.getIdReferidoDispositivo())};
        ReferidoBean.tableHelper.updateEntity(referidoBean, sentencia, parametros);
    }
    public static ArrayList<ReferidoBean> obtenerReferidoPorTipoReferido(int IdProspectoDispositivo, int codigoTipoReferido){
        String[] parametros = new String[]{Integer.toString(IdProspectoDispositivo), Integer.toString(codigoTipoReferido)};
        String sentencia = "IDPROSPECTODISPOSITIVO = ? AND CODIGOTIPOREFERIDO = ? AND FLAGACTIVO = 1";
        ArrayList<Entity> arrReferidos = ReferidoBean.tableHelper.getEntities(sentencia, parametros);
        ArrayList<ReferidoBean> arrReferidosFinal = new ArrayList<>();
        if(arrReferidos != null){
            for(Entity referido : arrReferidos){
                ReferidoBean referidoBean = (ReferidoBean)referido;
                if(referidoBean.getTelefono() != null && referidoBean.getTelefono().length() > 5 && referidoBean.getTelefono().length() < 10){
                    referidoBean.setFlagProspectoCreado(1);
                }
                arrReferidosFinal.add((ReferidoBean)referido);
            }
        }
        return arrReferidosFinal;
    }
    public static void guardaListaReferido(ArrayList<ReferidoBean> listaReferidoBean){
        for(ReferidoBean referidoBean : listaReferidoBean){
            if(referidoBean.getIdProspectoDispositivo() == -1){
                ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(referidoBean.getIdProspecto());
                referidoBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
            }
            ReferidoBean.tableHelper.insertEntity(referidoBean);
        }
    }
    public static ArrayList<ReferidoBean> obtenerReferidosSinEnviar(){
        ArrayList<Entity> arrReferidos = ReferidoBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ReferidoBean> arrReferidosFinal = new ArrayList<>();
        for(Entity entity : arrReferidos){
            arrReferidosFinal.add((ReferidoBean) entity);
        }
        return arrReferidosFinal;
    }
    public static ReferidoBean  obtenerReferidoPorIdReferidoDispositivo(int idReferidoDispositivo){
        String sentencia = "IDREFERIDODISPOSITIVO = ?";
        String[] parametros = new String[]{Integer.toString(idReferidoDispositivo)};
        ArrayList<Entity> arrReferidos = ReferidoBean.tableHelper.getEntities(sentencia, parametros);
        return (ReferidoBean)arrReferidos.get(0);
    }
    public static int obtenerMaximoIdReferidoDispositivo(){
        int idReferidoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDREFERIDODISPOSITIVO) FROM " + DatabaseConstants.TBL_REFERIDO;
        Cursor cursor = ADNApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idReferidoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idReferidoDispositivoMax;
    }
    public static void limpiarTablaReferido(){
        ReferidoBean.tableHelper.deleteAllEntities();
    }
}