package com.pacifico.adn.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Activity.BuscarProspectoActivity;
import com.pacifico.adn.Activity.InicioSesionActivity;
import com.pacifico.adn.Adapters.ReferidosListaFinalArrayAdapter;
import com.pacifico.adn.Model.Bean.CitaBean;
import com.pacifico.adn.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.adn.Model.Bean.TablaTablasBean;
import com.pacifico.adn.Model.Controller.CitaReunionController;
import com.pacifico.adn.Model.Controller.TablasGeneralesController;
import com.pacifico.adn.Network.RespuestaSincronizacionListener;
import com.pacifico.adn.Network.SincronizacionController;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReferidosListaFinalFragment extends Fragment{
    public static final String parametro_idProspecto = "parametro_idProspecto";
    private ADNActivity adnActivity;
    private ProgressDialog progressDialog;
    private EditText edtActual;
    private boolean sincronizando = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        adnActivity = (ADNActivity)getActivity();
        final View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_referidos_lista_final, container, false);
        ImageButton imgBtnAtras = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnAtras);
        imgBtnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adnActivity.mostrarReferidos();
            }
        });
        Button btnTerminarAgendar = (Button)view.findViewById(com.pacifico.adn.R.id.btnTerminarAgendar);
        if(adnActivity.getProspectoBean().getCodigoEtapa() != 2){
            btnTerminarAgendar.setText("terminar");
        }
        btnTerminarAgendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View dialogVerificarCorreo = layoutInflater.inflate(com.pacifico.adn.R.layout.dialog_verificar_correo, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(dialogVerificarCorreo);
                final EditText edtCorreo = (EditText) dialogVerificarCorreo.findViewById(com.pacifico.adn.R.id.edtCorreo);
                edtCorreo.setText(adnActivity.getProspectoBean().getCorreoElectronico1());
                TextView TxtEnviar = (TextView) dialogVerificarCorreo.findViewById(com.pacifico.adn.R.id.btnEnviar);
                TextView TxtCancelar = (TextView) dialogVerificarCorreo.findViewById(com.pacifico.adn.R.id.btnCancelar);
                builder.setCancelable(false);
                Spinner spiTipoDocumento = (Spinner)dialogVerificarCorreo.findViewById(com.pacifico.adn.R.id.spiTipoDocumento);
                final ArrayList<TablaTablasBean> arrTipoDocumentos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(5);
                ArrayList<String> arrTitulosTipoDocumentos = new ArrayList<>();
                int indiceTipoDocumentoSeleccionado = 0;
                for(int i = 0; i < arrTipoDocumentos.size(); i++){
                    TablaTablasBean tablaTablasBean = arrTipoDocumentos.get(i);
                    arrTitulosTipoDocumentos.add(tablaTablasBean.getValorCadena());
                    if(tablaTablasBean.getCodigoCampo() == adnActivity.getProspectoBean().getCodigoTipoDocumento()){
                        indiceTipoDocumentoSeleccionado = i;
                    }
                }
                final EditText edtNumeroDocumento = (EditText)dialogVerificarCorreo.findViewById(com.pacifico.adn.R.id.edtNumeroDocumento);
                ArrayAdapter<String> arrTipoDocumento = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, arrTitulosTipoDocumentos);
                spiTipoDocumento.setAdapter(arrTipoDocumento);
                spiTipoDocumento.setSelection(indiceTipoDocumentoSeleccionado);
                spiTipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        int indiceSeleccionadoTipoDocumento = arrTipoDocumentos.get(position).getCodigoCampo();
                        if (indiceSeleccionadoTipoDocumento != adnActivity.getProspectoBean().getCodigoTipoDocumento()) {
                            adnActivity.getProspectoBean().setCodigoTipoDocumento(indiceSeleccionadoTipoDocumento);
                            adnActivity.getProspectoBean().setFlagModificado(1);
                        }
                        TextInputLayout documentoTextInputLayout = (TextInputLayout)edtNumeroDocumento.getParent();
                        if(adnActivity.getProspectoBean().getCodigoTipoDocumento() == 1){
                            if(edtNumeroDocumento.getText().toString().length() != 8){
                                documentoTextInputLayout.setError("Por favor ingresa un DNI válido (ej: 44312341)");
                            }else{
                                documentoTextInputLayout.setError(null);
                            }
                        }else if(adnActivity.getProspectoBean().getCodigoTipoDocumento() == 2){
                            if(edtNumeroDocumento.getText().toString().length() != 11){
                                documentoTextInputLayout.setError("Por favor ingresa un RUC válido (ej: 10443123418)");
                            }else{
                                documentoTextInputLayout.setError(null);
                            }
                        }else if(adnActivity.getProspectoBean().getCodigoTipoDocumento() == 3) {
                            if (edtNumeroDocumento.getText().toString().length() != 9) {
                                documentoTextInputLayout.setError("Por favor ingresa un CE válido (ej: 123456789)");
                            } else {
                                documentoTextInputLayout.setError(null);
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent){}
                });
                edtNumeroDocumento.setText(adnActivity.getProspectoBean().getNumeroDocumento());
                edtNumeroDocumento.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after){}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count){}
                    @Override
                    public void afterTextChanged(Editable s){
                        String nuevoNumeroDocumento = edtNumeroDocumento.getText().toString();
                        TextInputLayout documentoTextInputLayout = (TextInputLayout)edtNumeroDocumento.getParent();
                        if(nuevoNumeroDocumento.length() > 0){
                            if(adnActivity.getProspectoBean().getCodigoTipoDocumento() == 1){
                                if(edtNumeroDocumento.getText().toString().length() != 8){
                                    documentoTextInputLayout.setError("Por favor ingresa un DNI válido (ej: 44312341)");
                                }else{
                                    documentoTextInputLayout.setError(null);
                                }
                            }else if(adnActivity.getProspectoBean().getCodigoTipoDocumento() == 2){
                                if(edtNumeroDocumento.getText().toString().length() != 11){
                                    documentoTextInputLayout.setError("Por favor ingresa un RUC válido (ej: 10443123418)");
                                }else{
                                    documentoTextInputLayout.setError(null);
                                }
                            }else if(adnActivity.getProspectoBean().getCodigoTipoDocumento() == 3) {
                                if (edtNumeroDocumento.getText().toString().length() != 9) {
                                    documentoTextInputLayout.setError("Por favor ingresa un CE válido (ej: 123456789)");
                                } else {
                                    documentoTextInputLayout.setError(null);
                                }
                            }
                        }else{
                            documentoTextInputLayout.setError(null);
                        }
                    }
                });
                final AlertDialog dialog = builder.create();
                TxtEnviar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int nuevoFlagEnviarADNDigital = adnActivity.getAdnBean().getIngresoMensualTitular() > 0 ? 1 : 0;
                        if((nuevoFlagEnviarADNDigital != adnActivity.getProspectoBean().getFlagEnviarADNDigital() || nuevoFlagEnviarADNDigital == 1) && adnActivity.getAdnModificado()){
                            adnActivity.getProspectoBean().setFlagEnviarADNDigital(nuevoFlagEnviarADNDigital);
                            adnActivity.getProspectoBean().setFlagModificado(1);
                        }
                        TextInputLayout textInputLayout = (TextInputLayout) edtCorreo.getParent();
                        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
                        Matcher matcher = pattern.matcher(edtCorreo.getText().toString());
                        if(matcher.matches()){
                            String nuevoCorreo = edtCorreo.getText().toString();
                            if(!nuevoCorreo.equals(adnActivity.getProspectoBean().getCorreoElectronico1())){
                                adnActivity.getProspectoBean().setCorreoElectronico1(nuevoCorreo);
                                adnActivity.getProspectoBean().setFlagModificado(1);
                            }
                            String nuevoNumeroDocumento = edtNumeroDocumento.getText().toString();
                            if(!nuevoNumeroDocumento.equals(adnActivity.getProspectoBean().getNumeroDocumento())){
                                adnActivity.getProspectoBean().setNumeroDocumento(nuevoNumeroDocumento);
                                adnActivity.getProspectoBean().setFlagModificado(1);
                            }
                            TextInputLayout documentoTextInputLayout = (TextInputLayout)edtNumeroDocumento.getParent();
                            if(documentoTextInputLayout.getError() == null){
                                InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                inputManager.hideSoftInputFromWindow(edtCorreo.getWindowToken(), 0);
                                edtCorreo.clearFocus();
                                dialog.dismiss();
                                sincronizar();
                            }
                        } else {
                            textInputLayout.setError("Por favor ingresa una cuenta de correo válida (ej: hola@gmail.com)");
                        }
                    }
                });
                TxtCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(edtCorreo.getWindowToken(), 0);
                        edtCorreo.clearFocus();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        ListView lstReferidos = (ListView)view.findViewById((com.pacifico.adn.R.id.lstReferidos));
        lstReferidos.setDividerHeight(0);
        ReferidosListaFinalArrayAdapter adapter = new ReferidosListaFinalArrayAdapter(this);
        lstReferidos.setAdapter(adapter);
        lstReferidos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(edtActual != null){
                        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(edtActual.getWindowToken(), 0);
                        edtActual.clearFocus();
                        edtActual = null;
                    }
                }
                return false;
            }
        });
        RelativeLayout relLayReferidosListaFinal = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayReferidosListaFinal);
        relLayReferidosListaFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtActual != null){
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(edtActual.getWindowToken(), 0);
                    edtActual.clearFocus();
                    edtActual = null;
                }
            }
        });
        return view;
    }
    public void sincronizar(){
        if(!sincronizando){
            sincronizando = true;
            int flagTerminado = adnActivity.getAdnBean().getFlagTerminado();
            if(flagTerminado == 0){
                adnActivity.getAdnBean().setFlagTerminado(1);
                adnActivity.getAdnBean().setFlagModificado(1);
            }
            CitaBean citaBean = CitaReunionController.obteniendoUltimaCitaProspecto(adnActivity.getProspectoBean());
            if(citaBean != null && citaBean.getCodigoEtapaProspecto() == 2){ // solo funciona si es Primera entrevista
                citaBean.setCodigoEstado(3);
                citaBean.setCodigoResultado(2);
                citaBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                citaBean.setFlagEnviado(0);
                CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
                citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
                citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());
                citaMovimientoEstadoBean.setCodigoEstado(3);
                citaMovimientoEstadoBean.setCodigoResultado(2);
                citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(Util.obtenerFechaActual());
                citaMovimientoEstadoBean.setFlagEnviado(0);
                CitaReunionController.guardarCita(citaBean, false);
                CitaReunionController.guardarCitaMovimientoEstado(citaMovimientoEstadoBean);
            }
            adnActivity.guardarProspecto();
            adnActivity.guardarAdn();
            adnActivity.guardarReferidos();
            SincronizacionController sincronizacionController = new SincronizacionController();
            if(progressDialog == null){
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setTitle("Enviando información");
                progressDialog.setMessage("Espere, por favor");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
                @Override
                public void terminoSincronizacion(int codigo, String mensaje) {
                    progressDialog.dismiss();
                    sincronizando = false;
                    if (codigo < 0) {
                        mostrarMensajeParaContinuar(mensaje);
                    }else if(codigo == 1){
                        continuar();
                    }else{
                        if(codigo == 4 || codigo == 6){
                            Intent inicioSesionIntent = new Intent(getActivity(), InicioSesionActivity.class);
                            inicioSesionIntent.putExtra("etapa", (codigo - 4)/2 + 1);
                            startActivity(inicioSesionIntent);
                        }else {
                            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                        }
                    }
                }
            });
            int resultado = sincronizacionController.sincronizar(getContext());
            if(resultado == 0){
                sincronizando = false;
                progressDialog.dismiss();
                mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
            }else if(resultado == 2){
                sincronizando = false;
                progressDialog.dismiss();
                mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
            }
        }
    }
    private void mostrarMensajeParaContinuar(String mensaje){
        new AlertDialog.Builder(getContext())
                .setTitle("Alerta")
                .setCancelable(false)
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                continuar();
            }
        }).show();
    }
    private void continuar(){
        if(adnActivity.getProspectoBean().getCodigoEtapa() == 2){
            Intent intent = new Intent(ReferidosListaFinalFragment.this.getActivity(), BuscarProspectoActivity.class);
            startActivity(intent);
            try{
                Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.pacifico.agenda");
                if(launchIntent != null){
                    launchIntent.putExtra(Constantes.TipoOperacion, Constantes.OperacionADNTerminaryAgendar);
                    launchIntent.putExtra(parametro_idProspecto, adnActivity.getProspectoBean().getIdProspectoDispositivo());
                    startActivity(launchIntent);
                }
            }catch(Exception e){
                Util.mostrarAlertaConTitulo("Alerta", "No se pudo abrir agenda", ReferidosListaFinalFragment.this.getContext());
            }
        }else{
            Intent buscarProspectoIntent = new Intent(getActivity(), BuscarProspectoActivity.class);
            startActivity(buscarProspectoIntent);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                sincronizar();
            }
        }
    }

    public EditText getEdtActual() {
        return edtActual;
    }

    public void setEdtActual(EditText edtActual) {
        this.edtActual = edtActual;
    }
}