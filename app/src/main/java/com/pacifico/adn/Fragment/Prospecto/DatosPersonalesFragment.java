package com.pacifico.adn.Fragment.Prospecto;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.CampoDatosPersonalesArrayAdapter;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Model.Bean.TablaTablasBean;
import com.pacifico.adn.Model.Controller.TablasGeneralesController;
import com.pacifico.adn.Util.CodigoTelefonoSorter;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DatosPersonalesFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, View.OnFocusChangeListener {
    final private Calendar calMax = Calendar.getInstance();
    final private Calendar calMin = Calendar.getInstance();
    private ArrayList<TablaTablasBean> arrTipoDocumentos, arrCodigoTelefonoFijos, arrEstadosCiviles;
    private ArrayList<String> arrTitulosTipoDocumentos, arrTitulosCodigoTelefonoFijos, arrTitulosEstadosCiviles;
    private Calendar calFechaNacimiento;
    private EditText edtPrimerNombre, edtApellidoPaterno, edtApellidoMaterno, edtTipoDocumento, edtNumeroDocumento, edtFechaNacimiento, edtEdad, edtTelefonoCelular, edtCodigoTelefonoFijo, edtTelefonoFijo, edtAnexo, edtCorreoElectronico, edtEstadoCivil, edtEmpresa, edtActual;
    private RadioGroup rgSexo, rgFumador, rgHijos;
    private ProspectoBean prospectoBean;
    private String codigoTelefonoFijo;
    private int listaCampoActual;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ADNActivity adnActivity = (ADNActivity)getActivity();
        prospectoBean = adnActivity.getProspectoBean();
        arrTipoDocumentos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(5);
        arrTitulosTipoDocumentos = new ArrayList<>();
        int codigoTipoDocumento = prospectoBean.getCodigoTipoDocumento();
        if(codigoTipoDocumento == -1){
            prospectoBean.setCodigoTipoDocumento(1);
        }
        int indiceTipoDocumentoSeleccionado = 0;
        for(int i = 0; i < arrTipoDocumentos.size(); i++){
            TablaTablasBean tablaTablasBean = arrTipoDocumentos.get(i);
            arrTitulosTipoDocumentos.add(tablaTablasBean.getValorCadena());
            if(codigoTipoDocumento == tablaTablasBean.getCodigoCampo()){
                indiceTipoDocumentoSeleccionado = i;
            }
        }
        arrCodigoTelefonoFijos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(20);
        Collections.sort(arrCodigoTelefonoFijos, new CodigoTelefonoSorter());
        arrTitulosCodigoTelefonoFijos = new ArrayList<>();
        DecimalFormat decimalFormat = new DecimalFormat("##");
        for(int i = 0; i < arrCodigoTelefonoFijos.size(); i++){
            TablaTablasBean tablaTablasBean = arrCodigoTelefonoFijos.get(i);
            int codigoTelefonoFijo = (int)tablaTablasBean.getValorNumerico();
            String cadenaCodigoTelefonoFijo = decimalFormat.format(codigoTelefonoFijo);
            if(cadenaCodigoTelefonoFijo.length() == 1){
                cadenaCodigoTelefonoFijo = "  " + cadenaCodigoTelefonoFijo;
            }
            arrTitulosCodigoTelefonoFijos.add(String.format("%s      %s", cadenaCodigoTelefonoFijo, tablaTablasBean.getValorCadena()));
        }
        String telefonoFijoTemp = "";
        String anexoTemp = "";
        String telefonoFijo = prospectoBean.getTelefonoFijo();
        codigoTelefonoFijo = "1";
        if(telefonoFijo != null){
            if(telefonoFijo.length() == 15){
                codigoTelefonoFijo = telefonoFijo.substring(0, 2).replace(" ", "");
                telefonoFijoTemp = telefonoFijo.substring(2, 10).replace(" ", "");
                anexoTemp = telefonoFijo.substring(10, 15).replace(" ", "");
            }
        }

        arrEstadosCiviles = TablasGeneralesController.obtenerTablaTablasPorIdTabla(2);
        arrTitulosEstadosCiviles = new ArrayList<>();
        int codigoEstadoCivil = prospectoBean.getCodigoEstadoCivil();
        int indiceEstadoCivilSeleccionado = -1;
        for(int i = 0; i < arrEstadosCiviles.size(); i++){
            TablaTablasBean tablaTablasBean = arrEstadosCiviles.get(i);
            arrTitulosEstadosCiviles.add(tablaTablasBean.getValorCadena());
            if(codigoEstadoCivil == tablaTablasBean.getCodigoCampo()){
                indiceEstadoCivilSeleccionado = i;
            }
        }
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_datos_personales, container, false);
        edtPrimerNombre = (EditText)view.findViewById(com.pacifico.adn.R.id.edtPrimerNombre);
        edtApellidoPaterno = (EditText)view.findViewById(com.pacifico.adn.R.id.edtApellidoPaterno);
        edtApellidoMaterno = (EditText)view.findViewById(com.pacifico.adn.R.id.edtApellidoMaterno);
        edtTipoDocumento = (EditText)view.findViewById(com.pacifico.adn.R.id.edtTipoDocumento);
        edtNumeroDocumento = (EditText)view.findViewById(com.pacifico.adn.R.id.edtNumeroDocumento);
        edtFechaNacimiento = (EditText)view.findViewById(com.pacifico.adn.R.id.edtFechaNacimiento);
        edtEdad = (EditText)view.findViewById(com.pacifico.adn.R.id.edtEdad);
        edtTelefonoCelular = (EditText)view.findViewById(com.pacifico.adn.R.id.edtTelefonoCelular);
        edtCodigoTelefonoFijo = (EditText)view.findViewById(com.pacifico.adn.R.id.edtCodigoTelefonoFijo);
        edtTelefonoFijo = (EditText)view.findViewById(com.pacifico.adn.R.id.edtTelefonoFijo);
        edtAnexo = (EditText)view.findViewById(com.pacifico.adn.R.id.edtAnexo);
        edtCorreoElectronico = (EditText)view.findViewById(com.pacifico.adn.R.id.edtCorreoElectronico);
        edtEstadoCivil = (EditText)view.findViewById(com.pacifico.adn.R.id.edtEstadoCivil);
        rgSexo = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgSexo);
        rgFumador = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgFumador);
        rgHijos = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgHijos);
        edtEmpresa = (EditText)view.findViewById(com.pacifico.adn.R.id.edtEmpresa);

        edtPrimerNombre.setText(Util.capitalizedString(prospectoBean.getNombres()));
        edtPrimerNombre.setOnFocusChangeListener(this);
        edtPrimerNombre.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(100)});
        edtApellidoPaterno.setText(Util.capitalizedString(prospectoBean.getApellidoPaterno()));
        edtApellidoPaterno.setOnFocusChangeListener(this);
        edtApellidoPaterno.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(80)});
        String apellidoMaterno = prospectoBean.getApellidoMaterno();
        if(apellidoMaterno != null){
            edtApellidoMaterno.setText(Util.capitalizedString(apellidoMaterno));
        }
        edtApellidoMaterno.setOnFocusChangeListener(this);
        edtApellidoMaterno.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(80)});
        edtTipoDocumento.setText(arrTitulosTipoDocumentos.get(indiceTipoDocumentoSeleccionado));
        edtTipoDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaCampoActual = 0;
                cargarLista();
            }
        });
        edtNumeroDocumento.setText(prospectoBean.getNumeroDocumento());
        edtNumeroDocumento.setOnFocusChangeListener(this);
        String fechaNacimiento = prospectoBean.getFechaNacimiento();
        calMax.add(Calendar.YEAR, -18);
        calMin.add(Calendar.YEAR, -100);
        calMin.add(Calendar.DAY_OF_MONTH, -1);
        if(fechaNacimiento != null){
            if(fechaNacimiento.length() == 10){
                String[] arrFechaNacimiento = fechaNacimiento.split("-");
                calFechaNacimiento = Calendar.getInstance();
                calFechaNacimiento.set(Integer.parseInt(arrFechaNacimiento[0]), Integer.parseInt(arrFechaNacimiento[1]) - 1, Integer.parseInt(arrFechaNacimiento[2]));
                actualizarFechaNacimiento();
            }
        }
        if(calFechaNacimiento == null){
            calFechaNacimiento = (Calendar)calMax.clone();
            calFechaNacimiento.add(Calendar.YEAR, -12);
        }
        edtFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calFechaNacimiento.set(Calendar.YEAR, year);
                        calFechaNacimiento.set(Calendar.MONTH, monthOfYear);
                        calFechaNacimiento.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        actualizarFechaNacimiento();
                    }
                }, calFechaNacimiento.get(Calendar.YEAR), calFechaNacimiento.get(Calendar.MONTH), calFechaNacimiento.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.getDatePicker().setMinDate(calMin.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(calMax.getTimeInMillis());
                datePickerDialog.setTitle("Seleccione una fecha");
                datePickerDialog.show();
            }
        });
        edtTelefonoCelular.setText(prospectoBean.getTelefonoCelular());
        edtTelefonoCelular.setOnFocusChangeListener(this);
        edtCodigoTelefonoFijo.setText(codigoTelefonoFijo);
        edtCodigoTelefonoFijo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaCampoActual = 1;
                cargarLista();
            }
        });

        edtTelefonoFijo.setText(telefonoFijoTemp);
        edtTelefonoFijo.setOnFocusChangeListener(this);
        edtAnexo.setText(anexoTemp);
        edtAnexo.setOnFocusChangeListener(this);
        edtCorreoElectronico.setText(prospectoBean.getCorreoElectronico1());
        edtCorreoElectronico.setOnFocusChangeListener(this);

        if(indiceEstadoCivilSeleccionado > -1){
            edtEstadoCivil.setText(arrTitulosEstadosCiviles.get(indiceEstadoCivilSeleccionado));
        }
        edtEstadoCivil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaCampoActual = 2;
                cargarLista();
            }
        });
        if(prospectoBean.getCodigoSexo() != -1){
            int codigoSexo = prospectoBean.getCodigoSexo();
            if(codigoSexo == 0 || codigoSexo == 1){
                rgSexo.check(codigoSexo == 1 ? com.pacifico.adn.R.id.rbMasculino : com.pacifico.adn.R.id.rbFemenino);
            }
        }
        rgSexo.setOnCheckedChangeListener(this);
        if(prospectoBean.getCondicionFumador() != -1){
            rgFumador.check(prospectoBean.getCondicionFumador() == 1 ? com.pacifico.adn.R.id.rbFumador : com.pacifico.adn.R.id.rbNoFumador);
        }
        rgFumador.setOnCheckedChangeListener(this);
        if(prospectoBean.getFlagHijo() != -1){
            rgHijos.check(prospectoBean.getFlagHijo() == 1 ? com.pacifico.adn.R.id.rbConHijos : com.pacifico.adn.R.id.rbSinHijos);
        }
        rgHijos.setOnCheckedChangeListener(this);
        edtEmpresa.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(100)});
        edtEmpresa.setText(prospectoBean.getEmpresa());
        edtEmpresa.setOnFocusChangeListener(this);

        RelativeLayout relLayDatosPersonales = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayDatosPersonales);
        relLayDatosPersonales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtActual != null){
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(edtActual.getWindowToken(), 0);
                    edtActual.clearFocus();
                }
            }
        });
        return view;
    }
    private void actualizarFechaNacimiento(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM, yyyy");
        edtFechaNacimiento.setText(simpleDateFormat.format(calFechaNacimiento.getTime()));
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String nuevaFechaNacimiento = simpleDateFormat.format(calFechaNacimiento.getTime());
        Calendar calHoy = Calendar.getInstance();
        int diaHoy = calHoy.get(Calendar.DAY_OF_MONTH);
        int mesHoy = calHoy.get(Calendar.MONTH);
        int anioHoy = calHoy.get(Calendar.YEAR);
        int diaFecNac = calFechaNacimiento.get(Calendar.DAY_OF_MONTH);
        int mesFecNac = calFechaNacimiento.get(Calendar.MONTH);
        int anioFecNac = calFechaNacimiento.get(Calendar.YEAR);
        int edad = anioHoy - anioFecNac;
        if(mesHoy < mesFecNac){
            edad -= 1;
        }else if(mesHoy == mesFecNac){
            if(diaHoy < diaFecNac){
                edad -= 1;
            }
        }
        edtEdad.setText(String.format("%d años", edad));
        edtFechaNacimiento.setError(null);
        if(!nuevaFechaNacimiento.equals(prospectoBean.getFechaNacimiento())){
            Log.i("TAG", "fechaNacimiento");
            int codigoRangoEdad = 4;
            if(edad < 25){
                codigoRangoEdad = 1;
            }else if(edad < 31){
                codigoRangoEdad = 2;
            }else if(edad < 41){
                codigoRangoEdad = 3;
            }
            prospectoBean.setCodigoRangoEdad(codigoRangoEdad);
            prospectoBean.setFechaNacimiento(nuevaFechaNacimiento);
            prospectoBean.setFlagModificado(1);
        }
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group.equals(rgSexo)){
            prospectoBean.setCodigoSexo(checkedId == com.pacifico.adn.R.id.rbMasculino ? 1 : 0);
            prospectoBean.setFlagModificado(1);
            Log.i("TAG", "codigoSexo");
        }else if(group.equals(rgFumador)){
            prospectoBean.setCondicionFumador(checkedId == com.pacifico.adn.R.id.rbFumador ? 1 : 0);
            prospectoBean.setFlagModificado(1);
            Log.i("TAG", "condicionFumador");
        }else if(group.equals(rgHijos)){
            prospectoBean.setFlagHijo(checkedId == com.pacifico.adn.R.id.rbConHijos ? 1 : 0);
            prospectoBean.setFlagModificado(1);
            Log.i("TAG", "flagHijo");
            if(prospectoBean.getFlagHijo() == 1){
                ADNActivity adnActivity = (ADNActivity)getActivity();
                adnActivity.revisarNumeroHijos();
            }
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus){
        if(hasFocus){
            edtActual = (EditText)v;
        }else{
            edtActual = (EditText)v;
            String textoActual = edtActual.getText().toString().trim();
            if(edtActual.equals(edtPrimerNombre)){
                edtPrimerNombre.setText(textoActual);
                if(!textoActual.toUpperCase().equals(prospectoBean.getNombres())){
                    prospectoBean.setNombres(textoActual.toUpperCase());
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "nombres");
                }
                if(textoActual.length() == 0){
                    edtPrimerNombre.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtPrimerNombre.setError(null);
                }
            }else if(edtActual.equals(edtApellidoPaterno)){
                edtApellidoPaterno.setText(textoActual);
                if(!textoActual.toUpperCase().equals(prospectoBean.getApellidoPaterno())){
                    prospectoBean.setApellidoPaterno(textoActual.toUpperCase());
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "apePat");
                }
                if(textoActual.length() == 0){
                    edtApellidoPaterno.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtApellidoPaterno.setError(null);
                }
            }else if(edtActual.equals(edtApellidoMaterno)){
                edtApellidoMaterno.setText(textoActual);
                if(!textoActual.toUpperCase().equals(prospectoBean.getApellidoMaterno())){
                    prospectoBean.setApellidoMaterno(textoActual.toUpperCase());
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "apeMat");
                }
            }else if(edtActual.equals(edtNumeroDocumento)){
                if(!textoActual.equals(prospectoBean.getNumeroDocumento())){
                    prospectoBean.setNumeroDocumento(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "numDoc");
                }
                validarNumeroDocumento();
            }else if(edtActual.equals(edtTelefonoCelular)){
                if(!textoActual.equals(prospectoBean.getTelefonoCelular())){
                    prospectoBean.setTelefonoCelular(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "telefonoCelular");
                    validarTelefonos();
                }
            }else if(edtActual.equals(edtTelefonoFijo) || edtActual.equals(edtAnexo)){
                actualizarTelefonoFijo();
                validarTelefonos();
            }else if(edtActual.equals(edtCorreoElectronico)){
                edtCorreoElectronico.setText(textoActual);
                if(!textoActual.equals(prospectoBean.getCorreoElectronico1())){
                    prospectoBean.setCorreoElectronico1(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "correoElectronico");
                }
                validarCorreoElectronico();
            }else if(edtActual.equals(edtEmpresa)){
                edtEmpresa.setText(textoActual);
                if(!textoActual.equals(prospectoBean.getEmpresa())){
                    prospectoBean.setEmpresa(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "empresa");
                }
            }
            edtActual = null;
        }
    }
    private void actualizarTelefonoFijo(){
        String telefonoFijo = edtTelefonoFijo.getText().toString();
        if(telefonoFijo.length() == 0){
            if(prospectoBean.getTelefonoFijo() != null){
                prospectoBean.setTelefonoFijo(null);
                prospectoBean.setFlagModificado(1);
            }
        }else{
            String codigoTelefonoFijoTemp = codigoTelefonoFijo;
            if (codigoTelefonoFijoTemp.length() == 1) {
                codigoTelefonoFijoTemp = " " + codigoTelefonoFijoTemp;
            }
            for (int i = telefonoFijo.length(); i < 8; i++) {
                telefonoFijo = telefonoFijo + " ";
            }
            String anexo = edtAnexo.getText().toString();
            for (int i = anexo.length(); i < 5; i++) {
                anexo = " " + anexo;
            }
            String telefonoFijoFinal = codigoTelefonoFijoTemp + telefonoFijo + anexo;
            if (!telefonoFijoFinal.equals(prospectoBean.getTelefonoFijo())) {
                prospectoBean.setTelefonoFijo(telefonoFijoFinal);
                prospectoBean.setFlagModificado(1);
                Log.i("TAG", "telefonoFijo");
            }
        }
    }
    private void validarNumeroDocumento(){
        if (edtNumeroDocumento.getText().toString().length() > 0){
            if(prospectoBean.getCodigoTipoDocumento() == 1){
                if(edtNumeroDocumento.getText().toString().length() != 8){
                    edtNumeroDocumento.setError("Por favor ingresa un DNI válido (ej: 44312341)");
                }else{
                    edtNumeroDocumento.setError(null);
                }
            }else if(prospectoBean.getCodigoTipoDocumento() == 2){
                if(edtNumeroDocumento.getText().toString().length() != 11){
                    edtNumeroDocumento.setError("Por favor ingresa un RUC válido (ej: 10443123418)");
                }else{
                    edtNumeroDocumento.setError(null);
                }
            }else if(prospectoBean.getCodigoTipoDocumento() == 3) {
                if (edtNumeroDocumento.getText().toString().length() != 9) {
                    edtNumeroDocumento.setError("Por favor ingresa un CE válido (ej: 123456789)");
                } else {
                    edtNumeroDocumento.setError(null);
                }
            }
        }else{
            edtNumeroDocumento.setError(null);
        }
    }
    private void validarCorreoElectronico(){
        if(prospectoBean.getCorreoElectronico1() != null && prospectoBean.getCorreoElectronico1().length() == 0){
            edtCorreoElectronico.setError(Constantes.MensajeErrorCampoVacio);
        }else{
            final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(edtCorreoElectronico.getText().toString());
            if(!matcher.matches()){
                edtCorreoElectronico.setError("Por favor ingresa una cuenta de correo válida (ej: hola@gmail.com)");
            }else{
                edtCorreoElectronico.setError(null);
            }
        }
    }
    public void validarTelefonos(){
        String telefonoCelular = edtTelefonoCelular.getText().toString();
        String telefonoFijo = edtTelefonoFijo.getText().toString();
        if(telefonoCelular.length() > 0){
            if(telefonoCelular.length() != 9 || !telefonoCelular.substring(0, 1).equals("9")){
                edtTelefonoCelular.setError("Por favor ingresa un teléfono válido de celular (ej: 992665930)");
            }else{
                edtTelefonoCelular.setError(null);
            }
            if(telefonoFijo.length() > 0){
                int maximoTamano = codigoTelefonoFijo.equals("1") ? 7 : 6;
                if(telefonoFijo.length() != maximoTamano || telefonoFijo.substring(0, 1).equals("9")) {
                    edtTelefonoFijo.setError("Por favor ingresa un teléfono válido para el código de provincia seleccionado.");
                }else{
                    edtTelefonoFijo.setError(null);
                }
            }else{
                edtTelefonoFijo.setError(null);
            }
        }else{
            if(telefonoFijo.length() > 0){
                int maximoTamano = codigoTelefonoFijo.equals("1") ? 7 : 6;
                if(telefonoFijo.length() != maximoTamano || telefonoFijo.substring(0, 1).equals("9")) {
                    edtTelefonoFijo.setError("Por favor ingresa un teléfono válido para el código de provincia seleccionado.");
                }else{
                    edtTelefonoFijo.setError(null);
                    edtTelefonoCelular.setError(null);
                }
            }else{
                edtTelefonoCelular.setError(Constantes.MensajeErrorCampoVacio);
                edtTelefonoFijo.setError(null);
            }
        }
    }
    public void validarTodosCampos(){
        onFocusChange(edtPrimerNombre, false);
        onFocusChange(edtApellidoPaterno, false);
        validarNumeroDocumento();
        if(edtFechaNacimiento.length() == 0){
            edtFechaNacimiento.setError(Constantes.MensajeErrorCampoVacio);
        }
        validarTelefonos();
        validarCorreoElectronico();
    }
    public void cargarLista(){
        if(edtActual != null){
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(edtActual.getWindowToken(), 0);
            edtActual.clearFocus();
        }
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View dialogCampoDatosPersonales = layoutInflater.inflate(com.pacifico.adn.R.layout.dialog_campo_datos_personales, null);
        ListView lstCampoDatosPersonales = (ListView) dialogCampoDatosPersonales.findViewById(com.pacifico.adn.R.id.lstCampoDatosPersonales);
        Button btnCancelar = (Button)dialogCampoDatosPersonales.findViewById(com.pacifico.adn.R.id.btnCancelar);
        ArrayList<String> arrCampoDatosPersonales;
        if(listaCampoActual == 0){
            arrCampoDatosPersonales = arrTitulosTipoDocumentos;
        }else if(listaCampoActual == 1){
            arrCampoDatosPersonales = arrTitulosCodigoTelefonoFijos;
        }else{
            arrCampoDatosPersonales = arrTitulosEstadosCiviles;
        }
        CampoDatosPersonalesArrayAdapter arrayAdapter = new CampoDatosPersonalesArrayAdapter(getContext(), arrCampoDatosPersonales, listaCampoActual);
        lstCampoDatosPersonales.setAdapter(arrayAdapter);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(dialogCampoDatosPersonales);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        lstCampoDatosPersonales.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listaCampoActual == 0) {
                    edtTipoDocumento.setText(arrTitulosTipoDocumentos.get(position));
                    int nuevoCodigoTipoDocumento = arrTipoDocumentos.get(position).getCodigoCampo();
                    if (nuevoCodigoTipoDocumento != prospectoBean.getCodigoTipoDocumento()) {
                        prospectoBean.setCodigoTipoDocumento(nuevoCodigoTipoDocumento);
                        prospectoBean.setFlagModificado(1);
                        validarNumeroDocumento();
                    }
                } else if(listaCampoActual == 1) {
                    codigoTelefonoFijo = String.format("%.0f", arrCodigoTelefonoFijos.get(position).getValorNumerico());
                    edtCodigoTelefonoFijo.setText(codigoTelefonoFijo);
                    actualizarTelefonoFijo();
                    validarTelefonos();
                } else {
                    edtEstadoCivil.setText(arrTitulosEstadosCiviles.get(position));
                    int nuevoCodigoEstadoCivil = arrEstadosCiviles.get(position).getCodigoCampo();
                    int nuevoFlagConyuge = (position == 0 || position == 1) ? 1 : 0;
                    if(nuevoCodigoEstadoCivil != prospectoBean.getCodigoEstadoCivil()) {
                        prospectoBean.setCodigoEstadoCivil(nuevoCodigoEstadoCivil);
                        prospectoBean.setFlagConyuge(nuevoFlagConyuge);
                        prospectoBean.setFlagModificado(1);
                    }
                }
                dialog.dismiss();
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public EditText getEdtNumeroDocumento() {
        return edtNumeroDocumento;
    }
    public EditText getEdtTelefonoCelular() {
        return edtTelefonoCelular;
    }
    public EditText getEdtTelefonoFijo() {
        return edtTelefonoFijo;
    }
    public EditText getEdtCorreoElectronico() {
        return edtCorreoElectronico;
    }
    public EditText getEdtActual() {
        return edtActual;
    }
}