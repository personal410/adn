package com.pacifico.adn.Activity;

import android.content.Context;

import com.pacifico.adn.Model.Controller.AjustesController;
import com.pacifico.adn.Model.Controller.TablaIdentificadorController;
import com.pacifico.adn.Persistence.DatabaseConstants;
import com.pacifico.adn.Persistence.OrganizateDatabaseAdapter;
import com.dsbmobile.dsbframework.DSBApplication;

/**
 * Created by vctrls3477 on 19/07/16.
 */
public class ADNApplication extends DSBApplication{
    @Override
    public void onCreate(){
        super.onCreate();
        Context context_compartido;
        try{
            context_compartido = this.createPackageContext("com.pacifico.agenda", Context.CONTEXT_INCLUDE_CODE);
            if(context_compartido == null){
                return;
            }
        }catch(Exception e){
            String error = e.getMessage();
            System.out.println("DB error : " + error);
            return;
        }
        OrganizateDatabaseAdapter.open(context_compartido, /*DatabaseConstants.DATABASE_PATH + "/" + DatabaseConstants.DATABASE_DIRECTORY
                + "/" +*/ DatabaseConstants.DATABASE_NAME, DatabaseConstants.DATABASE_VERSION);
        TablaIdentificadorController.inicializarTabla();
        AjustesController.inicializarTabla();
    }
}