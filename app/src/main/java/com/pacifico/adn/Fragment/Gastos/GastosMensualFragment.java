package com.pacifico.adn.Fragment.Gastos;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.adn.Adapters.GastosMensualPagerAdapter;
import com.pacifico.adn.Fragment.FlujoADNFragment;
import com.pacifico.adn.Views.CustomView.StepperView;

public class GastosMensualFragment extends Fragment{
    private ViewPager pagGastosMensual;
    private FlujoADNFragment flujoADNFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_gastos_mensual, container, false);
        TabLayout tblTabs = (TabLayout)view.findViewById(com.pacifico.adn.R.id.tblTabs);
        pagGastosMensual = (ViewPager)view.findViewById(com.pacifico.adn.R.id.pagGastosMensual);
        pagGastosMensual.setOffscreenPageLimit(4);
        GastosMensualPagerAdapter adapter = new GastosMensualPagerAdapter(getActivity().getSupportFragmentManager());
        GastoViviendaFragment gastoViviendaFragment = new GastoViviendaFragment();
        gastoViviendaFragment.setGastosMensualFragment(this);
        adapter.addFrag(gastoViviendaFragment);
        GastoSaludEducacionFragment gastoSaludEducacionFragment = new GastoSaludEducacionFragment();
        gastoSaludEducacionFragment.setGastosMensualFragment(this);
        adapter.addFrag(gastoSaludEducacionFragment);
        GastoTransporteFragment gastoTransporteFragment = new GastoTransporteFragment();
        gastoTransporteFragment.setGastosMensualFragment(this);
        adapter.addFrag(gastoTransporteFragment);
        GastoOtrosFragment gastoOtrosFragment = new GastoOtrosFragment();
        gastoOtrosFragment.setGastosMensualFragment(this);
        adapter.addFrag(gastoOtrosFragment);
        pagGastosMensual.setAdapter(adapter);
        tblTabs.setupWithViewPager(pagGastosMensual);
        RelativeLayout rel1 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt1 = (TextView)rel1.findViewById(com.pacifico.adn.R.id.txt1);
        txt1.setText("VIVIENDA");
        txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron));
        ImageView imgView1 = (ImageView)rel1.findViewById(com.pacifico.adn.R.id.img1);
        imgView1.setVisibility(View.GONE);
        RelativeLayout rel2 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt2 = (TextView)rel2.findViewById(com.pacifico.adn.R.id.txt1);
        txt2.setText("SALUD Y EDUCACIÓN");
        ImageView imgView2 = (ImageView)rel2.findViewById(com.pacifico.adn.R.id.img1);
        imgView2.setVisibility(View.GONE);
        imgView2.setColorFilter(Color.rgb(170, 170, 170));
        RelativeLayout rel3 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt3 = (TextView)rel3.findViewById(com.pacifico.adn.R.id.txt1);
        txt3.setText("TRANSPORTE");
        ImageView imgView3 = (ImageView)rel3.findViewById(com.pacifico.adn.R.id.img1);
        imgView3.setColorFilter(Color.rgb(170, 170, 170));
        imgView3.setVisibility(View.GONE);
        RelativeLayout rel4 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt4 = (TextView)rel4.findViewById(com.pacifico.adn.R.id.txt1);
        txt4.setText("OTROS");
        ImageView imgView4 = (ImageView)rel4.findViewById(com.pacifico.adn.R.id.img1);
        imgView4.setColorFilter(Color.rgb(170, 170, 170));
        imgView4.setVisibility(View.GONE);
        tblTabs.getTabAt(0).setCustomView(rel1);
        tblTabs.getTabAt(1).setCustomView(rel2);
        tblTabs.getTabAt(2).setCustomView(rel3);
        tblTabs.getTabAt(3).setCustomView(rel4);
        pagGastosMensual.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tblTabs));
        tblTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                RelativeLayout relSelected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relSelected.findViewById(com.pacifico.adn.R.id.txt1);
                txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron));
                pagGastosMensual.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab){
                if(tab.getPosition() == 0){
                    GastoViviendaFragment gastoViviendaFragmentTemp = (GastoViviendaFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(0);
                    StepperView stepActual = gastoViviendaFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }else if(tab.getPosition() == 1){
                    GastoSaludEducacionFragment gastoSaludEducacionFragmentTemp = (GastoSaludEducacionFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(1);
                    StepperView stepActual = gastoSaludEducacionFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }else if(tab.getPosition() == 2){
                    GastoTransporteFragment gastoTransporteFragmentTemp = (GastoTransporteFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(2);
                    StepperView stepActual = gastoTransporteFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }else{
                    GastoOtrosFragment gastoOtrosFragmentTemp = (GastoOtrosFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(3);
                    StepperView stepActual = gastoOtrosFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }
                RelativeLayout relUnselected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relUnselected.findViewById(com.pacifico.adn.R.id.txt1);
                txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron70));
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
        });
        RelativeLayout relativeLayout = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayGastoMensual);
        relativeLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                flujoADNFragment.clearFocusStepTotalGastoFamiliares();
                if(pagGastosMensual.getCurrentItem() == 0){
                    GastoViviendaFragment gastoViviendaFragmentTemp = (GastoViviendaFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(0);
                    StepperView stepActual = gastoViviendaFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }else if(pagGastosMensual.getCurrentItem() == 1){
                    GastoSaludEducacionFragment gastoSaludEducacionFragmentTemp = (GastoSaludEducacionFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(1);
                    StepperView stepActual = gastoSaludEducacionFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }else if(pagGastosMensual.getCurrentItem() == 2){
                    GastoTransporteFragment gastoTransporteFragmentTemp = (GastoTransporteFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(2);
                    StepperView stepActual = gastoTransporteFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }else{
                    GastoOtrosFragment gastoOtrosFragmentTemp = (GastoOtrosFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(3);
                    StepperView stepActual = gastoOtrosFragmentTemp.getStepActual();
                    if(stepActual != null){
                        stepActual.clearFocus();
                    }
                }
            }
        });
        return view;
    }
    public void setFlujoADNFragment(FlujoADNFragment flujoADNFragment){
        this.flujoADNFragment = flujoADNFragment;
    }
    public FlujoADNFragment getFlujoADNFragment() {
        return flujoADNFragment;
    }
    public void clearStepActual(){
        if(pagGastosMensual.getCurrentItem() == 0){
            GastoViviendaFragment gastoViviendaFragmentTemp = (GastoViviendaFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(0);
            StepperView stepActual = gastoViviendaFragmentTemp.getStepActual();
            if(stepActual != null){
                stepActual.clearFocus();
            }
        }else if(pagGastosMensual.getCurrentItem() == 1){
            GastoSaludEducacionFragment gastoSaludEducacionFragmentTemp = (GastoSaludEducacionFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(1);
            StepperView stepActual = gastoSaludEducacionFragmentTemp.getStepActual();
            if(stepActual != null){
                stepActual.clearFocus();
            }
        }else if(pagGastosMensual.getCurrentItem() == 2){
            GastoTransporteFragment gastoTransporteFragmentTemp = (GastoTransporteFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(2);
            StepperView stepActual = gastoTransporteFragmentTemp.getStepActual();
            if(stepActual != null){
                stepActual.clearFocus();
            }
        }else{
            GastoOtrosFragment gastoOtrosFragmentTemp = (GastoOtrosFragment)((GastosMensualPagerAdapter)pagGastosMensual.getAdapter()).getItem(3);
            StepperView stepActual = gastoOtrosFragmentTemp.getStepActual();
            if(stepActual != null){
                stepActual.clearFocus();
            }
        }
    }
}