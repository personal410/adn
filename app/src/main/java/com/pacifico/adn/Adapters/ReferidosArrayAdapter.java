package com.pacifico.adn.Adapters;

import android.content.Context;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;

import com.pacifico.adn.Fragment.Referidos.ReferidosGenericoFragment;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.CustomEditText;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 3/07/16.
 */
public class ReferidosArrayAdapter extends ArrayAdapter<ReferidoBean>{
    private ReferidosGenericoFragment referidosGenericoFragment;
    private ArrayList<Integer> arrIndicesActivos;
    private int tipoReferido, cantidadReferidos;
    public ReferidosArrayAdapter(ReferidosGenericoFragment referidosGenericoFragment, int tipoReferido){
        super(referidosGenericoFragment.getContext(), -1);
        this.referidosGenericoFragment = referidosGenericoFragment;
        this.tipoReferido = tipoReferido;
    }
    @Override
    public int getCount(){
        ArrayList<ReferidoBean> arrReferidosFinal = new ArrayList<>();
        arrIndicesActivos = new ArrayList<>();
        for(ReferidoBean referido : referidosGenericoFragment.getArrReferidos()){
            if(referido.getFlagActivo() == 1){
                arrReferidosFinal.add(referido);
                arrIndicesActivos.add(referidosGenericoFragment.getArrReferidos().indexOf(referido));
            }
        }
        cantidadReferidos = arrReferidosFinal.size();
        return cantidadReferidos + 1;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        int ultimoIndice = cantidadReferidos;
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(position == ultimoIndice){
            View view = inflater.inflate(com.pacifico.adn.R.layout.row_agregar_referido, parent, false);
            Button btnAgregarReferido = (Button)view.findViewById(com.pacifico.adn.R.id.btnAgregarReferido);
            btnAgregarReferido.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(cantidadReferidos < 100){
                        ReferidoBean referidoBean = new ReferidoBean();
                        referidoBean.inicializarValores();
                        referidoBean.setCodigoTipoReferido(tipoReferido);
                        referidosGenericoFragment.getArrReferidos().add(referidoBean);
                        if(referidosGenericoFragment.getEdtActual() != null){
                            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(referidosGenericoFragment.getEdtActual().getWindowToken(), 0);
                            referidosGenericoFragment.getEdtActual().clearFocus();
                        }
                        notifyDataSetChanged();
                    }
                }
            });
            return view;
        }else{
            View view = inflater.inflate(com.pacifico.adn.R.layout.row_nuevo_referido, parent, false);
            final ReferidoBean referidoBean = referidosGenericoFragment.getArrReferidos().get(arrIndicesActivos.get(position));
            CustomEditText edtNombre = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtNombre);
            CustomEditText edtApellido = (CustomEditText)view.findViewById(com.pacifico.adn.R.id.edtApellido);
            ImageButton imgBtnEliminar = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnEliminar);
            edtNombre.setText(Util.capitalizedString(referidoBean.getNombres()));
            edtNombre.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(100)});
            edtNombre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        CustomEditText edtNombreTemp = (CustomEditText) v;
                        referidosGenericoFragment.setEdtActual(edtNombreTemp);
                        String nuevoNombres = edtNombreTemp.getText().toString().toUpperCase().trim();
                        if (!nuevoNombres.equals(referidoBean.getNombres())) {
                            referidoBean.setNombres(nuevoNombres);
                            referidoBean.setFlagEnviado(0);
                            referidoBean.setFlagModificado(1);
                        }
                    } else {
                        CustomEditText edtNombreTemp = (CustomEditText) v;
                        String nuevoNombres = edtNombreTemp.getText().toString().toUpperCase().trim();
                        if (!nuevoNombres.equals(referidoBean.getNombres())) {
                            referidoBean.setNombres(nuevoNombres);
                            referidoBean.setFlagEnviado(0);
                            referidoBean.setFlagModificado(1);
                        }
                        referidosGenericoFragment.setEdtActual(null);
                    }
                }
            });
            edtApellido.setText(Util.capitalizedString(referidoBean.getApellidoPaterno()));
            edtApellido.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(80)});
            edtApellido.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        CustomEditText edtApellidoTemp = (CustomEditText) v;
                        referidosGenericoFragment.setEdtActual(edtApellidoTemp);
                    }else{
                        CustomEditText edtApellidoTemp = (CustomEditText) v;
                        String nuevoApellido = edtApellidoTemp.getText().toString().toUpperCase().trim();
                        if (!nuevoApellido.equals(referidoBean.getApellidoPaterno())) {
                            referidoBean.setApellidoPaterno(nuevoApellido);
                            referidoBean.setFlagEnviado(0);
                            referidoBean.setFlagModificado(1);
                        }
                        referidosGenericoFragment.setEdtActual(null);
                    }
                }
            });
            imgBtnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    referidoBean.setFlagActivo(0);
                    referidoBean.setFlagEnviado(0);
                    referidoBean.setFlagModificado(1);
                    if(referidosGenericoFragment.getEdtActual() != null){
                        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(referidosGenericoFragment.getEdtActual().getWindowToken(), 0);
                        referidosGenericoFragment.getEdtActual().clearFocus();
                    }
                    notifyDataSetChanged();
                }
            });
            return view;
        }
    }
}