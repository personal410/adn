package com.pacifico.adn.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.pacifico.adn.Fragment.FormaPagoFragment;
import com.pacifico.adn.Model.Bean.TablaTablasBean;

/**
 * Created by vctrls3477 on 20/08/16.
 */
public class MedioPagoArrayAdapter extends ArrayAdapter<TablaTablasBean> {
    FormaPagoFragment formaPagoFragment;
    public MedioPagoArrayAdapter(FormaPagoFragment nFormaPagoFragment){
        super(nFormaPagoFragment.getContext(), -1);
        formaPagoFragment = nFormaPagoFragment;
    }
    @Override
    public int getCount(){
        return formaPagoFragment.getArrMedioPagos().size();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(com.pacifico.adn.R.layout.row_forma_pago, parent, false);
        TextView txtFormaPago = (TextView)rowView.findViewById(com.pacifico.adn.R.id.txtFormaPago);
        TablaTablasBean medioPagoTablaTablasBean = formaPagoFragment.getArrMedioPagos().get(position);
        txtFormaPago.setText(medioPagoTablaTablasBean.getValorCadena());
        if(medioPagoTablaTablasBean.getCodigoCampo() == formaPagoFragment.getMedioPago()){
            RadioButton rbFormaPago = (RadioButton)rowView.findViewById(com.pacifico.adn.R.id.rbFormaPago);
            rbFormaPago.setChecked(true);
        }
        return rowView;
    }
}