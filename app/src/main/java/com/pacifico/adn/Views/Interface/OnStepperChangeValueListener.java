package com.pacifico.adn.Views.Interface;

import com.pacifico.adn.Views.CustomView.StepperView;

/**
 * Created by vctrls3477 on 20/03/16.
 */
public interface OnStepperChangeValueListener{
    void onChangeValue(StepperView stepView);
}
