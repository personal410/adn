package com.pacifico.adn.Util;

/**
 * Created by joel on 6/20/16.
 */
public class Constantes{
    public static final int MonedaSoles = 0;
    public static final int MonedaDolares = 1;
    public static final int OperacionADNTerminaryAgendar = 1;
    public static final int OperacionRegularizarDatosADN = 2;
    public static final int OperacionMostrarADN = 3;
    public static final String TipoOperacion = "tipo_operacion";
    public static final String ParametroExtraIdProspectoDispositivo = "id_prospecto_dispositivo";
    public static final String SemillaEncriptacion = "pacifico2016";
    public static final String MensajeErrorCampoVacio = "Por favor registra los datos para este campo";
    public static final String URLBase = "https://desvconecta.pacificovida.net";
    public static final int ResultadoCita_CierreVenta = 1;
}