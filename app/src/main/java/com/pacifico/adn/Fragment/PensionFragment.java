package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;

public class PensionFragment extends Fragment implements CheckBox.OnCheckedChangeListener {
    private double ingresoBrutoMensual, porcentajeConyuge, porcentajeHijos;
    private ADNActivity actADN;
    private TextView txtIngresoBrutoMensual, txtPensionConyuge, txtPensionHijos;
    private CheckBox checkConyuge, checkHijos, checkNoAFP;
    private StepperView stepNumeroHijos;
    private boolean stepNumeroHijosHasFocus;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_pension, container, false);
        actADN = (ADNActivity)getActivity();
        txtIngresoBrutoMensual = (TextView)view.findViewById(com.pacifico.adn.R.id.txtIngresoBrutoMensual);
        txtPensionConyuge = (TextView)view.findViewById(com.pacifico.adn.R.id.txtPensionConyuge);
        TextView txtPorcentajeConyuge = (TextView)view.findViewById(com.pacifico.adn.R.id.txtPorcentajeConyuge);
        txtPensionHijos = (TextView)view.findViewById(com.pacifico.adn.R.id.txtPensionHijos);
        TextView txtPorcentajeHijos = (TextView)view.findViewById(com.pacifico.adn.R.id.txtPorcentajeHijos);
        checkConyuge = (CheckBox)view.findViewById(com.pacifico.adn.R.id.checkConyuge);
        checkHijos = (CheckBox)view.findViewById(com.pacifico.adn.R.id.checkHijos);
        checkNoAFP = (CheckBox)view.findViewById(com.pacifico.adn.R.id.checkNoAFP);
        stepNumeroHijos = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepNumeroHijos);
        porcentajeConyuge = actADN.getAdnBean().getPorcentajeAFPConyuge();
        porcentajeHijos = actADN.getAdnBean().getPorcentajeAFPHijos();
        txtPorcentajeConyuge.setText(String.format("%.0f%s", porcentajeConyuge, "%"));
        txtPorcentajeHijos.setText(String.format("%.0f%s", porcentajeHijos, "%"));
        stepNumeroHijos.setValorActual(actADN.getAdnBean().getNumeroHijos());
        stepNumeroHijos.setOnStepperChangeValueListener(new OnStepperChangeValueListener() {
            @Override
            public void onChangeValue(StepperView stepView) {
                int valorActual = stepView.getValorActual();
                if (valorActual != actADN.getAdnBean().getNumeroHijos()) {
                    actADN.getAdnBean().setNumeroHijos(valorActual);
                    actADN.getAdnBean().setFlagModificado(1);
                    actADN.verificarTipoCambio();
                    actualizarMontoTotal();
                }
            }
        });
        checkConyuge.setChecked(actADN.getAdnBean().getFlagPensionConyuge() == 1);
        checkHijos.setChecked(actADN.getAdnBean().getFlagPensionHijos() == 1);
        checkNoAFP.setChecked(actADN.getAdnBean().getFlagPensionNoAFP() == 1);
        if(checkNoAFP.isChecked()){
            checkHijos.setEnabled(false);
            checkConyuge.setEnabled(false);
            checkHijos.setButtonTintList(ContextCompat.getColorStateList(getActivity(), com.pacifico.adn.R.color.colorGris));
            checkConyuge.setButtonTintList(ContextCompat.getColorStateList(getActivity(), com.pacifico.adn.R.color.colorGris));
        }
        checkConyuge.setOnCheckedChangeListener(this);
        checkHijos.setOnCheckedChangeListener(this);
        checkNoAFP.setOnCheckedChangeListener(this);
        actualizarIngresoBrutoMensual();
        stepNumeroHijos.setOnFocusChangeListener(new StepperView.OnFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus, StepperView stepperView) {
                stepNumeroHijosHasFocus = hasFocus;
            }
        });
        RelativeLayout relLayPension = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayPension);
        relLayPension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if(stepNumeroHijosHasFocus){
                    stepNumeroHijos.clearFocus();
                }
            }
        });
        return view;
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
        if(buttonView == checkConyuge){
            actADN.getAdnBean().setFlagPensionConyuge(isChecked ? 1 : 0);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else if(buttonView == checkHijos){
            actADN.getAdnBean().setFlagPensionHijos(isChecked ? 1 : 0);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else{
            actADN.getAdnBean().setFlagPensionNoAFP(isChecked ? 1 : 0);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
            checkConyuge.setEnabled(!isChecked);
            checkHijos.setEnabled(!isChecked);
            if(checkNoAFP.isChecked()){
                checkHijos.setButtonTintList(ContextCompat.getColorStateList(getActivity(), com.pacifico.adn.R.color.colorGris));
                checkConyuge.setButtonTintList(ContextCompat.getColorStateList(getActivity(), com.pacifico.adn.R.color.colorGris));
            }else{
                checkHijos.setButtonTintList(ContextCompat.getColorStateList(getActivity(), com.pacifico.adn.R.color.colorverde));
                checkConyuge.setButtonTintList(ContextCompat.getColorStateList(getActivity(), com.pacifico.adn.R.color.colorverde));
            }
        }
        actualizarMontoTotal();
    }
    public void actualizarIngresoBrutoMensual(){
        double factorIngresoBrutoMensual = actADN.getAdnBean().getMonedaIngresoBrutoMensualVidaLey() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        ingresoBrutoMensual = actADN.getAdnBean().getIngresoBrutoMensualVidaLey() / factorIngresoBrutoMensual;
        txtIngresoBrutoMensual.setText(Util.obtenerMontoDolaresConFomato(ingresoBrutoMensual));
        actualizarMontoTotal();
    }
    private void actualizarMontoTotal(){
        double pensionConyuge = 0;
        double pensionHijos = 0;
        double montoTotal = 0;
        double ingresoBrutoMensualFinal = ingresoBrutoMensual;
        double topeVidaLey = actADN.getAdnBean().getTopeVidaLey() / actADN.getAdnBean().getTipoCambio();
        if(ingresoBrutoMensualFinal > topeVidaLey){
            ingresoBrutoMensualFinal = topeVidaLey;
        }
        if(checkConyuge.isChecked()){
            pensionConyuge = Util.redondearNumero(ingresoBrutoMensualFinal * porcentajeConyuge / 100.0, 0);
        }
        if(checkHijos.isChecked()){
            pensionHijos = Util.redondearNumero(ingresoBrutoMensualFinal * stepNumeroHijos.getValorActual() * porcentajeHijos / 100.0, 0);
        }
        if(!checkNoAFP.isChecked()){
            montoTotal = pensionConyuge + pensionHijos;
        }
        actADN.getAdnBean().setPensionConyuge(pensionConyuge);
        actADN.getAdnBean().setPensionHijos(pensionHijos);
        actADN.getAdnBean().setTotalPensionMensualAFP(montoTotal);
        txtPensionConyuge.setText(Util.obtenerMontoDolaresConFomato(pensionConyuge));
        txtPensionHijos.setText(Util.obtenerMontoDolaresConFomato(pensionHijos));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    public void clearStepActual(){
        if(stepNumeroHijosHasFocus){
            stepNumeroHijos.clearFocus();
        }
    }
}