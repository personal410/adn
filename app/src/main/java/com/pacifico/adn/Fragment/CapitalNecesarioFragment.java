package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;

public class CapitalNecesarioFragment extends Fragment{
    private StepperView stepNumeroAniosProteger;
    private TextView txtDeficitAnual, txtAniosProteger, txtCapitalNecesario, txtSeguroVida, txtActivosRealizables;
    private ADNActivity actADN;
    private boolean stepNumeroAniosProtegerHasFocus;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_capital_necesario, container, false);
        stepNumeroAniosProteger = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepNumeroAniosProteger);
        txtDeficitAnual = (TextView)view.findViewById(com.pacifico.adn.R.id.txtDeficitAnual);
        txtAniosProteger = (TextView)view.findViewById(com.pacifico.adn.R.id.txtAniosProteger);
        txtCapitalNecesario = (TextView)view.findViewById(com.pacifico.adn.R.id.txtCapitalNecesario);
        txtSeguroVida = (TextView)view.findViewById(com.pacifico.adn.R.id.txtSeguroVida);
        txtActivosRealizables = (TextView)view.findViewById(com.pacifico.adn.R.id.txtActivosRealizables);
        stepNumeroAniosProteger.setValorActual(actADN.getAdnBean().getAniosProteger());
        stepNumeroAniosProteger.fijarCantidadMaximaDigitos(2);
        stepNumeroAniosProteger.setOnStepperChangeValueListener(new OnStepperChangeValueListener() {
            @Override
            public void onChangeValue(StepperView stepView) {
                int valorActual = stepView.getValorActual();
                if (valorActual == -1) {
                    valorActual = 0;
                    stepNumeroAniosProteger.setValorActual(0);
                }
                if (valorActual != actADN.getAdnBean().getAniosProteger()) {
                    actADN.getAdnBean().setAniosProteger(valorActual);
                    actADN.getAdnBean().setFlagModificado(1);
                    actADN.verificarTipoCambio();
                    actualizarMontos();
                }
            }
        });
        stepNumeroAniosProteger.setOnFocusChangeListener(new StepperView.OnFocusChangeListener(){
            @Override
            public void onFocusChange(boolean hasFocus, StepperView stepperView){
                stepNumeroAniosProtegerHasFocus = hasFocus;
            }
        });
        actualizarMontos();
        RelativeLayout relLayCapitalNecesario = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayCapitalNecesario);
        relLayCapitalNecesario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if(stepNumeroAniosProtegerHasFocus){
                    stepNumeroAniosProteger.clearFocus();
                }
            }
        });

        return view;
    }
    public void actualizarMontos(){
        double ingresoNecesario = actADN.getAdnBean().getDeficitMensual();
        if(ingresoNecesario < 0){
            ingresoNecesario = 0;
        }
        double ingresoAnualNecesario = ingresoNecesario * 12;
        int aniosProteger = stepNumeroAniosProteger.getValorActual();
        double capitalNecesario = ingresoAnualNecesario * aniosProteger;
        double segurosVida = Util.redondearNumero(actADN.getAdnBean().getTotalSegurosVida(), 0);
        double totalActivosRealizables = Util.redondearNumero(actADN.getAdnBean().getTotalActivoRealizable(), 0);
        txtDeficitAnual.setText(Util.obtenerMontoDolaresConFomato(ingresoAnualNecesario));
        txtAniosProteger.setText(Integer.toString(aniosProteger));
        txtCapitalNecesario.setText(Util.obtenerMontoDolaresConFomato(capitalNecesario));
        txtSeguroVida.setText(Util.obtenerMontoDolaresConFomato(segurosVida));
        txtActivosRealizables.setText(Util.obtenerMontoDolaresConFomato(totalActivosRealizables));
        double montoTotal = capitalNecesario - totalActivosRealizables - segurosVida;
        if(montoTotal < 0){
            montoTotal = 0;
        }
        if(montoTotal != actADN.getAdnBean().getCapitalNecesarioFallecimiento()){
            actADN.getAdnBean().setCapitalNecesarioFallecimiento(montoTotal);
            actADN.getAdnBean().setFlagModificado(1);
        }
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    public void clearStepActual(){
        if(stepNumeroAniosProtegerHasFocus){
            stepNumeroAniosProteger.clearFocus();
        }
    }
}