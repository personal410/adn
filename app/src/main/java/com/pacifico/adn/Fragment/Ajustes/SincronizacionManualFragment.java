package com.pacifico.adn.Fragment.Ajustes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Activity.InicioSesionActivity;
import com.pacifico.adn.Network.RespuestaSincronizacionListener;
import com.pacifico.adn.Network.SincronizacionController;
import com.pacifico.adn.R;

public class SincronizacionManualFragment extends Fragment{
    private ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_sincronizacion_manual, container, false);
        Button button = (Button)view.findViewById(R.id.btnSincronizar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sincronizar();
            }
        });
        return view;
    }
    public void sincronizar(){
        ADNActivity adnActivity = (ADNActivity)getActivity();
        adnActivity.guardarProspecto();
        adnActivity.guardarAdn();
        adnActivity.guardarReferidos();
        SincronizacionController sincronizacionController = new SincronizacionController();
        if(progressDialog == null){
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
        @Override
        public void terminoSincronizacion(int codigo, String mensaje) {
            progressDialog.dismiss();
            if (codigo < 0) {
                mostrarMensajeParaContinuar(mensaje);
            }else if(codigo == 1){
               mostrarMensajeParaContinuar("Se sincronizó la información correctamente");

            }else{
                if(codigo == 4 || codigo == 6){
                    Intent inicioSesionIntent = new Intent(getActivity(), InicioSesionActivity.class);
                    inicioSesionIntent.putExtra("etapa", (codigo - 4)/2 + 1);
                    startActivity(inicioSesionIntent);
                }else {
                    mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                }
            }
        }
    });
    int resultado = sincronizacionController.sincronizar(getContext());
    if(resultado == 0){
        progressDialog.dismiss();
        mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
    }else if(resultado == 2){
        progressDialog.dismiss();
        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }
    private void mostrarMensajeParaContinuar(String mensaje){
        new AlertDialog.Builder(getContext())
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                sincronizar();
            }
        }
    }
}