package com.pacifico.adn.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pacifico.adn.Model.Controller.CitaReunionController;
import com.pacifico.adn.Model.Controller.IntermediarioController;
import com.pacifico.adn.Util.Constantes;

import java.util.Timer;
import java.util.TimerTask;

public class LauncherActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(com.pacifico.adn.R.layout.activity_launcher);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run(){
                int tipoOperacion = -1;
                Intent intent = getIntent();
                try{
                    tipoOperacion = intent.getExtras().getInt(Constantes.TipoOperacion);
                }catch(Exception e){}
                if(tipoOperacion == Constantes.OperacionRegularizarDatosADN || tipoOperacion == Constantes.OperacionMostrarADN){
                    int idProspectoDispositivo = intent.getExtras().getInt(Constantes.ParametroExtraIdProspectoDispositivo, -1);
                    Intent buscarProspectoIntent = new Intent(LauncherActivity.this, BuscarProspectoActivity.class);
                    if(idProspectoDispositivo != -1){
                        buscarProspectoIntent.putExtra(Constantes.TipoOperacion, tipoOperacion);
                        buscarProspectoIntent.putExtra(Constantes.ParametroExtraIdProspectoDispositivo, idProspectoDispositivo);
                    }
                    startActivity(buscarProspectoIntent);
                }else{
                    if(IntermediarioController.obtenerIntermediario() == null){
                        startActivity(new Intent(LauncherActivity.this, InicioSesionActivity.class));
                    }else{
                        if(CitaReunionController.hayAlgunaCita()){
                            startActivity(new Intent(LauncherActivity.this, BuscarProspectoActivity.class));
                        }else{
                            startActivity(new Intent(LauncherActivity.this, OnboardingActivity.class));
                        }
                    }
                }
            }
        }, 1000);
    }
}