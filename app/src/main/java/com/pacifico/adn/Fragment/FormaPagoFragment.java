package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.EntidadArrayAdapter;
import com.pacifico.adn.Adapters.FrecuenciaPagoArrayAdapter;
import com.pacifico.adn.Adapters.MedioPagoArrayAdapter;
import com.pacifico.adn.Model.Bean.EntidadBean;
import com.pacifico.adn.Model.Bean.TablaTablasBean;
import com.pacifico.adn.Model.Controller.EntidadController;
import com.pacifico.adn.Model.Controller.TablasGeneralesController;

import java.util.ArrayList;

public class FormaPagoFragment extends Fragment{
    private ADNActivity adnActivity;
    private ArrayList<EntidadBean> arrEntidades = new ArrayList<>();
    private ArrayList<TablaTablasBean> arrMedioPagos = new ArrayList<>();
    private ArrayList<TablaTablasBean> arrFrecuenciaPagos = new ArrayList<>();
    private int medioPago, entidad, codigoFrecuenciaPago;
    private EntidadArrayAdapter entidadArrayAdapter;
    private MedioPagoArrayAdapter medioPagoArrayAdapter;
    private FrecuenciaPagoArrayAdapter frecuenciaPagoArrayAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        adnActivity = (ADNActivity)getActivity();
        arrMedioPagos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(16);
        arrFrecuenciaPagos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(15);
        entidad = adnActivity.getAdnBean().getIdEntidad();
        medioPago = 0;
        codigoFrecuenciaPago = adnActivity.getAdnBean().getCodigoFrecuenciaPago();

        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_forma_pago, container, false);
        ListView lstMedioPago = (ListView)view.findViewById(com.pacifico.adn.R.id.lstMedioPago);
        ListView lstEntidades = (ListView)view.findViewById(com.pacifico.adn.R.id.lstEntidades);
        ListView lstFrecuenciaPagos = (ListView)view.findViewById(com.pacifico.adn.R.id.lstFrecuenciaPago);
        ImageButton imgBtnAtras = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnAtras);
        ImageButton imgBtnSiguiente = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnSiguiente);

        lstMedioPago.setDividerHeight(0);
        medioPagoArrayAdapter = new MedioPagoArrayAdapter(this);
        lstMedioPago.setAdapter(medioPagoArrayAdapter);
        lstMedioPago.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                medioPago = arrMedioPagos.get(position).getCodigoCampo();
                cargarListaEntidadesPorCodigoTipoCobranza();
                entidad = -1;
                if (entidad != adnActivity.getAdnBean().getIdEntidad()) {
                    adnActivity.getAdnBean().setIdEntidad(entidad);
                    adnActivity.getAdnBean().setFlagModificado(1);
                }
                medioPagoArrayAdapter.notifyDataSetChanged();
            }
        });

        lstEntidades.setDividerHeight(0);
        entidadArrayAdapter = new EntidadArrayAdapter(this);
        lstEntidades.setAdapter(entidadArrayAdapter);
        lstEntidades.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                entidad = arrEntidades.get(position).getIdEntidad();
                if (entidad != adnActivity.getAdnBean().getIdEntidad()) {
                    adnActivity.getAdnBean().setIdEntidad(entidad);
                    adnActivity.getAdnBean().setFlagModificado(1);
                }
                entidadArrayAdapter.notifyDataSetChanged();
            }
        });

        lstFrecuenciaPagos.setDividerHeight(0);
        frecuenciaPagoArrayAdapter = new FrecuenciaPagoArrayAdapter(this);
        lstFrecuenciaPagos.setAdapter(frecuenciaPagoArrayAdapter);
        lstFrecuenciaPagos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                codigoFrecuenciaPago = arrFrecuenciaPagos.get(position).getCodigoCampo();
                if(codigoFrecuenciaPago != adnActivity.getAdnBean().getCodigoFrecuenciaPago()){
                    adnActivity.getAdnBean().setCodigoFrecuenciaPago(codigoFrecuenciaPago);
                    adnActivity.getAdnBean().setFlagModificado(1);
                }
                frecuenciaPagoArrayAdapter.notifyDataSetChanged();
            }
        });

        if(entidad != -1){
            EntidadBean entidadBean = EntidadController.obtenerEntidadPorIdEntidad(entidad);
            medioPago = entidadBean.getCodigoTipoCobranza();
            cargarListaEntidadesPorCodigoTipoCobranza();
        }

        imgBtnAtras.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                adnActivity.guardarAdn();
                adnActivity.mostrarFlujoADN(7);
            }
        });
        imgBtnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adnActivity.guardarAdn();
                if(entidad == -1){
                    Snackbar.make(getView(), "Seleccione un tarjeta o banco.", Snackbar.LENGTH_LONG).show();
                }else{
                    if(codigoFrecuenciaPago == -1){
                        Snackbar.make(getView(), "Seleccione una frecuencia de pago.", Snackbar.LENGTH_LONG).show();
                    }else{
                        adnActivity.mostrarReferidos();
                    }
                }
            }
        });
        return view;
    }
    private void cargarListaEntidadesPorCodigoTipoCobranza(){
        arrEntidades = EntidadController.obtenerEntidadesPorCodigoTipoCobranza(medioPago);
        entidadArrayAdapter.notifyDataSetChanged();
    }
    public ArrayList<EntidadBean> getArrEntidades() {
        return arrEntidades;
    }
    public int getEntidad() {
        return entidad;
    }
    public int getMedioPago() {
        return medioPago;
    }
    public ArrayList<TablaTablasBean> getArrMedioPagos() {
        return arrMedioPagos;
    }
    public ArrayList<TablaTablasBean> getArrFrecuenciaPagos() {
        return arrFrecuenciaPagos;
    }
    public int getCodigoFrecuenciaPago(){
        return codigoFrecuenciaPago;
    }
}