package com.pacifico.adn.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pacifico.adn.R;
/**
 * Created by vctrls3477 on 7/08/16.
 */
public class DatoReferidoArrayAdapter extends ArrayAdapter<String>{
    String[] arrDatos;
    public DatoReferidoArrayAdapter(Context context, String[] nArrDatos){
        super(context, -1);
        arrDatos = nArrDatos;
    }
    @Override
    public int getCount(){
        return arrDatos.length;
    }
    @Override
    public String getItem(int position) {
        return arrDatos[position];
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView view = (TextView)inflater.inflate(R.layout.spinner_item, parent, false);
        view.setText(arrDatos[position]);
        return view;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView view = (TextView)inflater.inflate(R.layout.spinner_dropdown_item, parent, false);
        if(position == 0){
            view.setTextColor(getContext().getResources().getColor(R.color.colorLightGray));
        }
        view.setText(arrDatos[position]);
        return view;
    }
}