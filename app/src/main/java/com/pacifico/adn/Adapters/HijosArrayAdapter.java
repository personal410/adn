package com.pacifico.adn.Adapters;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Fragment.Prospecto.HijosFragment;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.R;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.CustomEditText;

/**
 * Created by victorsalazar on 18/04/16.
 */
public class HijosArrayAdapter extends ArrayAdapter<FamiliarBean>{
    private HijosFragment hijosFragment;
    private ADNActivity adnActivity;
    public HijosArrayAdapter(HijosFragment hijosFragment){
        super(hijosFragment.getContext(), -1);
        this.hijosFragment = hijosFragment;
        adnActivity = (ADNActivity)hijosFragment.getActivity();
    }
    @Override
    public int getCount(){
        return hijosFragment.getArrHijos().size() + 1;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        int last = this.getCount() - 1;
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(position == last){
            View rowView = inflater.inflate(R.layout.row_agregar_hijo, parent, false);
            Button btnAgregarHijo = (Button)rowView.findViewById(R.id.btnAgregarHijo);
            btnAgregarHijo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(hijosFragment.getArrHijos().size() < 20){
                        FamiliarBean hijoBean = new FamiliarBean();
                        hijoBean.inicializarValores();
                        hijosFragment.getArrHijos().add(hijoBean);
                        if(hijosFragment.getEdtActual() != null){
                            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(hijosFragment.getEdtActual().getWindowToken(), 0);
                            hijosFragment.getEdtActual().clearFocus();
                        }
                        notifyDataSetChanged();
                    }
                }
            });
            return rowView;
        }else{
            View rowView = inflater.inflate(R.layout.row_nuevo_hijo, parent, false);
            final FamiliarBean hijo = hijosFragment.getArrHijos().get(position);
            final int edad = hijo.getEdad();
            final CustomEditText edtNombre = (CustomEditText)rowView.findViewById(R.id.edtNombre);
            final CustomEditText edtEdad = (CustomEditText)rowView.findViewById(R.id.edtEdad);
            ImageButton imgBtnEliminar = (ImageButton)rowView.findViewById(R.id.imgBtnEliminar);
            imgBtnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hijosFragment.getArrHijos().remove(hijo);
                    if(hijosFragment.getEdtActual() != null) {
                        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(hijosFragment.getEdtActual().getWindowToken(), 0);
                        hijosFragment.getEdtActual().clearFocus();
                    }
                    notifyDataSetChanged();
                }
            });
            edtNombre.setFilters(new InputFilter[]{Util.obtenerFiltroSoloLetras(), new InputFilter.LengthFilter(100)});
            edtNombre.setText(Util.capitalizedString(hijo.getNombres()));
            edtNombre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        hijosFragment.setEdtActual(edtNombre);
                    }else{
                        String nuevoNombre = edtNombre.getText().toString().toUpperCase().trim();
                        if(!nuevoNombre.equals(hijo.getNombres())){
                            hijo.setNombres(edtNombre.getText().toString());
                            hijo.setFlagModificado(1);
                        }
                        if(adnActivity.getProspectoBean().getFlagHijo() == 1){
                            if(nuevoNombre.length() == 0){
                                edtNombre.setError(Constantes.MensajeErrorCampoVacio);
                            }else{
                                edtNombre.setError(null);
                            }
                        }else{
                            edtNombre.setError(null);
                        }
                        hijosFragment.setEdtActual(null);
                    }
                }
            });
            edtEdad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        if(hijo.getEdad() > -1){
                            edtEdad.setText(Integer.toString(hijo.getEdad()));
                            edtEdad.selectAll();
                        }
                        edtEdad.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});
                        hijosFragment.setEdtActual(edtEdad);
                    }else{
                        edtEdad.setFilters(new InputFilter[]{new InputFilter.LengthFilter(7)});
                        int nuevaEdad = -1;
                        if(edtEdad.length() > 0){
                            nuevaEdad = Integer.parseInt(edtEdad.getText().toString());
                            if(nuevaEdad > 100){
                                nuevaEdad = 100;
                            }
                            edtEdad.setText(String.format("%d año%s", nuevaEdad, (nuevaEdad == 1) ? "" : "s"));
                        }
                        if(nuevaEdad != hijo.getEdad()){
                            hijo.setEdad(nuevaEdad);
                            hijo.setFlagModificado(1);
                        }
                        TextInputLayout textInputLayout = (TextInputLayout)edtEdad.getParent();
                        if(adnActivity.getProspectoBean().getFlagHijo() == 1){
                            if(nuevaEdad == -1){
                                edtEdad.setError(Constantes.MensajeErrorCampoVacio);
                            }else{
                                edtEdad.setError(null);
                            }
                        }else{
                            edtEdad.setError(null);
                        }
                        hijosFragment.setEdtActual(null);
                    }
                }
            });
            if(edad == -1){
                edtEdad.setText("");
            }else{
                edtEdad.setText(String.format("%d año%s", edad, (edad == 1) ? "" : "s"));
            }
            if(hijosFragment.getMostraError()){
                if(adnActivity.getProspectoBean().getFlagHijo() == 1){
                    if(hijo.getNombres().length() == 0){
                        edtNombre.setError(Constantes.MensajeErrorCampoVacio);
                    }
                    if(hijo.getEdad() == -1){
                        edtEdad.setError(Constantes.MensajeErrorCampoVacio);
                    }
                }
            }
            return rowView;
        }
    }
}