package com.pacifico.adn.Persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.pacifico.adn.Model.Bean.AjustesBean;
import com.pacifico.adn.Model.Bean.EntidadBean;
import com.pacifico.adn.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.adn.Model.Bean.ReminderBean;
import com.pacifico.adn.Model.Bean.TablaIdentificadorBean;
import com.pacifico.adn.Model.Bean.TablaIndiceBean;
import com.pacifico.adn.Model.Bean.TablaTablasBean;
import com.dsbmobile.dsbframework.controller.persistence.DatabaseHelper;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.adn.Model.Bean.AdnBean;
import com.pacifico.adn.Model.Bean.CalendarioBean;
import com.pacifico.adn.Model.Bean.CitaBean;
import com.pacifico.adn.Model.Bean.CitaHistoricaBean;
import com.pacifico.adn.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.adn.Model.Bean.DispositivoBean;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.Model.Bean.IntermediarioBean;
import com.pacifico.adn.Model.Bean.MensajeSistemaBean;
import com.pacifico.adn.Model.Bean.ParametroBean;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Model.Bean.ReunionInternaBean;

import java.util.ArrayList;

public class OrganizateDatabaseHelper extends DatabaseHelper{
	private ArrayList<TableHelper> tableHelpers;
	public OrganizateDatabaseHelper(Context context, String databaseName, int databaseVersion) {
		super(context, databaseName, databaseVersion);
		tableHelpers = new ArrayList<>();
		tableHelpers.add(AdnBean.tableHelper);
		tableHelpers.add(CalendarioBean.tableHelper);
		tableHelpers.add(CitaBean.tableHelper);
		tableHelpers.add(CitaHistoricaBean.tableHelper);
		tableHelpers.add(CitaMovimientoEstadoBean.tableHelper);
		tableHelpers.add(ProspectoBean.tableHelper);
		tableHelpers.add(ProspectoMovimientoEtapaBean.tableHelper);
		tableHelpers.add(DispositivoBean.tableHelper);
		tableHelpers.add(FamiliarBean.tableHelper);
		tableHelpers.add(IntermediarioBean.tableHelper);
		tableHelpers.add(MensajeSistemaBean.tableHelper);
		tableHelpers.add(ParametroBean.tableHelper);
		tableHelpers.add(ReferidoBean.tableHelper);
		tableHelpers.add(ReunionInternaBean.tableHelper);
		tableHelpers.add(TablaIndiceBean.tableHelper);
		tableHelpers.add(TablaTablasBean.tableHelper);
		tableHelpers.add(EntidadBean.tableHelper);
		tableHelpers.add(TablaIdentificadorBean.tableHelper);
		tableHelpers.add(RecordatorioLlamadaBean.tableHelper);
		tableHelpers.add(ReminderBean.tableHelper);
		tableHelpers.add(AjustesBean.tableHelper);
	}
	@Override
	public void executeCreates(SQLiteDatabase db){
		db.beginTransaction();
		for (TableHelper tableHelper : tableHelpers) {
			db.execSQL(tableHelper.getCreateSentence());
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	@Override
	public void executeDrops(SQLiteDatabase db){
		db.beginTransaction();
		for (TableHelper tableHelper : tableHelpers) {
			db.execSQL(tableHelper.getDropSentence());
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
}