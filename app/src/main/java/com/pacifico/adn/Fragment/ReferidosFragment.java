package com.pacifico.adn.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.GastosMensualPagerAdapter;
import com.pacifico.adn.Fragment.Referidos.ReferidosAmigosFragment;
import com.pacifico.adn.Fragment.Referidos.ReferidosFamiliaFragment;
import com.pacifico.adn.Fragment.Referidos.ReferidosOtrosFragment;
import com.pacifico.adn.Fragment.Referidos.ReferidosTrabajoFragment;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Model.Controller.ReferidoController;
import com.pacifico.adn.Util.Fuente;
import com.pacifico.adn.Util.Util;

import java.util.ArrayList;

public class ReferidosFragment extends Fragment{
    private ADNActivity adnActivity;
    private ViewPager pagReferidos;
    private ReferidosFamiliaFragment referidosFamiliaFragment = new ReferidosFamiliaFragment();
    private ReferidosAmigosFragment referidosAmigosFragment = new ReferidosAmigosFragment();
    private ReferidosTrabajoFragment referidosTrabajoFragment = new ReferidosTrabajoFragment();
    private ReferidosOtrosFragment referidosOtrosFragment = new ReferidosOtrosFragment();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        adnActivity = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_referidos, container, false);
        TabLayout tblTabs = (TabLayout)view.findViewById(com.pacifico.adn.R.id.tblTabs);
        pagReferidos = (ViewPager)view.findViewById(com.pacifico.adn.R.id.pagReferidos);
        pagReferidos.setOffscreenPageLimit(4);
        GastosMensualPagerAdapter adapter = new GastosMensualPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(referidosFamiliaFragment);
        adapter.addFrag(referidosAmigosFragment);
        adapter.addFrag(referidosTrabajoFragment);
        adapter.addFrag(referidosOtrosFragment);
        pagReferidos.setAdapter(adapter);
        tblTabs.setupWithViewPager(pagReferidos);
        RelativeLayout rel1 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt1 = (TextView)rel1.findViewById(com.pacifico.adn.R.id.txt1);
        txt1.setText("FAMILIA");
        Fuente.setFuenteRg(getActivity(), txt1);
        txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron70));
        ImageView imgView1 = (ImageView)rel1.findViewById(com.pacifico.adn.R.id.img1);
        imgView1.setImageResource(com.pacifico.adn.R.drawable.referido_familia);
        RelativeLayout rel2 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt2 = (TextView)rel2.findViewById(com.pacifico.adn.R.id.txt1);
        txt2.setText("AMIGOS");
        Fuente.setFuenteRg(getActivity(),txt2);
        txt2.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron70));
        ImageView imgView2 = (ImageView)rel2.findViewById(com.pacifico.adn.R.id.img1);
        imgView2.setImageResource(com.pacifico.adn.R.drawable.referido_amigo);
        imgView2.setColorFilter(Color.rgb(170, 170, 170));

        RelativeLayout rel3 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt3 = (TextView)rel3.findViewById(com.pacifico.adn.R.id.txt1);
        txt3.setText("TRABAJO");
        Fuente.setFuenteRg(getActivity(),txt3);
        txt3.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron70));
        ImageView imgView3 = (ImageView)rel3.findViewById(com.pacifico.adn.R.id.img1);
        imgView3.setColorFilter(Color.rgb(170, 170, 170));
        imgView3.setImageResource(com.pacifico.adn.R.drawable.referido_trabajo);
        RelativeLayout rel4 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(com.pacifico.adn.R.layout.custom_tab, null);
        TextView txt4 = (TextView)rel4.findViewById(com.pacifico.adn.R.id.txt1);
        txt4.setText("OTROS");
        Fuente.setFuenteRg(getActivity(),txt4);
        txt4.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron70));
        ImageView imgView4 = (ImageView)rel4.findViewById(com.pacifico.adn.R.id.img1);
        imgView4.setColorFilter(Color.rgb(170, 170, 170));
        imgView4.setImageResource(com.pacifico.adn.R.drawable.referido_otros);
        tblTabs.getTabAt(0).setCustomView(rel1);
        tblTabs.getTabAt(1).setCustomView(rel2);
        tblTabs.getTabAt(2).setCustomView(rel3);
        tblTabs.getTabAt(3).setCustomView(rel4);
        pagReferidos.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tblTabs));
        tblTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                RelativeLayout relSelected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relSelected.findViewById(com.pacifico.adn.R.id.txt1);
                txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron));
                ImageView imgViews = (ImageView) relSelected.findViewById(com.pacifico.adn.R.id.img1);
                imgViews.setColorFilter(getResources().getColor(com.pacifico.adn.R.color.colorMarron));
                pagReferidos.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab){
                if(tab.getPosition() == 0){
                    if (referidosFamiliaFragment.getEdtActual() != null){
                        ocultarTecladoConEditText(referidosFamiliaFragment.getEdtActual());
                    }
                }else if(tab.getPosition() == 1){
                    if (referidosAmigosFragment.getEdtActual() != null){
                        ocultarTecladoConEditText(referidosAmigosFragment.getEdtActual());
                    }
                }else if(tab.getPosition() == 2){
                    if (referidosTrabajoFragment.getEdtActual() != null){
                        ocultarTecladoConEditText(referidosTrabajoFragment.getEdtActual());
                    }
                }else if(tab.getPosition() == 3){
                    if (referidosOtrosFragment.getEdtActual() != null){
                        ocultarTecladoConEditText(referidosOtrosFragment.getEdtActual());
                    }
                }
                RelativeLayout relUnselected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relUnselected.findViewById(com.pacifico.adn.R.id.txt1);
                txt1.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorMarron70));
                ImageView imgViews = (ImageView) relUnselected.findViewById(com.pacifico.adn.R.id.img1);
                imgViews.setColorFilter(Color.rgb(170, 170, 170));
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        ImageButton imgBtnAtras = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnAtras);
        imgBtnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adnActivity.mostrarMetodoPago();
            }
        });
        ImageButton imgBtnSiguiente = (ImageButton)view.findViewById(com.pacifico.adn.R.id.imgBtnSiguiente);
        imgBtnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ReferidoBean> arrReferidos = new ArrayList<>();
                arrReferidos.addAll(adnActivity.getArrReferidosFamiliar());
                arrReferidos.addAll(adnActivity.getArrReferidosAmigos());
                arrReferidos.addAll(adnActivity.getArrReferidosTrabajo());
                arrReferidos.addAll(adnActivity.getArrReferidosOtros());
                boolean hayReferidoVacio = false;
                for(ReferidoBean referidoBean :  arrReferidos){
                    if(((referidoBean.getNombres().length() == 0 && referidoBean.getApellidoPaterno().length() > 0) || (referidoBean.getNombres().length() > 0 && referidoBean.getApellidoPaterno().length() == 0)) && referidoBean.getFlagActivo() == 1){
                        pagReferidos.setCurrentItem(referidoBean.getCodigoTipoReferido() - 1);
                        hayReferidoVacio = true;
                        break;
                    }
                }
                if(hayReferidoVacio){
                    Snackbar.make(getView(), "Complete el nombre y apellido de los referidos", Snackbar.LENGTH_LONG).show();
                }else{
                    adnActivity.setArrReferidosFamiliar(eliminarReferidosInnecesarios(adnActivity.getArrReferidosFamiliar()));
                    adnActivity.setArrReferidosAmigos(eliminarReferidosInnecesarios(adnActivity.getArrReferidosAmigos()));
                    adnActivity.setArrReferidosTrabajo(eliminarReferidosInnecesarios(adnActivity.getArrReferidosTrabajo()));
                    adnActivity.setArrReferidosOtros(eliminarReferidosInnecesarios(adnActivity.getArrReferidosOtros()));
                    adnActivity.mostrarReferidosListaFinal();
                }
            }
        });
        RelativeLayout relLayReferidos = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayReferidos);
        relLayReferidos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(referidosFamiliaFragment.getEdtActual() != null){
                    ocultarTecladoConEditText(referidosFamiliaFragment.getEdtActual());
                }
                if(referidosAmigosFragment.getEdtActual() != null){
                    ocultarTecladoConEditText(referidosAmigosFragment.getEdtActual());
                }
                if(referidosTrabajoFragment.getEdtActual() != null){
                    ocultarTecladoConEditText(referidosTrabajoFragment.getEdtActual());
                }
                if(referidosOtrosFragment.getEdtActual() != null){
                    ocultarTecladoConEditText(referidosOtrosFragment.getEdtActual());
                }
            }
        });
        return view;
    }
    private ArrayList<ReferidoBean> eliminarReferidosInnecesarios(ArrayList<ReferidoBean> arrReferidos){
        ArrayList<ReferidoBean> arrReferidosFinal = new ArrayList<>();
        for(ReferidoBean referidoBean : arrReferidos){
            if(referidoBean.getFlagActivo() == 0){
                if(referidoBean.getIdReferidoDispositivo() != -1){
                    referidoBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                    ReferidoController.actualizarReferido(referidoBean);
                }
            }else{
                if(referidoBean.getNombres().length() > 0 && referidoBean.getApellidoPaterno().length() > 0){
                    arrReferidosFinal.add(referidoBean);
                }
            }
        }
        return arrReferidosFinal;
    }
    private void ocultarTecladoConEditText(EditText editText){
        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        editText.clearFocus();
    }
}