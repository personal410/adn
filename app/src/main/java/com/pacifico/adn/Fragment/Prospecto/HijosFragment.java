package com.pacifico.adn.Fragment.Prospecto;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.HijosArrayAdapter;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.Views.CustomView.CustomEditText;

import java.util.ArrayList;

public class HijosFragment extends Fragment{
    private ArrayList<FamiliarBean> arrHijos = new ArrayList<>();
    private CustomEditText edtActual;
    private HijosArrayAdapter hijosArrayAdapter;
    private boolean mostraError = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_hijos, container, false);
        ListView lstHijos = (ListView)view.findViewById(com.pacifico.adn.R.id.lstHijos);
        arrHijos = (ArrayList<FamiliarBean>)((ADNActivity)getActivity()).getArrHijosBean().clone();
        lstHijos.setDividerHeight(0);
        hijosArrayAdapter = new HijosArrayAdapter(this);
        lstHijos.setAdapter(hijosArrayAdapter);
        lstHijos.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event){
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(edtActual != null){
                        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(edtActual.getWindowToken(), 0);
                        edtActual.clearFocus();
                    }
                }
                return false;
            }
        });
        return view;
    }
    public void validarCampos(){
        mostraError = true;
        recargarListaHijos();
    }
    public void recargarListaHijos(){
        hijosArrayAdapter.notifyDataSetChanged();
    }
    public ArrayList<FamiliarBean> getArrHijos(){
        return arrHijos;
    }
    public void setArrHijos(ArrayList<FamiliarBean> arrHijos) {
        this.arrHijos = arrHijos;
    }
    public CustomEditText getEdtActual() {
        return edtActual;
    }
    public void setEdtActual(CustomEditText edtActual) {
        this.edtActual = edtActual;
    }
    public boolean getMostraError(){
        return mostraError;
    }
}