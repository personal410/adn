package com.pacifico.adn;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * Created by victorsalazar on 8/04/16.
 */
public class ExpandAnimation extends Animation{
    private float startWeigth;
    private float deltaWeight;
    private ViewGroup content;
    public ExpandAnimation(ViewGroup con, float start, float end){
        content = con;
        startWeigth = start;
        deltaWeight = end - start;
    }
    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)content.getLayoutParams();
        layoutParams.weight = startWeigth + deltaWeight * interpolatedTime;
        content.setLayoutParams(layoutParams);
    }
    @Override
    public boolean willChangeBounds() {
        return true;
    }
}