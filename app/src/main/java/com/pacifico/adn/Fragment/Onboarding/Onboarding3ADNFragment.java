package com.pacifico.adn.Fragment.Onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Onboarding3ADNFragment extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(com.pacifico.adn.R.layout.fragment_onboarding3_adn, container, false);
    }
}