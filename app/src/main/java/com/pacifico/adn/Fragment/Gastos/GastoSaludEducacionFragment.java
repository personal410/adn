package com.pacifico.adn.Fragment.Gastos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;
import com.pacifico.adn.Views.Interface.OnStepperDoneSelectedListener;

public class GastoSaludEducacionFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, OnStepperChangeValueListener, StepperView.OnFocusChangeListener {
    private ADNActivity actADN;
    private RadioGroup rgMonedaGastoSalud;
    private StepperView stepGastoSalud, stepGastoEducacion, stepActual;
    private GastosMensualFragment gastosMensualFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_gasto_salud_educacion, container, false);
        rgMonedaGastoSalud = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaGastoSalud);
        RadioGroup rgMonedaGastoEducacion = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaGastoEducacion);
        stepGastoSalud = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepGastoSalud);
        stepGastoEducacion = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepGastoEducacion);
        rgMonedaGastoSalud.check(actADN.getAdnBean().getMonedaGastoSalud() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb11 : com.pacifico.adn.R.id.rb12);
        rgMonedaGastoEducacion.check(actADN.getAdnBean().getMonedaGastoEducacion() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb21 : com.pacifico.adn.R.id.rb22);
        stepGastoSalud.setValorActual(actADN.getAdnBean().getGastoSalud());
        stepGastoEducacion.setValorActual(actADN.getAdnBean().getGastoEducacion());
        rgMonedaGastoSalud.setOnCheckedChangeListener(this);
        rgMonedaGastoEducacion.setOnCheckedChangeListener(this);
        stepGastoSalud.setOnStepperChangeValueListener(this);
        stepGastoEducacion.setOnStepperChangeValueListener(this);
        stepGastoSalud.setOnFocusChangeListener(this);
        stepGastoEducacion.setOnFocusChangeListener(this);
        stepGastoSalud.setOnStepperDoneSelectedListener(new OnStepperDoneSelectedListener() {
            @Override
            public void onStepperDoneSelected(StepperView stepperView) {
                stepGastoEducacion.requestStepperFocus();
            }
        });
        RelativeLayout relLayGastoSaludEducacion = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayGastoSaludEducacion);
        relLayGastoSaludEducacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stepActual == null){
                    gastosMensualFragment.getFlujoADNFragment().clearFocusStepTotalGastoFamiliares();
                }else{
                    stepActual.clearFocus();
                }
            }
        });
        return view;
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group == rgMonedaGastoSalud){
            actADN.getAdnBean().setMonedaGastoSalud((checkedId == com.pacifico.adn.R.id.rb11) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else{
            actADN.getAdnBean().setMonedaGastoEducacion((checkedId == com.pacifico.adn.R.id.rb21) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }
        calcularTotal();
    }
    @Override
    public void onChangeValue(StepperView stepView){
        int valorActual = stepView.getValorActual();
        if(stepView == stepGastoSalud){
            if(valorActual != actADN.getAdnBean().getGastoSalud()){
                actADN.getAdnBean().setGastoSalud(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else{
            if(valorActual != actADN.getAdnBean().getGastoEducacion()){
                actADN.getAdnBean().setGastoEducacion(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }
        calcularTotal();
    }
    public void calcularTotal(){
        double factorGastoSalud = actADN.getAdnBean().getMonedaGastoSalud() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorGastoEducacion = actADN.getAdnBean().getMonedaGastoEducacion() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double montoTotal = stepGastoSalud.getValorActual() / factorGastoSalud + stepGastoEducacion.getValorActual() / factorGastoEducacion;
        //montoTotal = Util.redondearNumero(montoTotal, 2);
        actADN.getAdnBean().setTotalGastoSaludEducacion(montoTotal);
        double totalGastos = actADN.getAdnBean().getTotalGastoVivienda() + montoTotal + actADN.getAdnBean().getTotalGastoTransporte() + actADN.getAdnBean().getTotalGastoOtros();
        actADN.getAdnBean().setTotalGastoFamiliarMensual(Util.redondearNumero(totalGastos, 2));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    public void setGastosMensualFragment(GastosMensualFragment gastosMensualFragment) {
        this.gastosMensualFragment = gastosMensualFragment;
    }
    public StepperView getStepActual() {
        return stepActual;
    }
    @Override
    public void onFocusChange(boolean hasFocus, StepperView stepperView) {
        if(hasFocus){
            stepActual = stepperView;
        }else{
            stepActual = null;
        }
    }
}