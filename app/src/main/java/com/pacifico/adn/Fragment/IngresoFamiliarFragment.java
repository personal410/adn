package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;
import com.pacifico.adn.Views.CustomView.StepperView;
import com.pacifico.adn.Views.Interface.OnStepperChangeValueListener;
import com.pacifico.adn.Views.Interface.OnStepperDoneSelectedListener;

public class IngresoFamiliarFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, OnStepperChangeValueListener, OnStepperDoneSelectedListener, StepperView.OnFocusChangeListener {
    private ADNActivity actADN;
    private RadioGroup rgMonedaIngresoMensualTitular, rgMonedaIngresoMensualConyuge;
    private StepperView stepIngresoMensualTitular, stepIngresoMensualConyuge, stepBonosUtilidades, stepActual;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_ingreso_familiar, container, false);
        rgMonedaIngresoMensualTitular = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaIngresoMensualTitular);
        rgMonedaIngresoMensualConyuge = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaIngresoMensualConyuge);
        RadioGroup rgMonedaBonosUtilidades = (RadioGroup)view.findViewById(com.pacifico.adn.R.id.rgMonedaBonosUtilidades);
        stepIngresoMensualTitular = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepIngresoMensualTitular);
        stepIngresoMensualConyuge = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepIngresoMensualConyuge);
        stepBonosUtilidades = (StepperView)view.findViewById(com.pacifico.adn.R.id.stepBonosUtilidades);
        rgMonedaIngresoMensualTitular.check(actADN.getAdnBean().getMonedaIngresoMensualTitular() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb11 : com.pacifico.adn.R.id.rb12);
        rgMonedaIngresoMensualConyuge.check(actADN.getAdnBean().getMonedaIngresoMensualConyuge() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb21 : com.pacifico.adn.R.id.rb22);
        rgMonedaBonosUtilidades.check(actADN.getAdnBean().getMonedaIngresoFamiliarOtros() == Constantes.MonedaDolares ? com.pacifico.adn.R.id.rb31 : com.pacifico.adn.R.id.rb32);
        stepIngresoMensualTitular.setValorActual(actADN.getAdnBean().getIngresoMensualTitular());
        stepIngresoMensualConyuge.setValorActual(actADN.getAdnBean().getIngresoMensualConyuge());
        stepBonosUtilidades.setValorActual(actADN.getAdnBean().getIngresoFamiliarOtros());
        rgMonedaIngresoMensualTitular.setOnCheckedChangeListener(this);
        rgMonedaIngresoMensualConyuge.setOnCheckedChangeListener(this);
        rgMonedaBonosUtilidades.setOnCheckedChangeListener(this);
        stepIngresoMensualTitular.setOnStepperChangeValueListener(this);
        stepIngresoMensualConyuge.setOnStepperChangeValueListener(this);
        stepBonosUtilidades.setOnStepperChangeValueListener(this);
        stepIngresoMensualTitular.setOnStepperDoneSelectedListener(this);
        stepIngresoMensualConyuge.setOnStepperDoneSelectedListener(this);
        stepIngresoMensualTitular.setOnFocusChangeListener(this);
        stepIngresoMensualConyuge.setOnFocusChangeListener(this);
        stepBonosUtilidades.setOnFocusChangeListener(this);
        RelativeLayout relLayIngresoFamiliar = (RelativeLayout)view.findViewById(com.pacifico.adn.R.id.relLayIngresoFamiliar);
        relLayIngresoFamiliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stepActual != null){
                    stepActual.clearFocus();
                }
            }
        });
        return view;
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group == rgMonedaIngresoMensualTitular){
            actADN.getAdnBean().setMonedaIngresoMensualTitular((checkedId == com.pacifico.adn.R.id.rb11) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagADNDigital(actADN.getAdnBean().getIngresoMensualTitular() > 0 ? 1 : 0);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
            verificarRangoIngreso();
        }else if(group == rgMonedaIngresoMensualConyuge){
            actADN.getAdnBean().setMonedaIngresoMensualConyuge((checkedId == com.pacifico.adn.R.id.rb21) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }else{
            actADN.getAdnBean().setMonedaIngresoFamiliarOtros((checkedId == com.pacifico.adn.R.id.rb31) ? Constantes.MonedaDolares : Constantes.MonedaSoles);
            actADN.getAdnBean().setFlagModificado(1);
            actADN.verificarTipoCambio();
        }
        calcularTotal();
    }
    @Override
    public void onChangeValue(StepperView stepView){
        int valorActual = stepView.getValorActual();
        if(stepView == stepIngresoMensualTitular){
            if(valorActual != actADN.getAdnBean().getIngresoMensualTitular()){
                actADN.getAdnBean().setIngresoMensualTitular(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
                verificarRangoIngreso();
            }
        }else if(stepView == stepIngresoMensualConyuge){
            if(valorActual != actADN.getAdnBean().getIngresoMensualConyuge()){
                actADN.getAdnBean().setIngresoMensualConyuge(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }else{
            if(valorActual != actADN.getAdnBean().getIngresoFamiliarOtros()){
                actADN.getAdnBean().setIngresoFamiliarOtros(valorActual);
                actADN.getAdnBean().setFlagModificado(1);
                actADN.verificarTipoCambio();
            }
        }
        calcularTotal();
    }
    public void calcularTotal(){
        double factorIngresoMensualTitular = actADN.getAdnBean().getMonedaIngresoMensualTitular() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorIngresoMensualConyuge = actADN.getAdnBean().getMonedaIngresoMensualConyuge() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double factorBonosUtilidades = actADN.getAdnBean().getMonedaIngresoFamiliarOtros() == Constantes.MonedaDolares ? 1 : actADN.getAdnBean().getTipoCambio();
        double montoTotal = stepIngresoMensualTitular.getValorActual() / factorIngresoMensualTitular + stepIngresoMensualConyuge.getValorActual() / factorIngresoMensualConyuge + (stepBonosUtilidades.getValorActual() / factorBonosUtilidades) / 12;
        actADN.getAdnBean().setTotalIngresoFamiliarMensual(Util.redondearNumero(montoTotal, 2));
        actADN.getFlujoADNFragment().actualizarMontoTotal();
    }
    public void verificarRangoIngreso(){
        double factorIngresoMensualTitular = actADN.getAdnBean().getMonedaIngresoMensualTitular() == Constantes.MonedaDolares ? actADN.getAdnBean().getTipoCambio() : 1;
        double ingresoMensualTitularSoles = actADN.getAdnBean().getIngresoMensualTitular() / factorIngresoMensualTitular;
        int nuevoCodigoRangoIngreso = 4;
        if(ingresoMensualTitularSoles < 6000){
            nuevoCodigoRangoIngreso = 1;
        }else if(ingresoMensualTitularSoles < 9000){
            nuevoCodigoRangoIngreso = 2;
        }else if(ingresoMensualTitularSoles < 12000){
            nuevoCodigoRangoIngreso = 3;
        }
        if(actADN.getProspectoBean().getCodigoRangoIngreso() != nuevoCodigoRangoIngreso){
            actADN.getProspectoBean().setCodigoRangoIngreso(nuevoCodigoRangoIngreso);
            actADN.guardarProspecto();
        }
    }
    @Override
    public void onStepperDoneSelected(StepperView stepperView){
        if(stepperView.equals(stepIngresoMensualTitular)){
            stepIngresoMensualConyuge.requestStepperFocus();
        }else{
            stepBonosUtilidades.requestStepperFocus();
        }
    }
    @Override
    public void onFocusChange(boolean hasFocus, StepperView stepperView) {
        if(hasFocus){
            stepActual = stepperView;
        }else{
            stepActual = null;
        }
    }
    public void clearStepActual(){
        if(stepActual != null){
            stepActual.clearFocus();
        }
    }
}