package com.pacifico.adn.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pacifico.adn.Model.Bean.ProspectoBean;

import java.util.ArrayList;

/**
 * Created by victorsalazar on 6/04/16.
 */
public class PosiblesProspectosArrayAdapter extends ArrayAdapter<String>{
    private ArrayList<ProspectoBean> arrProspectos;
    public PosiblesProspectosArrayAdapter(Context context){
        super(context, -1);
        arrProspectos = new ArrayList<>();
    }
    public void setArrProspectos(ArrayList<ProspectoBean> arrProspectos) {
        this.arrProspectos = arrProspectos;
        this.notifyDataSetChanged();
    }
    @Override
    public int getCount(){
        return arrProspectos.size();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(com.pacifico.adn.R.layout.row_posible_prospecto, parent, false);
        ProspectoBean pro = arrProspectos.get(position);
        TextView txtProspectName = (TextView)rowView.findViewById(com.pacifico.adn.R.id.txtProspectName);
        txtProspectName.setText(pro.getNombreCompleto());
        return rowView;
    }
    public ProspectoBean getProspectByPosition(int position){
        return arrProspectos.get(position);
    }
}