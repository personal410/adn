package com.pacifico.adn.Fragment.Referidos;

import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.pacifico.adn.Model.Bean.ReferidoBean;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 3/07/16.
 */
public class ReferidosGenericoFragment extends Fragment{
    private ArrayList<ReferidoBean> arrReferidos;
    private EditText edtActual;
    public ArrayList<ReferidoBean> getArrReferidos() {
        return arrReferidos;
    }
    public void setArrReferidos(ArrayList<ReferidoBean> arrReferidos) {
        this.arrReferidos = arrReferidos;
    }
    public EditText getEdtActual() {
        return edtActual;
    }
    public void setEdtActual(EditText edtActual) {
        this.edtActual = edtActual;
    }
}