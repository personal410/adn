package com.pacifico.adn.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Util;

public class IngresoNecesarioFragment extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ADNActivity actADN = (ADNActivity)getActivity();
        View view = inflater.inflate(com.pacifico.adn.R.layout.fragment_ingreso_necesario, container, false);
        TextView txtIngresoNetoMensualFamiliar = (TextView)view.findViewById(com.pacifico.adn.R.id.txtIngresoNetoMensualFamiliar);
        TextView txtIngresoConyuge = (TextView)view.findViewById(com.pacifico.adn.R.id.txtIngresoConyuge);
        TextView txtIngresoProvenientesAFP = (TextView)view.findViewById(com.pacifico.adn.R.id.txtIngresoProvenientesAFP);
        double ingresoNetoMensualFamiliar = Util.redondearNumero(actADN.getAdnBean().getTotalIngresoFamiliarMensual(), 0);
        txtIngresoNetoMensualFamiliar.setText(Util.obtenerMontoDolaresConFomato(ingresoNetoMensualFamiliar));
        double factorIngresoConyuge = (actADN.getAdnBean().getMonedaIngresoMensualConyuge() == Constantes.MonedaDolares) ? 1 : actADN.getAdnBean().getTipoCambio();
        double ingresoConyuge = Util.redondearNumero(actADN.getAdnBean().getIngresoMensualConyuge() / factorIngresoConyuge, 0);
        txtIngresoConyuge.setText(Util.obtenerMontoDolaresConFomato(ingresoConyuge));
        double ingresoProvenientesAFP = Util.redondearNumero(actADN.getAdnBean().getTotalPensionMensualAFP(), 0);
        txtIngresoProvenientesAFP.setText(Util.obtenerMontoDolaresConFomato(ingresoProvenientesAFP));
        return view;
    }
}