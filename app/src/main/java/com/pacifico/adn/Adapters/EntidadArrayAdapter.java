package com.pacifico.adn.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.pacifico.adn.Fragment.FormaPagoFragment;
import com.pacifico.adn.Model.Bean.EntidadBean;
import com.pacifico.adn.Util.Fuente;

public class EntidadArrayAdapter extends ArrayAdapter<EntidadBean>{
    private FormaPagoFragment formaPagoFragment;
    public EntidadArrayAdapter(FormaPagoFragment fragment){
        super(fragment.getContext(), -1);
        formaPagoFragment = fragment;
    }
    @Override
    public int getCount(){
        return formaPagoFragment.getArrEntidades().size();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(com.pacifico.adn.R.layout.row_entidad, parent, false);
        EntidadBean entidadBean = formaPagoFragment.getArrEntidades().get(position);
        String imagen = entidadBean.getImagen();
        if(imagen.length() == 0){
            TextView txtEntidad = (TextView)rowView.findViewById(com.pacifico.adn.R.id.txtEntidad);
            txtEntidad.setText(entidadBean.getDescripcion().charAt(0)+entidadBean.getDescripcion().substring(1,entidadBean.getDescripcion().length()).toLowerCase());
            Fuente.setFuenteRg(getContext(), txtEntidad);
        }else{
            byte[] imagenDecodificada = Base64.decode(imagen, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(imagenDecodificada, 0, imagenDecodificada.length);
            ImageView imgViewEntidad = (ImageView)rowView.findViewById(com.pacifico.adn.R.id.imgViewEntidad);
            imgViewEntidad.setVisibility(View.VISIBLE);
            imgViewEntidad.setImageBitmap(bitmap);
        }
        if(entidadBean.getIdEntidad() == formaPagoFragment.getEntidad()){
            RadioButton rbEntidad = (RadioButton)rowView.findViewById(com.pacifico.adn.R.id.rbEntidad);
            rbEntidad.setChecked(true);
        }
        return rowView;
    }
}