package com.pacifico.adn.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.pacifico.adn.Fragment.FormaPagoFragment;
import com.pacifico.adn.Model.Bean.TablaTablasBean;
import com.pacifico.adn.R;

/**
 * Created by vctrls3477 on 20/08/16.
 */
public class FrecuenciaPagoArrayAdapter extends ArrayAdapter<TablaTablasBean> {
    FormaPagoFragment formaPagoFragment;
    public FrecuenciaPagoArrayAdapter(FormaPagoFragment nFormaPagoFragment){
        super(nFormaPagoFragment.getContext(), -1);
        formaPagoFragment = nFormaPagoFragment;
    }
    @Override
    public int getCount(){
        return formaPagoFragment.getArrFrecuenciaPagos().size();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_forma_pago, parent, false);
        TextView txtFormaPago = (TextView)rowView.findViewById(R.id.txtFormaPago);
        TablaTablasBean frecuenciaPagoTablaTablasBean = formaPagoFragment.getArrFrecuenciaPagos().get(position);
        txtFormaPago.setText(frecuenciaPagoTablaTablasBean.getValorCadena());
        if(frecuenciaPagoTablaTablasBean.getCodigoCampo() == formaPagoFragment.getCodigoFrecuenciaPago()){
            RadioButton rbFormaPago = (RadioButton)rowView.findViewById(R.id.rbFormaPago);
            rbFormaPago.setChecked(true);
        }
        return rowView;
    }
}