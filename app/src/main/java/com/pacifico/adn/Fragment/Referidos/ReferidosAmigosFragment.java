package com.pacifico.adn.Fragment.Referidos;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;

import com.pacifico.adn.Activity.ADNActivity;
import com.pacifico.adn.Adapters.ReferidosArrayAdapter;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.R;

public class ReferidosAmigosFragment extends ReferidosGenericoFragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ADNActivity adnActivity = (ADNActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_referidos_amigos, container, false);
        ListView listReferidos = (ListView)view.findViewById(R.id.lstReferidos);
        listReferidos.setDividerHeight(0);
        if(adnActivity.getArrReferidosAmigos().size() > 0){
            ReferidoBean ultimoReferidoBean = adnActivity.getArrReferidosAmigos().get(adnActivity.getArrReferidosAmigos().size() - 1);
            if(ultimoReferidoBean.getNombres().length() != 0 || ultimoReferidoBean.getApellidoPaterno().length() != 0){
                ReferidoBean referidoBean = new ReferidoBean();
                referidoBean.inicializarValores();
                referidoBean.setCodigoTipoReferido(2);
                adnActivity.getArrReferidosAmigos().add(referidoBean);
            }
        }else{
            ReferidoBean referidoBean = new ReferidoBean();
            referidoBean.inicializarValores();
            referidoBean.setCodigoTipoReferido(2);
            adnActivity.getArrReferidosAmigos().add(referidoBean);
        }
        this.setArrReferidos(adnActivity.getArrReferidosAmigos());
        ReferidosArrayAdapter adapter = new ReferidosArrayAdapter(this, 2);
        listReferidos.setAdapter(adapter);
        listReferidos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (getEdtActual() != null) {
                        InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(getEdtActual().getWindowToken(), 0);
                        getEdtActual().clearFocus();
                    }
                }
                return false;
            }
        });
        return view;
    }
}