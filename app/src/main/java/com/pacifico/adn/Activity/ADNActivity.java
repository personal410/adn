package com.pacifico.adn.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.pacifico.adn.Fragment.Ajustes.AjustesFragment;
import com.pacifico.adn.Fragment.FlujoADNFragment;
import com.pacifico.adn.Fragment.FormaPagoFragment;
import com.pacifico.adn.Fragment.Prospecto.ProspectoFragment;
import com.pacifico.adn.Fragment.ReferidosFragment;
import com.pacifico.adn.Fragment.ReferidosListaFinalFragment;
import com.pacifico.adn.Model.Bean.AdnBean;
import com.pacifico.adn.Model.Bean.CitaBean;
import com.pacifico.adn.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.adn.Model.Bean.ParametroBean;
import com.pacifico.adn.Model.Bean.ProspectoBean;
import com.pacifico.adn.Model.Bean.FamiliarBean;
import com.pacifico.adn.Model.Bean.ReferidoBean;
import com.pacifico.adn.Model.Bean.TablaIdentificadorBean;
import com.pacifico.adn.Model.Controller.ADNController;
import com.pacifico.adn.Model.Controller.CitaReunionController;
import com.pacifico.adn.Model.Controller.FamiliarController;
import com.pacifico.adn.Model.Controller.ParametroController;
import com.pacifico.adn.Model.Controller.ProspectoController;
import com.pacifico.adn.Model.Controller.ReferidoController;
import com.pacifico.adn.Model.Controller.TablaIdentificadorController;
import com.pacifico.adn.Persistence.DatabaseConstants;
import com.pacifico.adn.Util.Constantes;
import com.pacifico.adn.Util.Fuente;
import com.pacifico.adn.Util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ADNActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FlujoADNFragment flujoADNFragment;
    private ProspectoFragment prospectoFragment;
    private ProspectoBean prospectoBean;
    private FamiliarBean conyugeBean;
    private Button btnNombreProspecto;
    private ArrayList<FamiliarBean> arrHijosBean = new ArrayList<>();
    private ArrayList<ReferidoBean> arrReferidosFamiliar = new ArrayList<>();
    private ArrayList<ReferidoBean> arrReferidosAmigos = new ArrayList<>();
    private ArrayList<ReferidoBean> arrReferidosTrabajo = new ArrayList<>();
    private ArrayList<ReferidoBean> arrReferidosOtros = new ArrayList<>();
    private AdnBean adnBean;
    private int indicePantalla = 0, indicePantallaPrincipal = 0;
    private boolean deberiaVerificar = true, cambiarNumeroHijosADN = false, adnModificado = false;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        prospectoBean = (ProspectoBean)getIntent().getExtras().getSerializable("prospecto");
        if(prospectoBean.getFlagConyuge() == 1){
            ArrayList<FamiliarBean> arrFamiliaresTemp = FamiliarController.obtenerFamiliarPorIdProspectoPorTipoFamiliar(prospectoBean.getIdProspectoDispositivo(), 1);
            if(arrFamiliaresTemp.size() > 0){
                conyugeBean = arrFamiliaresTemp.get(0);
            }
        }
        if(conyugeBean == null){
            conyugeBean = new FamiliarBean();
            conyugeBean.inicializarValores();
        }
        if(prospectoBean.getFlagHijo() == 1){
            cargarHijos();
        }
        arrReferidosFamiliar = ReferidoController.obtenerReferidoPorTipoReferido(prospectoBean.getIdProspectoDispositivo(), 1);
        arrReferidosAmigos = ReferidoController.obtenerReferidoPorTipoReferido(prospectoBean.getIdProspectoDispositivo(), 2);
        arrReferidosTrabajo = ReferidoController.obtenerReferidoPorTipoReferido(prospectoBean.getIdProspectoDispositivo(), 3);
        arrReferidosOtros = ReferidoController.obtenerReferidoPorTipoReferido(prospectoBean.getIdProspectoDispositivo(), 4);
        setContentView(com.pacifico.adn.R.layout.activity_adn);
        Toolbar toolbar = (Toolbar) findViewById(com.pacifico.adn.R.id.toolbar);
        setSupportActionBar(toolbar);
        btnNombreProspecto = (Button)findViewById(com.pacifico.adn.R.id.btnNombreProspecto);
        btnNombreProspecto.setText("ADN: "+ prospectoBean.getNombreCompleto());
        btnNombreProspecto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(indicePantalla > 0){
                    mostrarProspecto();
                }
            }
        });
        TextView txtFechaActual =(TextView)findViewById(com.pacifico.adn.R.id.txtFechaActual);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
        Calendar ahora = Calendar.getInstance();
        txtFechaActual.setText(simpleDateFormat.format(ahora.getTime()));
        TextView txtTipoCambio = (TextView)findViewById(com.pacifico.adn.R.id.txtTipoCambio);
        ParametroBean tipoCambioParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(2);
        txtTipoCambio.setText(String.format("%s %.2f", getString(com.pacifico.adn.R.string.simbolo_soles), tipoCambioParametroBean.getValorNumerico()));
        DrawerLayout drawer = (DrawerLayout) findViewById(com.pacifico.adn.R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, com.pacifico.adn.R.string.navigation_drawer_open, com.pacifico.adn.R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(com.pacifico.adn.R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        prospectoFragment = new ProspectoFragment();
        getSupportFragmentManager().beginTransaction().add(com.pacifico.adn.R.id.fraLayPrincipal, prospectoFragment).commit();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(com.pacifico.adn.R.menu.nav_menu, menu);
        MenuItem moreItem = menu.findItem(com.pacifico.adn.R.id.itemMas);
        Drawable more = moreItem.getIcon();
        more.mutate().setColorFilter(Color.rgb(255, 255, 255), PorterDuff.Mode.SRC_IN);
        moreItem.setIcon(more);

        if (CitaReunionController.validarCitaVentaRealizada(prospectoBean.getIdProspectoDispositivo())) {
            MenuItem itemDescartar = menu.findItem(com.pacifico.adn.R.id.itemDescartarProspecto);
            itemDescartar.setVisible(false);
        }

        return true;
    }
    @Override
    public void onBackPressed(){
        DrawerLayout drawer = (DrawerLayout) findViewById(com.pacifico.adn.R.id.drawer_layout);
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            if(indicePantalla == 0){
                Intent intent = new Intent(this, BuscarProspectoActivity.class);
                startActivity(intent);
            }else if(indicePantalla == 1){
                    int posicionActual = flujoADNFragment.getPosicionActual();
                    flujoADNFragment.cambiarPosicionalActualPorResumen(posicionActual - 1);
            }else if(indicePantalla == 2){
                mostrarFlujoADN(7);
            }else if(indicePantalla == 3){
                mostrarMetodoPago();
            }else if(indicePantalla == 4){
                mostrarReferidos();
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == com.pacifico.adn.R.id.itemGuardarSalir){
            guardarProspecto();
            if(prospectoBean.getNombres().length() == 0 || prospectoBean.getApellidoPaterno().length() == 0){
                Util.mostrarAlertaConTitulo("Alerta", "Debe completar el nombre y/o el apellido paterno", this);
            }else{
                guardarAdn();
                guardarReferidos();
                Intent intent = new Intent(this, BuscarProspectoActivity.class);
                startActivity(intent);
            }
            return true;
        }else if(item.getItemId() == com.pacifico.adn.R.id.itemCierreVenta){
            if(prospectoBean.getCodigoEstado() == 3){
                Util.mostrarAlertaConTitulo("Alerta", "No se puede descartar porque ya se cerro la venta", this);
            }else{
                final CitaBean citaBean = CitaReunionController.obteniendoUltimaCitaProspecto(prospectoBean);
                if(citaBean != null){
                    LayoutInflater layoutInflater = LayoutInflater.from(this);
                    final View dialogCierreVenta = layoutInflater.inflate(com.pacifico.adn.R.layout.dialog_cierre_venta, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setView(dialogCierreVenta).setCancelable(false).setNegativeButton("Cancelar", null).setPositiveButton("Confirmar Cierre", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            guardarAdn();
                            guardarReferidos();
                            CheckBox checkSeguroVida = (CheckBox) dialogCierreVenta.findViewById(com.pacifico.adn.R.id.checkSeguroVida);
                            CheckBox checkSeguroAP = (CheckBox) dialogCierreVenta.findViewById(com.pacifico.adn.R.id.checkSeguroAP);
                            citaBean.setCodigoEstado(3);
                            citaBean.setCodigoResultado(1);
                            if(checkSeguroVida.isChecked()){
                                EditText edtCantidadSV = (EditText) dialogCierreVenta.findViewById(com.pacifico.adn.R.id.edtCantidadSV);
                                String cantidadSV = edtCantidadSV.getText().toString();
                                if(cantidadSV.length() > 0){
                                    citaBean.setCantidadVI(Integer.parseInt(cantidadSV));
                                }else{
                                    citaBean.setCantidadVI(0);
                                }
                                EditText edtPrimaTargetSV = (EditText) dialogCierreVenta.findViewById(com.pacifico.adn.R.id.edtPrimaTargetSV);
                                String primaTargetSV = edtPrimaTargetSV.getText().toString();
                                if(primaTargetSV.length() > 0){
                                    citaBean.setPrimaTargetVI(Double.parseDouble(primaTargetSV));
                                }else{
                                    citaBean.setPrimaTargetVI(0);
                                }
                            }
                            if(checkSeguroAP.isChecked()){
                                EditText edtCantidaSAP = (EditText) dialogCierreVenta.findViewById(com.pacifico.adn.R.id.edtCantidadSAP);
                                String cantidadSAP = edtCantidaSAP.getText().toString();
                                if(cantidadSAP.length() > 0){
                                    citaBean.setCantidadAP(Integer.parseInt(cantidadSAP));
                                }else{
                                    citaBean.setCantidadAP(0);
                                }
                                EditText edtPrimaTargetSAP = (EditText) dialogCierreVenta.findViewById(com.pacifico.adn.R.id.edtPrimaTargetSAP);
                                String primaTargetSAP = edtPrimaTargetSAP.getText().toString();
                                if(primaTargetSAP.length() > 0){
                                    citaBean.setPrimaTargetAP(Double.parseDouble(primaTargetSAP));
                                }else{
                                    citaBean.setPrimaTargetAP(0);
                                }
                            }
                            citaBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                            citaBean.setFlagEnviado(0);
                            CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
                            citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
                            citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());
                            citaMovimientoEstadoBean.setCodigoEstado(3);
                            citaMovimientoEstadoBean.setCodigoResultado(1);
                            citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(Util.obtenerFechaActual());
                            citaMovimientoEstadoBean.setFlagEnviado(0);
                            CitaReunionController.guardarCita(citaBean, false);
                            CitaReunionController.guardarCitaMovimientoEstado(citaMovimientoEstadoBean);
                            if(prospectoBean.getCodigoEstado() != 3){
                                prospectoBean.setCodigoEstado(3);
                                prospectoBean.setFlagModificado(1);
                            }
                            guardarProspecto();
                            Intent intent = new Intent(ADNActivity.this, BuscarProspectoActivity.class);
                            startActivity(intent);
                        }
                    });
                    AlertDialog  alert = builder.create();
                    alert.show();
                    Button btnConfirmar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                    btnConfirmar.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorCeleste));
                    Fuente.setFuenteButtonRg(this, btnConfirmar, btnCancelar);
                }else{
                    Util.mostrarAlertaConTitulo("Alerta", "No hay cita pendiente", ADNActivity.this);
                }
            }
        }else if(item.getItemId() == com.pacifico.adn.R.id.itemDescartarProspecto){

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            final View dialogDescartarProspecto = layoutInflater.inflate(com.pacifico.adn.R.layout.dialog_descartar_prospecto, null);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            TextView txtPreguntarDescartarProspecto = (TextView)dialogDescartarProspecto.findViewById(com.pacifico.adn.R.id.txtPreguntaDescartarProspecto);
            txtPreguntarDescartarProspecto.setText("¿Deseas descartar a \"" + prospectoBean.getNombreCompleto() + "\"?");
            builder.setView(dialogDescartarProspecto).setCancelable(false).setNegativeButton("Cancelar", null).setPositiveButton("Descartar y continuar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    guardarAdn();
                    guardarReferidos();
                    CitaBean citaBean = CitaReunionController.obteniendoUltimaCitaProspecto(prospectoBean);
                    if(citaBean != null){
                        citaBean.setCodigoEstado(3);
                        citaBean.setCodigoResultado(3);
                        citaBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                        citaBean.setFlagEnviado(0);
                        CitaMovimientoEstadoBean citaMovimientoEstadoBean = new CitaMovimientoEstadoBean();
                        citaMovimientoEstadoBean.setIdCita(citaBean.getIdCita());
                        citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());
                        citaMovimientoEstadoBean.setCodigoEstado(3);
                        citaMovimientoEstadoBean.setCodigoResultado(3);
                        citaMovimientoEstadoBean.setFechaMovimientoEstadoDispositivo(Util.obtenerFechaActual());
                        citaMovimientoEstadoBean.setFlagEnviado(0);
                        CitaReunionController.guardarCita(citaBean, false);
                        CitaReunionController.guardarCitaMovimientoEstado(citaMovimientoEstadoBean);
                    }
                    if(prospectoBean.getCodigoEstado() != 4){
                        prospectoBean.setCodigoEstado(4);
                        prospectoBean.setFlagModificado(1);
                    }
                    guardarProspecto();
                    Intent intent = new Intent(ADNActivity.this, BuscarProspectoActivity.class);
                    startActivity(intent);
                }
            });
           AlertDialog alert =  builder.create();
            alert.show();
            Button btnConfirmar = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            Button btnCancelar = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            btnConfirmar.setTextColor(getResources().getColor(com.pacifico.adn.R.color.colorCeleste));
            Fuente.setFuenteButtonRg(this,btnConfirmar,btnCancelar);
        }
        return super.onOptionsItemSelected(item);
    }
    public void mostrarProspecto(){
        indicePantalla = 0;
        prospectoFragment = new ProspectoFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(com.pacifico.adn.R.id.fraLayPrincipal, prospectoFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void mostrarFlujoADN(int posicion){
        btnNombreProspecto.setText("ADN: "+ prospectoBean.getNombreCompleto());
        indicePantalla = 1;
        if(adnBean == null){
            int idProspecto = prospectoBean.getIdProspecto();
            int idProspectoDispositivo = prospectoBean.getIdProspectoDispositivo();
            adnBean = ADNController.getAdnPorIdProspecto(idProspecto, idProspectoDispositivo);
            if(adnBean == null){
                adnBean = new AdnBean();
                adnBean.inicializarValores();
                adnBean.setFlagPensionConyuge(prospectoBean.getFlagConyuge());
                adnBean.setFlagPensionHijos(prospectoBean.getFlagHijo());
                deberiaVerificar = false;
            }else{
                adnBean.calcularTotalGastoVivienda();
                adnBean.calcularTotalGastoSaludEducacion();
                adnBean.calcularTotalGastoTransporte();
                adnBean.calcularTotalGastoOtros();
            }
        }
        if(cambiarNumeroHijosADN){
            cambiarNumeroHijosADN = false;
            int cantHijosMenoresEdad = 0;
            for(FamiliarBean hijoBean : arrHijosBean){
                if (hijoBean.getEdad() < 18){
                    cantHijosMenoresEdad++;
                }
            }
            adnBean.setNumeroHijos(cantHijosMenoresEdad);
            adnBean.setFlagModificado(1);
            guardarAdn();
        }
        flujoADNFragment = new FlujoADNFragment();
        flujoADNFragment.setPosicionActual(posicion);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(com.pacifico.adn.R.id.fraLayPrincipal, flujoADNFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void mostrarMetodoPago(){
        indicePantalla = 2;
        FormaPagoFragment formaPagoFragment = new FormaPagoFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(com.pacifico.adn.R.id.fraLayPrincipal, formaPagoFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void mostrarReferidos(){
        indicePantalla = 3;
        ReferidosFragment referidosFragment = new ReferidosFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(com.pacifico.adn.R.id.fraLayPrincipal, referidosFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void mostrarReferidosListaFinal(){
        indicePantalla = 4;
        ReferidosListaFinalFragment referidosListaFinalFragment = new ReferidosListaFinalFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(com.pacifico.adn.R.id.fraLayPrincipal, referidosListaFinalFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void mostrarAjustes(){
        AjustesFragment ajustesFragment = new AjustesFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(com.pacifico.adn.R.id.fraLayPrincipal, ajustesFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void guardarAdn(){
        if(adnBean != null){
            if(adnBean.getFlagModificado() == 1){
                adnModificado = true;
                adnBean.setFlagModificado(0);
                adnBean.setFlagEnviado(0);
                if(adnBean.getIdProspecto() == -1 && adnBean.getIdProspectoDispositivo() == -1){
                    adnBean.setIdProspecto(prospectoBean.getIdProspecto());
                    adnBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                    adnBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                    ADNController.guardarAdn(adnBean);
                }else{
                    adnBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                    ADNController.actualizarADN(adnBean);
                }
            }
        }
    }
    public void verificarTipoCambio(){
        if(deberiaVerificar){
            ParametroBean tipoCambioParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(2);
            double tipoCambio = tipoCambioParametroBean.getValorNumerico();
            deberiaVerificar = false;
            if(adnBean.getTipoCambio() != tipoCambio){
                adnBean.setTipoCambio(tipoCambio);
                double factorEfectivoAhorros = adnBean.getMonedaEfectivoAhorros() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorPropiedades = adnBean.getMonedaPropiedades() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorVehiculos = adnBean.getMonedaVehiculos() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double montoTotalActivos = adnBean.getEfectivoAhorros() / factorEfectivoAhorros + adnBean.getPropiedades() / factorPropiedades + adnBean.getVehiculos() / factorVehiculos;
                adnBean.setTotalActivoRealizable(Util.redondearNumero(montoTotalActivos, 2));

                double montoIngresoBrutoMensual = adnBean.getIngresoBrutoMensualVidaLey();
                double factorSeguroVidaLey = adnBean.getMonedaIngresoBrutoMensualVidaLey() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorTopeLey = adnBean.getMonedaIngresoBrutoMensualVidaLey() == Constantes.MonedaSoles ? 1 : tipoCambio;
                double topeVidaLey = adnBean.getTopeVidaLey() / factorTopeLey;
                double montoIngresoBrutoMensualTemp = montoIngresoBrutoMensual;
                if(montoIngresoBrutoMensualTemp > topeVidaLey){
                    montoIngresoBrutoMensualTemp = topeVidaLey;
                }
                double factorVidaLey = adnBean.getFactorVidaLey();
                double montoSeguroVidaLey = Util.redondearNumero(montoIngresoBrutoMensualTemp * factorVidaLey, 0);
                adnBean.setSeguroVidaLey(montoSeguroVidaLey);
                double factorSeguroIndividual = adnBean.getMonedaSeguroIndividual() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double montoVidaLey = 0;
                if(adnBean.getFlagCuentaConVidaLey() == 1){
                    montoVidaLey = montoSeguroVidaLey / factorSeguroVidaLey;
                }
                double montoTotalSeguros = adnBean.getSeguroIndividual() / factorSeguroIndividual + montoVidaLey;
                adnBean.setTotalSegurosVida(Util.redondearNumero(montoTotalSeguros, 2));

                double ingresoBrutoMensual = montoIngresoBrutoMensual / factorSeguroVidaLey;
                double pensionConyuge = 0;
                double pensionHijos = 0;
                double montoTotalPension = 0;
                if(adnBean.getFlagPensionNoAFP() == 0){
                    if(adnBean.getFlagPensionConyuge() == 1){
                        pensionConyuge = Util.redondearNumero(ingresoBrutoMensual * adnBean.getPorcentajeAFPConyuge() / 100.0, 0);
                    }
                    if(adnBean.getFlagPensionHijos() == 1){
                        pensionHijos = Util.redondearNumero(ingresoBrutoMensual * adnBean.getNumeroHijos() * adnBean.getPorcentajeAFPHijos() / 100.0, 0);
                    }
                    montoTotalPension = pensionConyuge + pensionHijos;
                }
                adnBean.setPensionConyuge(pensionConyuge);
                adnBean.setPensionHijos(pensionHijos);
                adnBean.setTotalPensionMensualAFP(montoTotalPension);

                double factorIngresoMensualTitular = adnBean.getMonedaIngresoMensualTitular() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorIngresoMensualConyuge = adnBean.getMonedaIngresoMensualConyuge() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorBonosUtilidades = adnBean.getMonedaIngresoFamiliarOtros() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double montoTotalIngresoFamiliar = adnBean.getIngresoMensualTitular() / factorIngresoMensualTitular + adnBean.getIngresoMensualConyuge() / factorIngresoMensualConyuge + (adnBean.getIngresoFamiliarOtros() / factorBonosUtilidades) / 12;
                adnBean.setTotalIngresoFamiliarMensual(Util.redondearNumero(montoTotalIngresoFamiliar, 2));

                boolean cambiarTotalGastos = adnBean.getTotalGastoFamiliarMensual() == (adnBean.getTotalGastoVivienda() + adnBean.getTotalGastoSaludEducacion() + adnBean.getTotalGastoTransporte() + adnBean.getTotalGastoOtros());
                double factorGastoVivienda = adnBean.getMonedaGastoVivienda() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorGastoServicios = adnBean.getMonedaGastoServicios() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorGastoHogarAlimentacion = adnBean.getMonedaGastoHogarAlimentacion() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double montoTotalGastoVivienda = adnBean.getGastoVivienda() / factorGastoVivienda + adnBean.getGastoServicios() / factorGastoServicios + adnBean.getGastoHogarAlimentacion() / factorGastoHogarAlimentacion;
                montoTotalGastoVivienda = Util.redondearNumero(montoTotalGastoVivienda, 2);
                adnBean.setTotalGastoVivienda(montoTotalGastoVivienda);
                double factorGastoSalud = adnBean.getMonedaGastoSalud() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorGastoEducacion = adnBean.getMonedaGastoEducacion() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double montoTotalGastoSaludEducacion = adnBean.getGastoSalud() / factorGastoSalud + adnBean.getGastoEducacion() / factorGastoEducacion;
                montoTotalGastoSaludEducacion = Util.redondearNumero(montoTotalGastoSaludEducacion, 2);
                adnBean.setTotalGastoSaludEducacion(montoTotalGastoSaludEducacion);
                double factorGastoVehiculosTransporte = adnBean.getMonedaGastoVehiculoTransporte() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double montoTotalGastoVehiculosTransporte = adnBean.getGastoVehiculoTransporte() / factorGastoVehiculosTransporte;
                montoTotalGastoVehiculosTransporte = Util.redondearNumero(montoTotalGastoVehiculosTransporte, 2);
                adnBean.setTotalGastoTransporte(montoTotalGastoVehiculosTransporte);
                double factorGastoEsparcimiento = adnBean.getMonedaGastoEsparcimiento() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double factorGastoOtros = adnBean.getMonedaGastoOtros() == Constantes.MonedaDolares ? 1 : tipoCambio;
                double montoTotalGastoOtros = adnBean.getGastoEsparcimiento() / factorGastoEsparcimiento + adnBean.getGastoOtros() / factorGastoOtros;
                montoTotalGastoOtros = Util.redondearNumero(montoTotalGastoOtros, 2);
                adnBean.setTotalGastoOtros(montoTotalGastoOtros);
                if(cambiarTotalGastos){
                    double totalGastos = montoTotalGastoVivienda + montoTotalGastoSaludEducacion + montoTotalGastoVehiculosTransporte + montoTotalGastoOtros;
                    adnBean.setTotalGastoFamiliarMensual(Util.redondearNumero(totalGastos, 2));
                }

                double ingresoNetoMensualFamiliar = Util.redondearNumero(montoTotalIngresoFamiliar, 0);
                double ingresoConyuge = Util.redondearNumero(adnBean.getIngresoMensualConyuge() / factorIngresoMensualConyuge, 0);
                double ingresoProvenientesAFP = Util.redondearNumero(montoTotalPension, 0);
                double montoDeficitMensual = Util.redondearNumero(ingresoNetoMensualFamiliar - ingresoConyuge - ingresoProvenientesAFP, 0);
                adnBean.setDeficitMensual(montoDeficitMensual);

                double ingresoAnualNecesario = montoDeficitMensual * 12;
                double capitalNecesario = ingresoAnualNecesario * adnBean.getAniosProteger();
                double segurosVida = Util.redondearNumero(montoTotalSeguros, 0);
                double totalActivosRealizables = Util.redondearNumero(montoTotalActivos, 0);
                double montoTotal = capitalNecesario - totalActivosRealizables - segurosVida;
                if(montoTotal < 0){
                    montoTotal = 0;
                }
                adnBean.setCapitalNecesarioFallecimiento(montoTotal);

                adnBean.setFlagEnviado(0);
                guardarAdn();

                flujoADNFragment.actualizarMontoTotal();
                flujoADNFragment.actualizarResumen();

                Util.mostrarAlertaConTitulo("Tipo de Cambio", "Se actualizo el tipo de cambio", this);
            }
        }
    }
    public void guardarReferidos(){
        int IdProspecto = prospectoBean.getIdProspecto();
        int IdProspectoDispositivo = prospectoBean.getIdProspectoDispositivo();
        TablaIdentificadorBean referidoIdentificadorBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_REFERIDO);
        int idReferidoDispositivoDisponible = referidoIdentificadorBean.getIdentity() + 1;
        ArrayList<ReferidoBean> arrReferidos = new ArrayList<>();
        arrReferidos.addAll(arrReferidosFamiliar);
        arrReferidos.addAll(arrReferidosAmigos);
        arrReferidos.addAll(arrReferidosTrabajo);
        arrReferidos.addAll(arrReferidosOtros);
        TablaIdentificadorBean prospectoIdentificadorBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_PROSPECTO);
        int idProspectoDispositivo = prospectoIdentificadorBean.getIdentity();
        for(ReferidoBean referidoBean : arrReferidos){
            if(referidoBean.getFlagModificado() == 1){
                referidoBean.setFlagModificado(0);
                int idReferidoDispositivo = referidoBean.getIdReferidoDispositivo();
                if(idReferidoDispositivo == -1){
                    if(referidoBean.getFlagActivo() == 1){
                        referidoBean.setIdReferidoDispositivo(idReferidoDispositivoDisponible);
                        idReferidoDispositivoDisponible++;
                        referidoBean.setIdProspecto(IdProspecto);
                        referidoBean.setIdProspectoDispositivo(IdProspectoDispositivo);
                        referidoBean.setFechaCreaContacto(Util.obtenerFechaActual());
                        referidoBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                        ReferidoController.guardarReferido(referidoBean);
                        if(referidoBean.getTelefono() != null){
                            if(referidoBean.getTelefono().length() > 5 && referidoBean.getTelefono().length() < 10 && referidoBean.getFlagProspectoCreado() == 0){
                                ProspectoBean prospectoBeanTemp = new ProspectoBean();
                                prospectoBeanTemp.setIdProspecto(-1);
                                idProspectoDispositivo++;
                                prospectoBeanTemp.setIdProspectoDispositivo(idProspectoDispositivo);
                                prospectoBeanTemp.setIdProspectoExterno(-1);
                                prospectoBeanTemp.setNombres(referidoBean.getNombres());
                                prospectoBeanTemp.setApellidoPaterno(referidoBean.getApellidoPaterno());
                                prospectoBeanTemp.setCodigoSexo(-1);
                                prospectoBeanTemp.setCodigoEstadoCivil(-1);
                                prospectoBeanTemp.setCodigoRangoEdad(referidoBean.getCodigoRangoEdad());
                                prospectoBeanTemp.setCodigoRangoIngreso(referidoBean.getCodigoRangoIngreso());
                                prospectoBeanTemp.setFlagHijo(referidoBean.getFlagHijo());
                                prospectoBeanTemp.setFlagConyuge(-1);
                                prospectoBeanTemp.setCodigoTipoDocumento(-1);
                                prospectoBeanTemp.setTelefonoCelular(referidoBean.getTelefono());
                                prospectoBeanTemp.setCodigoNacionalidad(-1);
                                prospectoBeanTemp.setCondicionFumador(-1);
                                prospectoBeanTemp.setCodigoEtapa(1);
                                prospectoBeanTemp.setCodigoEstado(1);
                                prospectoBeanTemp.setCodigoFuente(2);
                                prospectoBeanTemp.setCodigoEntorno(referidoBean.getCodigoTipoReferido());
                                prospectoBeanTemp.setIdReferenciador(prospectoBean.getIdProspecto());
                                prospectoBeanTemp.setIdReferenciadorDispositivo(prospectoBean.getIdProspectoDispositivo());
                                prospectoBeanTemp.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                                prospectoBeanTemp.setAdicionalNumerico1(-1);
                                prospectoBeanTemp.setAdicionalNumerico2(-1);
                                prospectoBeanTemp.setFlagEnviado(0);
                                prospectoBeanTemp.setAdicionalNumerico1(referidoBean.getIdReferidoDispositivo()); // idReferido dispositivo fuente
                                ProspectoController.guardarProspecto(prospectoBeanTemp);
                            }
                        }
                    }
                }else{
                    Log.i("TAG", "actualizar referido");
                    if(referidoBean.getTelefono() != null){
                        if(referidoBean.getTelefono().length() > 5 && referidoBean.getTelefono().length() < 10 && referidoBean.getFlagProspectoCreado() == 0){
                            ProspectoBean prospectoBeanTemp = new ProspectoBean();
                            prospectoBeanTemp.setIdProspecto(-1);
                            idProspectoDispositivo++;
                            prospectoBeanTemp.setIdProspectoDispositivo(idProspectoDispositivo);
                            prospectoBeanTemp.setIdProspectoExterno(-1);
                            prospectoBeanTemp.setNombres(referidoBean.getNombres());
                            prospectoBeanTemp.setApellidoPaterno(referidoBean.getApellidoPaterno());
                            prospectoBeanTemp.setCodigoSexo(-1);
                            prospectoBeanTemp.setCodigoEstadoCivil(-1);
                            prospectoBeanTemp.setCodigoRangoEdad(referidoBean.getCodigoRangoEdad());
                            prospectoBeanTemp.setCodigoRangoIngreso(referidoBean.getCodigoRangoIngreso());
                            prospectoBeanTemp.setFlagHijo(referidoBean.getFlagHijo());
                            prospectoBeanTemp.setFlagConyuge(-1);
                            prospectoBeanTemp.setCodigoTipoDocumento(-1);
                            prospectoBeanTemp.setTelefonoCelular(referidoBean.getTelefono());
                            prospectoBeanTemp.setCodigoNacionalidad(-1);
                            prospectoBeanTemp.setCondicionFumador(-1);
                            prospectoBeanTemp.setCodigoEtapa(1);
                            prospectoBeanTemp.setCodigoEstado(1);
                            prospectoBeanTemp.setCodigoFuente(2);
                            prospectoBeanTemp.setCodigoEntorno(referidoBean.getCodigoTipoReferido());
                            prospectoBeanTemp.setIdReferenciador(prospectoBean.getIdProspecto());
                            prospectoBeanTemp.setIdReferenciadorDispositivo(prospectoBean.getIdProspectoDispositivo());
                            prospectoBeanTemp.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                            prospectoBeanTemp.setAdicionalNumerico1(-1);
                            prospectoBeanTemp.setAdicionalNumerico2(-1);
                            prospectoBeanTemp.setFlagEnviado(0);
                            prospectoBeanTemp.setAdicionalNumerico1(referidoBean.getIdReferidoDispositivo()); // idReferido dispositivo fuente
                            ProspectoController.guardarProspecto(prospectoBeanTemp);
                        }
                    }
                    referidoBean.setFlagEnviado(0);
                    referidoBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                    ReferidoController.actualizarReferido(referidoBean);
                }
            }
        }
        referidoIdentificadorBean.setIdentity(idReferidoDispositivoDisponible);
        prospectoIdentificadorBean.setIdentity(idProspectoDispositivo);
        TablaIdentificadorController.actualizarTablaIdentificador(referidoIdentificadorBean);
        TablaIdentificadorController.actualizarTablaIdentificador(prospectoIdentificadorBean);
    }
    public void guardarProspecto(){
        prospectoFragment.guardar();
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        if(item.getItemId() == com.pacifico.adn.R.id.nav_item_salir){
            finishAffinity();
        }else if(item.getItemId() == com.pacifico.adn.R.id.nav_item_Adn){
            if(indicePantallaPrincipal == 1){
                indicePantallaPrincipal = 0;
                mostrarProspecto();
            }
        }else if (item.getItemId() == com.pacifico.adn.R.id.nav_item_ajustes){
            if(indicePantallaPrincipal == 0){
                indicePantallaPrincipal = 1;
                mostrarAjustes();
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(com.pacifico.adn.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void cargarHijos(){
        arrHijosBean = FamiliarController.obtenerFamiliarPorIdProspectoPorTipoFamiliar(prospectoBean.getIdProspectoDispositivo(), 2);
    }
    public void revisarNumeroHijos(){
        prospectoFragment.revisarNumeroHijos();
    }
    public FlujoADNFragment getFlujoADNFragment() {
        return flujoADNFragment;
    }
    public ProspectoBean getProspectoBean() {
        return prospectoBean;
    }
    public AdnBean getAdnBean() {
        return adnBean;
    }
    public FamiliarBean getConyugeBean() {
        return conyugeBean;
    }
    public ArrayList<FamiliarBean> getArrHijosBean() {
        return arrHijosBean;
    }
    public ArrayList<ReferidoBean> getArrReferidosFamiliar() {
        return arrReferidosFamiliar;
    }
    public ArrayList<ReferidoBean> getArrReferidosAmigos() {
        return arrReferidosAmigos;
    }
    public ArrayList<ReferidoBean> getArrReferidosTrabajo() {
        return arrReferidosTrabajo;
    }
    public ArrayList<ReferidoBean> getArrReferidosOtros() {
        return arrReferidosOtros;
    }
    public void setArrReferidosFamiliar(ArrayList<ReferidoBean> arrReferidosFamiliar) {
        this.arrReferidosFamiliar = arrReferidosFamiliar;
    }
    public void setArrReferidosAmigos(ArrayList<ReferidoBean> arrReferidosAmigos) {
        this.arrReferidosAmigos = arrReferidosAmigos;
    }
    public void setArrReferidosTrabajo(ArrayList<ReferidoBean> arrReferidosTrabajo) {
        this.arrReferidosTrabajo = arrReferidosTrabajo;
    }
    public void setArrReferidosOtros(ArrayList<ReferidoBean> arrReferidosOtros) {
        this.arrReferidosOtros = arrReferidosOtros;
    }
    public void setCambiarNumeroHijosADN(boolean cambiarNumeroHijosADN) {
        this.cambiarNumeroHijosADN = cambiarNumeroHijosADN;
    }
    public boolean getAdnModificado() {
        return adnModificado;
    }
}